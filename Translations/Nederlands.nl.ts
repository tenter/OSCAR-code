<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl_NL">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialoog</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="35"/>
        <source>&amp;About</source>
        <translation>&amp;Over</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="49"/>
        <location filename="../oscar/aboutdialog.cpp" line="125"/>
        <source>Release Notes</source>
        <translation>Versie-opmerkingen</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="63"/>
        <source>Credits</source>
        <translation>Dank</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="77"/>
        <source>GPL License</source>
        <translation>GPL Licentie</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="239"/>
        <source>Close</source>
        <translation>Sluiten</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="43"/>
        <source>Show data folder</source>
        <translation>Toon data map</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="47"/>
        <source>About OSCAR</source>
        <translation>Over OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="91"/>
        <source>Sorry, could not locate About file.</source>
        <translation>Sorry, kan het bestand &quot;Over&quot; niet vinden.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="104"/>
        <source>Sorry, could not locate Credits file.</source>
        <translation>Sorry, kan het bestand &quot;Dank&quot; niet vinden.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="116"/>
        <source>Sorry, could not locate Release Notes.</source>
        <translation>Sorry, kan geen lijst van veranderingen vinden.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="126"/>
        <source>OSCAR v%1</source>
        <translation>OSCAR v%1</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="129"/>
        <source>Important:</source>
        <translation>Belangrijk:</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="130"/>
        <source>As this is a pre-release version, it is recommended that you &lt;b&gt;back up your data folder manually&lt;/b&gt; before proceding, because attempting to roll back later may break things.</source>
        <translation>Dit is nog geen uiteindelijke versie, dus maak een backup van de map OSCAR-Data voordat U verder gaat. Dan kunt U terugvallen op een vorige versie.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="142"/>
        <source>To see if the license text is available in your language, see %1.</source>
        <translation>Kijk bij %1 om te zien of een vertaling van de licentie in het Nederlands beschikbaar is.</translation>
    </message>
</context>
<context>
    <name>CMS50F37Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="873"/>
        <source>Could not find the oximeter file:</source>
        <translation>Kan het oxymeter bestand niet vinden:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="879"/>
        <source>Could not open the oximeter file:</source>
        <translation>Kan het oxymeter bestand niet openen:</translation>
    </message>
</context>
<context>
    <name>CMS50Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="478"/>
        <source>Could not get data transmission from oximeter.</source>
        <translation>Er kwam geen gegevensoverdracht van de oxymeter.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="478"/>
        <source>Please ensure you select &apos;upload&apos; from the oximeter devices menu.</source>
        <translation>Kies eerst &apos;upload&apos; in het menu van de oxymeter.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="546"/>
        <source>Could not find the oximeter file:</source>
        <translation>Kan het oxymeter bestand niet vinden:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="552"/>
        <source>Could not open the oximeter file:</source>
        <translation>Kan het oxymeter bestand niet openen:</translation>
    </message>
</context>
<context>
    <name>Daily</name>
    <message>
        <location filename="../oscar/daily.ui" line="435"/>
        <source>Form</source>
        <translatorcomment>Staat bovenaan het venster hierboven</translatorcomment>
        <translation>Formulier</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="506"/>
        <source>Go to the previous day</source>
        <translation>Ga naar de vorige dag</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="551"/>
        <source>Show or hide the calender</source>
        <translation>Kalender aan/uit zetten</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="614"/>
        <source>Go to the next day</source>
        <translation>Ga naar de volgende dag</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="674"/>
        <source>Go to the most recent day with data records</source>
        <translatorcomment>WJG: compacter</translatorcomment>
        <translation>Ga naar de laatste dag met gegevens</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="861"/>
        <source>Events</source>
        <translatorcomment>WJG: Zou &apos;Apneus&apos; niet beter zijn dan Gebeurtenissen&apos;? Want dat is wat hier geteld wordt en het past beter op de ruimte van het tabje.
AK: Nee, er zijn ook andere gebeurtenissen: snurken, RERA, enz. Misschien &apos;evenementen&apos; of &apos;incidenten&apos;?</translatorcomment>
        <translation>Incidenten</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="923"/>
        <source>View Size</source>
        <translatorcomment>Onder INCIDENTEN
Misschien is &apos;Zoomniveau&apos; beter?</translatorcomment>
        <translation>Beeldgrootte</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="968"/>
        <location filename="../oscar/daily.ui" line="1401"/>
        <source>Notes</source>
        <translatorcomment>WJG: Is compacter, past beter op tabje
In verband met de koppeling met Bladwijzers, lijkt me &apos;Notities&apos; beter.</translatorcomment>
        <translation>Notities</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1022"/>
        <source>Journal</source>
        <translatorcomment>WJG: is gebruikelijker</translatorcomment>
        <translation>Dagboek</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1116"/>
        <location filename="../oscar/daily.ui" line="1126"/>
        <source>Small</source>
        <translation>Klein</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1131"/>
        <source>Medium</source>
        <translation>Medium</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1136"/>
        <source>Big</source>
        <translation>Groot</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1094"/>
        <source>Color</source>
        <translation>Kleur</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1050"/>
        <source> i </source>
        <translation> i </translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1081"/>
        <source>u</source>
        <translation>u</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1066"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1195"/>
        <source>Zombie</source>
        <translation>Zombie</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1208"/>
        <source>I&apos;m feeling ...</source>
        <translation>Ik voel me ...</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1224"/>
        <source>Weight</source>
        <translation>Gewicht</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1298"/>
        <source>Awesome</source>
        <translation>Fantastisch</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1336"/>
        <source>B.M.I.</source>
        <translatorcomment>zonder puntjes?</translatorcomment>
        <translation>B.M.I.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1352"/>
        <source>Bookmarks</source>
        <translation>Bladwijzers</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1373"/>
        <source>Add Bookmark</source>
        <translation>Bladwijzer toevoegen</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1396"/>
        <source>Starts</source>
        <translatorcomment>WJG: er wordt een punt in de tijd mee aangegeven vanaf wanneer je opmerkingen wilt plaatsen</translatorcomment>
        <translation>Vanaf</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1409"/>
        <source>Remove Bookmark</source>
        <translation>Bladwijzer verwijderen</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1502"/>
        <source>Flags</source>
        <translation>Markeringen</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1554"/>
        <source>Graphs</source>
        <translation>Grafieken</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1579"/>
        <source>Show/hide available graphs.</source>
        <translation>Toon/verberg grafieken.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="220"/>
        <source>Breakdown</source>
        <translatorcomment>Niet gezien</translatorcomment>
        <translation>Verdeling</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="220"/>
        <source>events</source>
        <translation>incidenten</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="231"/>
        <source>UF1</source>
        <translatorcomment>Letters in de cirkelgrafiek</translatorcomment>
        <translation>UF1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="232"/>
        <source>UF2</source>
        <translatorcomment>Letters in de cirkelgrafiek</translatorcomment>
        <translation>UF2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="291"/>
        <source>Time at Pressure</source>
        <translation>Tijdsduur bij Druk</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="533"/>
        <source>No %1 events are recorded this day</source>
        <translation>Er zijn vandaag geen %1 incidenten geweest</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="639"/>
        <source>%1 event</source>
        <translation>%1 incident</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="640"/>
        <source>%1 events</source>
        <translation>%1 incidenten</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1145"/>
        <source>%1%2</source>
        <translation>%1%2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1222"/>
        <source>Total time in apnea</source>
        <translation>Totale Tijd in Apneu (TTiA)</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1232"/>
        <source>Time over leak redline</source>
        <translation>Tijdsduur boven de rode leklimiet</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1428"/>
        <source>BRICK! :(</source>
        <translation>BAKSTEEN!  :(</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1472"/>
        <source>Event Breakdown</source>
        <translation>Verdeling incidenten</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1507"/>
        <source>Sessions all off!</source>
        <translatorcomment>Niet gevonden</translatorcomment>
        <translation>Alle sessies uit!</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1509"/>
        <source>Sessions exist for this day but are switched off.</source>
        <translation>Er zijn wel sessies, maar die staan uit.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1512"/>
        <source>Impossibly short session</source>
        <translation>Onmogelijk korte sessie</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1513"/>
        <source>Zero hours??</source>
        <translation>Nul uren???</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1516"/>
        <source>BRICK :(</source>
        <translatorcomment>Arie: Als er niets uit komt is het echt fout
Volgens mij zit er een foutje in deze string: dat eerste ( hoort er niet in dacht ik...
Oh, dat is een smiley   ;-)</translatorcomment>
        <translation>BAKSTEEN :(</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1518"/>
        <source>Complain to your Equipment Provider!</source>
        <translation>Klaag bij uw leverancier!</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1140"/>
        <source>Statistics</source>
        <translation>Statistieken</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1068"/>
        <source>Oximeter Information</source>
        <translation>Oxymeterinformatie</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="677"/>
        <source>Session Start Times</source>
        <translation>Starttijden</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="678"/>
        <source>Session End Times</source>
        <translation>Stoptijden</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="881"/>
        <source>Duration</source>
        <translation>Tijdsduur</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="867"/>
        <source>Position Sensor Sessions</source>
        <translation>Sessies met positie-sensor</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="872"/>
        <source>Unknown Session</source>
        <translation>Onbekende sessie</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="899"/>
        <source>Click to %1 this session.</source>
        <translation>Klik om deze sessie %1 te zetten.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="899"/>
        <source>disable</source>
        <translation>uit</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="899"/>
        <source>enable</source>
        <translation>aan</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="915"/>
        <source>%1 Session #%2</source>
        <translation>%1 Sessie #%2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="916"/>
        <source>%1h %2m %3s</source>
        <translation>%1u %2m %3s</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="930"/>
        <source>One or more waveform record(s) for this session had faulty source data. Some waveform overlay points may not match up correctly.</source>
        <translation>Een of meer golfvormgegevens had foutieve brongegevens. Sommige kunnen niet goed aansluiten.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="948"/>
        <source>Machine Settings Unavailable</source>
        <translation>Geen apparaat-instellingen beschikbaar</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1099"/>
        <source>PAP Mode: %1</source>
        <translation>Soort apparaat: %1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1238"/>
        <source>Total ramp time</source>
        <translation>Totale aanlooptijd</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1242"/>
        <source>Time outside of ramp</source>
        <translation>Tijd na aanloop</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1283"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1283"/>
        <source>End</source>
        <translation>Einde</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1485"/>
        <source>Unable to display Pie Chart on this system</source>
        <translation>Kan op dit systeem het taartdiagram niet tonen</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1517"/>
        <source>Sorry, this machine only provides compliance data.</source>
        <translation>Sorry, dit apparaat geeft uitsluitend gegevens over therapietrouw.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1536"/>
        <source>&quot;Nothing&apos;s here!&quot;</source>
        <translation>&quot;Er is hier niets!&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1072"/>
        <source>SpO2 Desaturations</source>
        <translatorcomment>WJG: hoofdletter D?</translatorcomment>
        <translation>SpO2 desaturaties</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1073"/>
        <source>Pulse Change events</source>
        <translatorcomment>AK: Oei! Bedoeld worden plotselinge, kortdurende wijzigingen in de polsslag. Maar hoe maak je dat kort?</translatorcomment>
        <translation>Polsslag incidenten</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1074"/>
        <source>SpO2 Baseline Used</source>
        <translatorcomment>WJG: hoofdletter B?</translatorcomment>
        <translation>SpO2 basislijn gebruikt</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="942"/>
        <source>Machine Settings</source>
        <translation>Apparaatinstellingen</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="128"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="838"/>
        <source>Session Information</source>
        <translation>Sessie-informatie</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="858"/>
        <source>CPAP Sessions</source>
        <translation>CPAP-sessies</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="861"/>
        <source>Oximetry Sessions</source>
        <translation>Oxymetrie sessies</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="864"/>
        <source>Sleep Stage Sessions</source>
        <translation>Slaapfase sessies</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="946"/>
        <source>&lt;b&gt;Please Note:&lt;/b&gt; All settings shown below are based on assumptions that nothing&apos;s changed since previous days.</source>
        <translation>&lt;b&gt;Let op:&lt;/b&gt; Alle onderstaande instellingen zijn gebaseerd op de aanname dat er niet is veranderd.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1094"/>
        <source>Model %1 - %2</source>
        <translation>Model %1 - %2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1103"/>
        <source>(Mode/Pressure settings are guessed on this day.)</source>
        <translation>(Gegevens over druk, soort PAP en instellingen ontbreken voor deze dag.)</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1212"/>
        <source>This day just contains summary data, only limited information is available.</source>
        <translation>Van deze dag zijn alleen overzichtsgegevens beschikbaar.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1539"/>
        <source>No data is available for this day.</source>
        <translation>Geen gegevens beschikbaar.voor deze dag.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1831"/>
        <source>Pick a Colour</source>
        <translation>Kies een kleur</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2092"/>
        <source>This bookmarked is in a currently disabled area..</source>
        <translation>Deze bladwijzer staat in een uitgeschakeld gebied..</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2135"/>
        <source>Bookmark at %1</source>
        <translation>Bladwijzer bij %1</translation>
    </message>
</context>
<context>
    <name>ExportCSV</name>
    <message>
        <location filename="../oscar/exportcsv.ui" line="14"/>
        <source>Export as CSV</source>
        <translation>Exporteer naar .csv bestand</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="24"/>
        <source>Dates:</source>
        <translation>Periode:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="37"/>
        <source>Resolution:</source>
        <translation>Resolutie:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="46"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="53"/>
        <source>Sessions</source>
        <translation>Sessies</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="60"/>
        <source>Daily</source>
        <translation>Dagrapport</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="85"/>
        <source>Filename:</source>
        <translation>Bestandsnaam:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="107"/>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="114"/>
        <source>Export</source>
        <translation>Exporteren</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="131"/>
        <source>Start:</source>
        <translation>Start:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="154"/>
        <source>End:</source>
        <translation>Einde:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="192"/>
        <source>Quick Range:</source>
        <translation>Snelkeuze:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="200"/>
        <location filename="../oscar/exportcsv.cpp" line="60"/>
        <location filename="../oscar/exportcsv.cpp" line="122"/>
        <source>Most Recent Day</source>
        <translatorcomment>WJG: zie ook bij Daily</translatorcomment>
        <translation>Meest recente dag</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="205"/>
        <location filename="../oscar/exportcsv.cpp" line="125"/>
        <source>Last Week</source>
        <translation>Afgelopen week</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="210"/>
        <location filename="../oscar/exportcsv.cpp" line="128"/>
        <source>Last Fortnight</source>
        <translation>Afgelopen twee weken</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="215"/>
        <location filename="../oscar/exportcsv.cpp" line="131"/>
        <source>Last Month</source>
        <translation>Afgelopen maand</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="220"/>
        <location filename="../oscar/exportcsv.cpp" line="134"/>
        <source>Last 6 Months</source>
        <translation>Afgelopen halfjaar</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="225"/>
        <location filename="../oscar/exportcsv.cpp" line="137"/>
        <source>Last Year</source>
        <translatorcomment>AK: Bij vorig jaar denk ik nu aan 2012. Bedoeld wordt een jaar voor gisteren.... Dat is toch afgelopen jaar?</translatorcomment>
        <translation>Afgelopen jaar</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="230"/>
        <location filename="../oscar/exportcsv.cpp" line="119"/>
        <source>Everything</source>
        <translation>Alles</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="235"/>
        <location filename="../oscar/exportcsv.cpp" line="108"/>
        <source>Custom</source>
        <translation>Zelf kiezen</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="73"/>
        <source>OSCAR_</source>
        <translation>OSCAR_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="76"/>
        <source>Details_</source>
        <translation>Details_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="78"/>
        <source>Sessions_</source>
        <translatorcomment>WJG: Engels is meervoud
AK: Wat betekent het streepje erachter?
Het zit in de bestandsnaam, het streepje is een spatie</translatorcomment>
        <translation>Sessies_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="80"/>
        <source>Summary_</source>
        <translation>Overzicht_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="87"/>
        <source>Select file to export to</source>
        <translation>Kies exportbestand</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="88"/>
        <source>CSV Files (*.csv)</source>
        <translatorcomment>bestandstype</translatorcomment>
        <translation>CSV bestanden (*.csv)</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>DateTime</source>
        <translation>Datum-Tijd</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Session</source>
        <translation>Sessie</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>Event</source>
        <translation>Incident</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>Data/Duration</source>
        <translation>Gegevens/duur</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <source>Session Count</source>
        <translation>Aantal sessies</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>End</source>
        <translation>Einde</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="207"/>
        <location filename="../oscar/exportcsv.cpp" line="210"/>
        <source>Total Time</source>
        <translation>Totale tijdsduur</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="207"/>
        <location filename="../oscar/exportcsv.cpp" line="210"/>
        <source>AHI</source>
        <translation>AHI</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="214"/>
        <source> Count</source>
        <translation> Aantal</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="222"/>
        <source>%1% </source>
        <translation>%1% </translation>
    </message>
</context>
<context>
    <name>FPIconLoader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="139"/>
        <source>Import Error</source>
        <translation>Importfout</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="140"/>
        <source>This Machine Record cannot be imported in this profile.</source>
        <translation>Deze apparaatgegevens kunnen niet in dit profiel worden geimporteerd.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="140"/>
        <source>The Day records overlap with already existing content.</source>
        <translation>De gegevens van deze dag overlappen met bestaande gegevens.</translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="../oscar/help.ui" line="20"/>
        <source>Form</source>
        <translation>Formulier</translation>
    </message>
    <message>
        <location filename="../oscar/help.ui" line="92"/>
        <source>Hide this message</source>
        <translation>Verberg dit bericht</translation>
    </message>
    <message>
        <location filename="../oscar/help.ui" line="198"/>
        <source>Search Topic:</source>
        <translation>Zoek onderwerp:</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="58"/>
        <source>Help Files are not yet available for %1 and will display in %2.</source>
        <translation>Help bestanden zijn niet beschikbaar voor %1 en zullen komen in %2.</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="68"/>
        <source>Help files do not appear to be present.</source>
        <translation>Er zijn blijkbaar geen help bestanden.</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="84"/>
        <source>HelpEngine did not set up correctly</source>
        <translation>Help is niet goed geinstalleerd</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="99"/>
        <source>HelpEngine could not register documentation correctly.</source>
        <translation>Help kon de documentatie niet vinden.</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="110"/>
        <source>Contents</source>
        <translation>Inhoud</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="111"/>
        <source>Index</source>
        <translation>Index</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="115"/>
        <source>Search</source>
        <translation>Zoeken</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="125"/>
        <source>No documentation available</source>
        <translation>Geen documentatie beschikbaar</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="213"/>
        <source>Please wait a bit.. Indexing still in progress</source>
        <translation>Even wachten, ben nog bezig</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="239"/>
        <source>No</source>
        <translation>Nee</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="240"/>
        <source>%1 result(s) for &quot;%2&quot;</source>
        <translation>%1 resultaten voor &quot;%2&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="241"/>
        <source>clear</source>
        <translation>wissen</translation>
    </message>
</context>
<context>
    <name>MD300W1Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="165"/>
        <source>Could not find the oximeter file:</source>
        <translation>Kan het oxymeter bestand niet vinden:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="171"/>
        <source>Could not open the oximeter file:</source>
        <translation>Kan het oxymeter bestand niet openen:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../oscar/mainwindow.ui" line="942"/>
        <source>&amp;Statistics</source>
        <translation>&amp;Statistieken</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="996"/>
        <source>Report Mode</source>
        <translation>Soort rapportage</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1003"/>
        <source>Standard</source>
        <translation>Standaard layout</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1013"/>
        <source>Monthly</source>
        <translation>Maand layout</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1020"/>
        <source>Date Range</source>
        <translation>Tijdspanne</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1391"/>
        <source>Statistics</source>
        <translation>Statistieken</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1441"/>
        <source>Daily</source>
        <translation>Dagrapport</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1485"/>
        <source>Overview</source>
        <translation>Overzicht</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1529"/>
        <location filename="../oscar/mainwindow.cpp" line="1112"/>
        <source>Oximetry</source>
        <translation>Oxymetrie</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1579"/>
        <source>Import</source>
        <translation>Importeren</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1629"/>
        <source>Help</source>
        <translation>Over OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2805"/>
        <source>&amp;File</source>
        <translatorcomment>WJG: Onderstreepte letter kan geen B zijn, is al gebruikt bij Bladwijzers
AK: Dan zou ik het andersom doen: B&amp;ladwijzers</translatorcomment>
        <translation>&amp;Bestand</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2830"/>
        <source>&amp;View</source>
        <translation>&amp;Weergave</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2848"/>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2864"/>
        <source>&amp;Data</source>
        <translation>&amp;Gegevens</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2868"/>
        <source>&amp;Advanced</source>
        <translation>Ge&amp;avanceerd</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2872"/>
        <source>Purge Oximetry Data</source>
        <translation>Wis oxymetrie gegevens</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2909"/>
        <source>&amp;Import SDcard Data</source>
        <translation>Importeer &amp;SD-kaart gegevens</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2980"/>
        <source>&amp;Maximize Toggle</source>
        <translation>&amp;Schermvullend aan/uit</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3185"/>
        <source>Report an Issue</source>
        <translation>Meld een probleem</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3175"/>
        <source>CSV Export Wizard</source>
        <translation>Wizard bestandsexport</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3180"/>
        <source>Export for Review</source>
        <translation>Export voor beoordeling</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2878"/>
        <source>Purge ALL CPAP Data</source>
        <translation>Wis ALLE gegevens</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2888"/>
        <source>Rebuild CPAP Data</source>
        <translation>Herstel CPAP gegevens</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2917"/>
        <source>&amp;Preferences</source>
        <translatorcomment>WJG: i is al gebruikt bij Gegevens importeren</translatorcomment>
        <translation>I&amp;nstellingen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2922"/>
        <source>&amp;Profiles</source>
        <translation>&amp;Profielen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2927"/>
        <source>Exit</source>
        <translation>Afsluiten</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3012"/>
        <source>O&amp;ximetry Wizard</source>
        <translation>O&amp;xymetrie wizard</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3040"/>
        <source>&amp;Automatic Oximetry Cleanup</source>
        <translation>&amp;Automatisch opschonen van de oxymetrie-gegevens</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3121"/>
        <source>Toggle &amp;Line Cursor</source>
        <translation>Kies &amp;Lijn of Cursor</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="103"/>
        <source>E&amp;xit</source>
        <translation>&amp;Afsluiten</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2932"/>
        <source>View &amp;Daily</source>
        <translatorcomment>20/9 WJG: aangepast na compilatie</translatorcomment>
        <translation>&amp;Dagrapport</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2940"/>
        <source>View &amp;Overview</source>
        <translation>&amp;Overzichtpagina</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2948"/>
        <source>View &amp;Welcome</source>
        <translatorcomment>WJG: Om de al gebruikte W te omzeilen
AK: Waar staat dat Welkomst-/Startscherm???
20/9 WJG: Goeie vraag, waarschijnlijk dateert dat nog uit een oudere versie en heeft Mark dat niet opgeruimd?</translatorcomment>
        <translation variants="yes">
            <lengthvariant>&amp;Welkomstscherm</lengthvariant>
            <lengthvariant></lengthvariant>
        </translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2956"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2967"/>
        <source>Use &amp;AntiAliasing</source>
        <translation>Gebruik &amp;Anti-aliasing</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2991"/>
        <source>Show Debug Pane</source>
        <translation>Foutopsporingsvenster</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2999"/>
        <source>&amp;Reset Graph Layout</source>
        <translation>&amp;Reset alle grafieken</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3004"/>
        <source>Take &amp;Screenshot</source>
        <translation>&amp;Schermopname maken</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2809"/>
        <source>Exp&amp;ort Data</source>
        <translation>Exp&amp;orteer gegevens</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3154"/>
        <source>Daily Calendar</source>
        <translation>Dagkalender</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3162"/>
        <source>Backup &amp;Journal</source>
        <translation>&amp;Dagboek opslaan</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3170"/>
        <source>Show Performance Information</source>
        <translation>Toon informatie over prestaties</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1347"/>
        <source>Profiles</source>
        <translation>Profielen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2972"/>
        <source>&amp;About OSCAR</source>
        <translation>&amp;Over OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3020"/>
        <source>Print &amp;Report</source>
        <translation>&amp;Rapport afdrukken</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3025"/>
        <source>&amp;Edit Profile</source>
        <translation>Profiel &amp;aanpassen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3030"/>
        <source>Online Users &amp;Guide</source>
        <translation>Online &amp;gebruiksaanwijzing</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3035"/>
        <source>&amp;Frequently Asked Questions</source>
        <translation>&amp;FAQ</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3045"/>
        <source>Change &amp;User</source>
        <translation>Ander &amp;profiel</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3050"/>
        <source>Purge &amp;Current Selected Day</source>
        <translation>Wis de &amp;huidige geselecteerde dag</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3113"/>
        <source>Current Days</source>
        <translation>Huidige dagen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3061"/>
        <source>Right &amp;Sidebar</source>
        <translation>&amp;Rechter zijbalk aan/uit</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3140"/>
        <source>Daily Sidebar</source>
        <translation>Zijbalk dagrapport</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3069"/>
        <source>View S&amp;tatistics</source>
        <translation>Bekijk S&amp;tatistiek</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1320"/>
        <source>Navigation</source>
        <translation>Navigatie</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="441"/>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2746"/>
        <source>Records</source>
        <translation>Gegevens</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3072"/>
        <location filename="../oscar/mainwindow.ui" line="3075"/>
        <source>View Statistics</source>
        <translation>Bekijk Statistiek</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3108"/>
        <source>Import &amp;Somnopose Data</source>
        <translation>Importeer &amp;SomnoPose gegevens</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3083"/>
        <source>Import &amp;ZEO Data</source>
        <translation>Importeer &amp;ZEO gegevens</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3088"/>
        <source>Import RemStar &amp;MSeries Data</source>
        <translation>Importeer RemStar &amp;M-series gegevens</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3093"/>
        <source>Sleep Disorder Terms &amp;Glossary</source>
        <translation>&amp;Woordenlijst slaapaandoeningen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3098"/>
        <source>Change &amp;Language</source>
        <translation>Wijzig &amp;Taal</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3103"/>
        <source>Change &amp;Data Folder</source>
        <translation>Wijzig &amp;Gegevensmap</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1030"/>
        <source>Importing Data</source>
        <translation>Gegevens importeren</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1107"/>
        <source>CPAP</source>
        <translation>CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1751"/>
        <source>Bookmarks</source>
        <translation>Bladwijzers</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="522"/>
        <source>Profile</source>
        <translation>Profiel</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="501"/>
        <location filename="../oscar/mainwindow.cpp" line="2237"/>
        <source>Welcome</source>
        <translation>Welkom</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="100"/>
        <source>&amp;About</source>
        <translation>&amp;Over</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="644"/>
        <source>Imported %1 CPAP session(s) from

%2</source>
        <translation>%1 Sessies geimporteerd van

%2</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="644"/>
        <source>Import Success</source>
        <translation>Import gelukt</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="646"/>
        <source>Already up to date with CPAP data at

%1</source>
        <translation>Al bijgewerkt met gegevens van

%1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="646"/>
        <source>Up to date</source>
        <translation>Reeds bijgewerkt</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="648"/>
        <source>Couldn&apos;t find any valid Machine Data at

%1</source>
        <translation>Kon geen geldige gegevens vinden op

%1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="877"/>
        <source>Access to Import has been blocked while recalculations are in progress.</source>
        <translation>Tijdens een herberekening kan niet geïmporteerd worden.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="648"/>
        <source>Import Problem</source>
        <translation>Import probleem</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="916"/>
        <source>CPAP Data Located</source>
        <translation>CPAP gegevens gevonden</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="945"/>
        <source>Please remember to point the importer at the root folder or drive letter of your data-card, and not a subfolder.</source>
        <translation>LET OP: kies de hoofdmap of een drive letter, niet een submap.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="946"/>
        <source>Import Reminder</source>
        <translation>Import herinnering</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1009"/>
        <source>Processing import list...</source>
        <translation>Importlijst verwerken...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1099"/>
        <source>This software has been created to assist you in reviewing the data produced by CPAP Machines, used in the treatment of various Sleep Disorders.</source>
        <translation>Deze software is gemaakt om u te helpen bij het beoordelen van de gegevens van een CPAP, die wordt gebruikt bij de behandeling van verschillende slaapstoornissen.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1103"/>
        <source>This is a beta release, some features may not yet behave as expected.</source>
        <translation>Dit is een bètaversie, mogelijk dat bepaalde functies zich nog niet gedragen als verwacht.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1106"/>
        <source>Currenly supported machines:</source>
        <translation>Momenteel ondersteunde apparaten:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1109"/>
        <source>ResMed S9 models (CPAP, Auto, VPAP)</source>
        <translation>ResMed S9 modellen (CPAP, AutoPAP, VPAP)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1110"/>
        <source>DeVilbiss Intellipap (Auto)</source>
        <translation>DeVilbiss Intellipap (Auto)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1111"/>
        <source>Fisher &amp; Paykel ICON (CPAP, Auto)</source>
        <translation>Fisher &amp; Paykel ICON (CPAP, AutoPAP)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1113"/>
        <source>Contec CMS50D+, CMS50E and CMS50F (not 50FW) Oximeters</source>
        <translation>Contec CMS50D+, CMS50E en CMS50F (niet de 50FW) Oxymeters</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1114"/>
        <source>ResMed S9 Oximeter Attachment</source>
        <translation>ResMed S9 Oxymeter adapter</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1115"/>
        <source>Online Help Resources</source>
        <translation>Online hulpbronnen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1116"/>
        <source>Note:</source>
        <translation>Let op:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1117"/>
        <source>I don&apos;t recommend using this built in web browser to do any major surfing in, it will work, but it&apos;s mainly meant as a help browser.</source>
        <translation>Deze ingebouwde webbrowser is niet geschikt op uitgebreid te surfen, het werkt maar is vooral bedoeld als help browser.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1119"/>
        <source>(It doesn&apos;t support SSL encryption, so it&apos;s not a good idea to type your passwords or personal details anywhere.)</source>
        <translation>(Ondersteunt geen SSL-encryptie, dus het is geen goed idee om uw wachtwoorden of persoonlijke gegevens ergens te typen.)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1132"/>
        <source>Further Information</source>
        <translation>Verdere informatie</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1136"/>
        <source>Plus a few &lt;a href=&apos;qrc:/docs/usage.html&apos;&gt;usage notes&lt;/a&gt;, and some important information for Mac users.</source>
        <translation>Plus een paar &lt;a href=&apos;qrc:/docs/usage.html&apos;&gt;gebruiks-aantekeningen&lt;/a&gt;, en belangrijke informatie voor Mac gebruikers.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1138"/>
        <source>About &lt;a href=&apos;http://en.wikipedia.org/wiki/Sleep_apnea&apos;&gt;Sleep Apnea&lt;/a&gt; on Wikipedia</source>
        <translation>Informatie over &lt;a href=&apos;http://nl.wikipedia.org/wiki/Slaapapneu&apos;&gt;Slaapapneu&lt;/a&gt; op de Nederlandse Wikipedia</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1141"/>
        <source>Friendly forums to talk and learn about Sleep Apnea:</source>
        <translation>Vriendelijke forums om te praten en leren over slaapapneu:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1142"/>
        <source>&lt;a href=&apos;http://www.cpaptalk.com&apos;&gt;CPAPTalk Forum&lt;/a&gt;,</source>
        <translation>&lt;a href=&apos;http://www.apneuvereniging.nl/forum&apos;&gt;het forum van de ApneuVereniging&lt;/a&gt; en &lt;a href=&apos;http://www.cpaptalk.com&apos;&gt;het CPAPTalk Forum&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1151"/>
        <source>Copyright:</source>
        <translation>Auteursrecht:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1153"/>
        <source>License:</source>
        <translation>Licentie:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1157"/>
        <source>DISCLAIMER:</source>
        <translation>AANSPRAKELIJKHEID:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1159"/>
        <source>This is &lt;font color=&apos;red&apos;&gt;&lt;u&gt;NOT&lt;/u&gt;&lt;/font&gt; medical software. This application is merely a data viewer, and no guarantee is made regarding accuracy or correctness of any calculations or data displayed.</source>
        <translation>Dit is GEEN MEDISCHE SOFTWARE, maar meer een onderzoeksgereedschap voor de visuele interpretatie van gegevens uit ondersteunde apparatuur.Deze software is NIET TOEPASBAAR voor medische diagnose, CPAP compliantie rapportage of vergelijkbare doelen.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1163"/>
        <source>Your doctor should always be your first and best source of guidance regarding the important matter of managing your health.</source>
        <translation>Uw arts is altijd de eerste en beste bron van hulp voor het belangrijke onderwerp van uw gezondheid.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1165"/>
        <source>*** &lt;u&gt;Use at your own risk&lt;/u&gt; ***</source>
        <translation>*** &lt;u&gt;Het gebruik van deze software is geheel voor eigen risico&lt;/u&gt; ***</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1291"/>
        <source>Please open a profile first.</source>
        <translation>Open eerst een profiel.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1511"/>
        <source>The User&apos;s Guide will open in your default browser</source>
        <translation>De gebruikershandleiding wordt geopend in uw standaardbrowser</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1919"/>
        <source>Provided you have made &lt;i&gt;your &lt;b&gt;own&lt;/b&gt; backups for ALL of your CPAP data&lt;/i&gt;, you can still complete this operation, but you will have to restore from your backups manually.</source>
        <translation>Mits &lt;i&gt;U&lt;b&gt; zelf &lt;/b&gt; backups gemaakt hebt van AL UW CPAP gegevens &lt;/i&gt;, kunt U dit nog steeds afronden, maar U zult deze back-ups handmatig moeten terugzetten.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1920"/>
        <source>Are you really sure you want to do this?</source>
        <translation>Weet U echt zeker dat U dit wilt?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1974"/>
        <source>Note as a precaution, the backup folder will be left in place.</source>
        <translation>Ter geruststelling: de backup map blijft intakt.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1975"/>
        <source>Are you &lt;b&gt;absolutely sure&lt;/b&gt; you want to proceed?</source>
        <translation>Weet U &lt;b&gt;absoluut zeker&lt;/b&gt; dat U wilt doorgaan?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2310"/>
        <source>The Glossary will open in your default browser</source>
        <translation>De woordenlijst wordt geopend in uw standaardbrowser</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2520"/>
        <source>%1&apos;s Journal</source>
        <translation>%1&apos;s dagboek</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2522"/>
        <source>Choose where to save journal</source>
        <translation>Kies waar het dagboek moet worden opgeslagen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2522"/>
        <source>XML Files (*.xml)</source>
        <translation>XML bestanden (*.xml)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1935"/>
        <source>Because there are no internal backups to rebuild from, you will have to restore from your own.</source>
        <translation>Aangezien er geen interne backups zijn om uit te herstellen, moet je dat uit je eigen backups doen.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1936"/>
        <source>Would you like to import from your own backups now? (you will have no data visible for this machine until you do)</source>
        <translation>WilT U nu importeren vanuit uw eigen back-ups? (U heeft geen zichtbare gegevens voor dit apparaat totdat U dit doet)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1972"/>
        <source>You are about to &lt;font size=+2&gt;obliterate&lt;/font&gt; OSCAR&apos;s machine database for the following machine:&lt;/p&gt;</source>
        <translation>U staat op het punt om alle gegevens te &lt;font size=+2&gt;vernietigen&lt;/font&gt; van het volgende apparaat:&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2025"/>
        <source>A file permission error casued the purge process to fail; you will have to delete the following folder manually:</source>
        <translation>Het samenstellen is mislukt, U moet zelf de volgende map wissen:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2074"/>
        <source>No help is available.</source>
        <translation>Er is geen help bestand.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2318"/>
        <source>Donations are not implemented</source>
        <translation>Het OSCAR project heeft geen donaties nodig</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2459"/>
        <source>Are you sure you want to delete oximetry data for %1</source>
        <translation>Weet U zeker dat U de oxymetrie-gegevens van %1 wilt wissen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2461"/>
        <source>&lt;b&gt;Please be aware you can not undo this operation!&lt;/b&gt;</source>
        <translation>&lt;b&gt;Dit kan niet ongedaan worden gemaakt!&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2482"/>
        <source>Select the day with valid oximetry data in daily view first.</source>
        <translation>Selecteer eerst de dag met geldige oxymetrie-gegevens in het dagrapport.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2356"/>
        <source>There was a problem opening Somnopose Data File: </source>
        <translation>Er was een probleem met het openen van het SomnoPose gegevensbestand: </translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2360"/>
        <source>Somnopause Data Import complete</source>
        <translation>Import van SomnoPose gegevens voltooid</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1296"/>
        <source>Access to Preferences has been blocked until recalculation completes.</source>
        <translation>Toegang tot de instellingen is geblokkeerd gedurende herberekening.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="703"/>
        <location filename="../oscar/mainwindow.cpp" line="1932"/>
        <source>Please wait, importing from backup folder(s)...</source>
        <translation>Even wachten, importeren vanuit de backup-map(pen)...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="238"/>
        <source>Help Browser</source>
        <translation>Handleiding</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="312"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="483"/>
        <source>Loading profile &quot;%1&quot;</source>
        <translation>Profiel &quot;%1&quot; laden</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="804"/>
        <source>Please insert your CPAP data card...</source>
        <translation>Plaats uw cpap gegevenskaart...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="811"/>
        <source>Choose a folder</source>
        <translation>Kies een gegevensmap</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="873"/>
        <source>No profile has been selected for Import.</source>
        <translation>Er is nog geen profiel geselcteerd om te importeren.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="881"/>
        <source>Import is already running in the background.</source>
        <translation>Op de achtergrond draait al een import.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="909"/>
        <source>A %1 file structure for a %2 was located at:</source>
        <translation>Een %1 bestandsstructuur voor een %2 is gevonden op:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="911"/>
        <source>A %1 file structure was located at:</source>
        <translation>Een %1 bestandsstructuur is gevonden op:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="917"/>
        <source>Would you like to import from this location?</source>
        <translation>Wilt U vanaf deze lokatie importeren?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="920"/>
        <source>Specify</source>
        <translation>Specificeren</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1095"/>
        <source>Welcome to OSCAR</source>
        <translation>Welkom bij OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1098"/>
        <source>About OSCAR</source>
        <translation>Over OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1101"/>
        <source>OSCAR has been designed by a software developer with personal experience with a sleep disorder, and shaped by the feedback of many other willing testers dealing with similar conditions.</source>
        <translation>OSCAR is ontworpen door een softwareontwikkelaar met persoonlijke ervaring van een slaapstoornis en gevormd door de feedback van vele andere welwillende testers die omgaan met vergelijkbare omstandigheden.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1104"/>
        <source>Please report any bugs you find to the OSCAR developer&apos;s group.</source>
        <translation>Geef alle problemen die U vindt op bij de ontwikkelaarsgroep van OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1108"/>
        <source>Philips Respironics System One (CPAP Pro, Auto, BiPAP &amp; ASV models)</source>
        <translation>Philips Respironics System One (CPAP Pro, AutoPAP, BiPAP &amp; ASV modellen)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1134"/>
        <source>The release notes for this version can be found in the About OSCAR menu item.</source>
        <translation>De lijst van veranderingen staan onder het menu Over OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1154"/>
        <source>This software is released freely under the &lt;a href=&quot;qrc:/COPYING&quot;&gt;GNU Public License version 3&lt;/a&gt;.</source>
        <translation>Deze software wordt vrijgegeven onder de &lt;a href=&quot;qrc:/COPYING&quot;&gt;GNU Public License version 3&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1161"/>
        <source>The authors will NOT be held liable by anyone who harms themselves or others by use or misuse of this software.</source>
        <translation>De auteurs accepteren GEEN ENKELE AANSPRAKELIJKHEID voor schade, in welke vorm ook, door het gebruik of misbruik van deze software.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1333"/>
        <source>Updates are not yet implemented</source>
        <translation>Automatische update is nog niet geinstalleerd</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1910"/>
        <source>Please note, that this could result in loss of data if OSCAR&apos;s backups have been disabled.</source>
        <translation>Houd er rekening mee, dat dit kan leiden tot verlies van gegevens indien de interne back-ups van OSCAR op enige manier zijn uitgeschakeld of verstoord.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1519"/>
        <source>The FAQ is not yet implemented</source>
        <translation>de FAQ is nog niet geimplementeerd</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1767"/>
        <location filename="../oscar/mainwindow.cpp" line="1794"/>
        <source>If you can read this, the restart command didn&apos;t work. You will have to do it yourself manually.</source>
        <translation>Als U dit kunt lezen, heeft het herstartcommando niet gewerkt. U zult het handmatig moeten doen.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1907"/>
        <source>Are you sure you want to rebuild all CPAP data for the following machine:

</source>
        <translation>Weet U zeker dat U alle CPAP gegevens voor het volgende apparaat wilt herstellen:

</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1917"/>
        <source>For some reason, OSCAR does not have any backups for the following machine:</source>
        <translation>Om een ​​of andere reden heeft OSCAR geen back-ups voor de volgende machine:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2542"/>
        <source>Export review is not yet implemented</source>
        <translation>Exporteren is nog niet geimplementeerd</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2557"/>
        <source>Reporting issues is not yet implemented</source>
        <translation>Melden van problemen is nog niet geimplementeerd</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1143"/>
        <source>&lt;a href=&apos;http://www.apneaboard.com/forums/&apos;&gt;Apnea Board&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;http://www.apneuvereniging.nl&apos;&gt;ApneuVereniging&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1393"/>
        <source>There was an error saving screenshot to file &quot;%1&quot;</source>
        <translation>Er is iets fout gegaan bij het opslaan van een beeldschermafdruk naar het bestand &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1395"/>
        <source>Screenshot saved to file &quot;%1&quot;</source>
        <translation>Schermafbeelding bewaard als bestand &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1766"/>
        <location filename="../oscar/mainwindow.cpp" line="1793"/>
        <source>Gah!</source>
        <translation>Bah!</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2269"/>
        <source>There was a problem opening ZEO File: </source>
        <translation>Er was een probleem met het openen van het ZEO gegevensbestand: </translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2273"/>
        <source>Zeo CSV Import complete</source>
        <translation>Import van het ZEO .csv bestand voltooid</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2295"/>
        <source>There was a problem opening MSeries block File: </source>
        <translation>Er was een probleem bij het openen van het M-Series blokbestand: </translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2299"/>
        <source>MSeries Import complete</source>
        <translation>Import M-Series voltooid</translation>
    </message>
</context>
<context>
    <name>MinMaxWidget</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1964"/>
        <source>Auto-Fit</source>
        <translation>Automatisch</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1965"/>
        <source>Defaults</source>
        <translation>Standaard</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1966"/>
        <source>Override</source>
        <translation>Instellen</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1967"/>
        <source>The Y-Axis scaling mode, &apos;Auto-Fit&apos; for automatic scaling, &apos;Defaults&apos; for settings according to manufacturer, and &apos;Override&apos; to choose your own.</source>
        <translation>Instelling y-as: &apos;Automatisch&apos; om alles te zien, &apos;Standaard&apos; voor fabrieksinstelling en &apos;Instellen&apos; om zelf te kiezen.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1973"/>
        <source>The Minimum Y-Axis value.. Note this can be a negative number if you wish.</source>
        <translation>De minimale waarde. Dit mag negatief zijn als U wilt.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1974"/>
        <source>The Maximum Y-Axis value.. Must be greater than Minimum to work.</source>
        <translation>De maximale waarde. Deze moet groter zijn dan de minimale waarde.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2009"/>
        <source>Scaling Mode</source>
        <translation>Schaalinstelling</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2031"/>
        <source>This button resets the Min and Max to match the Auto-Fit</source>
        <translation>Deze knop reset de min en max waarden naar Automatisch</translation>
    </message>
</context>
<context>
    <name>NewProfile</name>
    <message>
        <location filename="../oscar/newprofile.ui" line="14"/>
        <source>Edit User Profile</source>
        <translation>Gebruikersprofiel aanpassen</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="70"/>
        <source>I agree to all the conditions above.</source>
        <translation>Ik ga akkoord met alle bovengenoemde voorwaarden.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="111"/>
        <source>User Information</source>
        <translation>Gebruikersinformatie</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="137"/>
        <source>User Name</source>
        <translation>Naam gebruiker</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="152"/>
        <source>Keep the kids out.. Nothing more.. This isn&apos;t meant to be uber security.</source>
        <translatorcomment>WJG: Mooi gevonden!</translatorcomment>
        <translation>Hou de kinderen erbuiten... niets meer of minder... 
Dit is GEEN ECHTE BEVEILIGING.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="155"/>
        <source>Password Protect Profile</source>
        <translation>Wachtwoordbeveiliging van het profiel</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="182"/>
        <source>Password</source>
        <translation>Wachtwoord</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="196"/>
        <source>...twice...</source>
        <translation>... nog eens ...</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="213"/>
        <source>Locale Settings</source>
        <translation>Landinstellingen</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="285"/>
        <source>Country</source>
        <translation>Land</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="253"/>
        <source>TimeZone</source>
        <translation>Tijdzone</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="54"/>
        <source>about:blank</source>
        <translation>about:blank</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="240"/>
        <source>DST Zone</source>
        <translation>Automatische zomertijd</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="320"/>
        <source>Personal Information (for reports)</source>
        <translation>Persoonlijke informatie (voor rapporten)</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="344"/>
        <source>First Name</source>
        <translation>Voornaam</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="354"/>
        <source>Last Name</source>
        <translation>Achternaam</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="364"/>
        <source>It&apos;s totally ok to fib or skip this, but your rough age is needed to enhance accuracy of certain calculations.</source>
        <translation>Het is OK om hier geen of een foute leeftijd in te vullen, maar deze is nodig om meer nauwkeurige berekeningen te kunnen doen.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="367"/>
        <source>D.O.B.</source>
        <translation>Geboortedatum</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="383"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Biological (birth) gender is sometimes needed to enhance the accuracy of a few calculations, feel free to leave this blank and skip any of them.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Het biologische (geboorte-)geslacht is soms nodig voor de nauwkeurigheid van berekeningen, U mag het best leeg laten of overslaan.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="386"/>
        <source>Gender</source>
        <translation>Geslacht</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="399"/>
        <source>Male</source>
        <translation>Man</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="404"/>
        <source>Female</source>
        <translation>Vrouw</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="420"/>
        <source>Height</source>
        <translation>Lengte</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="459"/>
        <source>metric</source>
        <translation>metrisch</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="464"/>
        <source>archiac</source>
        <translatorcomment>WJG: is grapje van de maker
AK: Ik heb het nu ook door!</translatorcomment>
        <translation>archaïsch</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="477"/>
        <source>Contact Information</source>
        <translation>Contactinformatie</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="504"/>
        <location filename="../oscar/newprofile.ui" line="779"/>
        <source>Address</source>
        <translation>Adres</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="521"/>
        <location filename="../oscar/newprofile.ui" line="810"/>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="531"/>
        <location filename="../oscar/newprofile.ui" line="800"/>
        <source>Phone</source>
        <translation>Telefoon</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="576"/>
        <source>CPAP Treatment Information</source>
        <translation>Informatie over de behandeling</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="603"/>
        <source>Date Diagnosed</source>
        <translation>Datum diagnose</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="617"/>
        <source>Untreated AHI</source>
        <translation>Onbehandelde AHI</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="631"/>
        <source>CPAP Mode</source>
        <translatorcomment>WJG: klopt dit wel, want Bi-level en APAP zijn in feite geen CPAP-soorten, toch? Ik geef maar wat alternatieven, ook spreekt het wel voor zich en zou je ook &apos;Soort CPAP&apos; kunnen laten staan.
20/9 WJG: Soort apparaat lijkt me prima!</translatorcomment>
        <translation variants="yes">
            <lengthvariant>Soort apparaat</lengthvariant>
            <lengthvariant></lengthvariant>
            <lengthvariant></lengthvariant>
        </translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="639"/>
        <source>CPAP</source>
        <translation>CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="644"/>
        <source>APAP</source>
        <translation>APAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="649"/>
        <source>Bi-Level</source>
        <translation>Bi-level</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="654"/>
        <source>ASV</source>
        <translatorcomment>Adaptieve ventilatie-instelling ASI?</translatorcomment>
        <translation>ASV</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="662"/>
        <source>RX Pressure</source>
        <translation>Voorgeschreven druk</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="708"/>
        <source>Doctors / Clinic Information</source>
        <translation>Specialist/ziekenhuis</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="735"/>
        <source>Doctors Name</source>
        <translation>Specialist</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="752"/>
        <source>Practice Name</source>
        <translatorcomment>WJG: zou dit niet bedoeld worden? Bij het adres wordt wel duidelijk welk ziekenhuis het is. Bij mij is de behandeling bij &apos;Longziekten&apos;, misschien heet dat bij een ander ziekenhuis anders?</translatorcomment>
        <translation>Afdeling</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="762"/>
        <source>Patient ID</source>
        <translation>Patient-ID</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="877"/>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="955"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuleren</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="971"/>
        <source>&amp;Back</source>
        <translation>&amp;Terug</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="987"/>
        <location filename="../oscar/newprofile.cpp" line="274"/>
        <location filename="../oscar/newprofile.cpp" line="283"/>
        <source>&amp;Next</source>
        <translation>&amp;Volgende</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="61"/>
        <source>Select Country</source>
        <translation>Kies land</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="111"/>
        <source>This software is being designed to assist you in reviewing the data produced by your CPAP machines and related equipment.</source>
        <translation>Deze software is ontworpen om U te helpen bij het analyseren van de gegevens van uw CPAP.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="116"/>
        <source>PLEASE READ CAREFULLY</source>
        <translation>GRAAG AANDACHTIG LEZEN</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="120"/>
        <source>Accuracy of any data displayed is not and can not be guaranteed.</source>
        <translation>De betrouwbaarheid van de weergegeven gegevens wordt en kan nooit worden gegarandeerd.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="122"/>
        <source>Any reports generated are for PERSONAL USE ONLY, and NOT IN ANY WAY fit for compliance or medical diagnostic purposes.</source>
        <translation>Alle rapportages zijn voor EIGEN GEBRUIK en kunnen ONDER GEEN VOORWAARDE worden toegepast voor medische diagnostiek.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="129"/>
        <source>Use of this software is entirely at your own risk.</source>
        <translation>Het gebruik van deze software is geheel voor eigen risico.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="109"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation>Welkom bij de Open Source CPAP Analysis Reporter</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="114"/>
        <source>OSCAR has been released freely under the &lt;a href=&apos;qrc:/COPYING&apos;&gt;GNU Public License v3&lt;/a&gt;, and comes with no warranty, and without ANY claims to fitness for any purpose.</source>
        <translation>OSCAR is vrijgegeven onder de &lt;a href=&apos;qrc:/COPYING&apos;&gt; GNU Public License v3&lt;/ a&gt;, en wordt geleverd zonder garantie en zonder enige aanspraak op geschiktheid voor enig doel.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="117"/>
        <source>OSCAR is intended merely as a data viewer, and definitely not a substitute for competent medical guidance from your Doctor.</source>
        <translation>OSCAR is voornamelijk bedoeld om gegevens te bekijken en absoluut geen vervanging voor de medische begeleiding door uw arts.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="125"/>
        <source>The authors will not be held liable for &lt;u&gt;anything&lt;/u&gt; related to the use or misuse of this software.</source>
        <translation>De auteurs accepteren GEEN ENKELE AANSPRAKELIJKHEID voor schade, in welke vorm ook, door het gebruik of misbruik van deze software.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="132"/>
        <source>OSCAR is copyright &amp;copy;2011-2018 Mark Watkins and portions &amp;copy;2019 Nightowl Software</source>
        <translation>Het auteursrecht van SleepyHead &amp;copy;2011-2018 berust bij Mark Watkins en delen &amp;copy;2019 Nightowl Software</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="155"/>
        <source>Please provide a username for this profile</source>
        <translation>Geef een gebruikersnaam voor dit profiel</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="165"/>
        <source>Passwords don&apos;t match</source>
        <translation>Wachtwoorden komen niet overeen</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="195"/>
        <source>Profile Changes</source>
        <translation>Profielwijzigingen</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="195"/>
        <source>Accept and save this information?</source>
        <translation>Opslaan?</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="272"/>
        <source>&amp;Finish</source>
        <translation>&amp;Einde</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="448"/>
        <source>&amp;Close this window</source>
        <translation>&amp;Sluit dit venster</translation>
    </message>
</context>
<context>
    <name>Overview</name>
    <message>
        <location filename="../oscar/overview.ui" line="14"/>
        <source>Form</source>
        <translation>Formulier</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="68"/>
        <source>Range:</source>
        <translation>Bereik:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="82"/>
        <source>Last Week</source>
        <translation>Afgelopen week</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="87"/>
        <source>Last Two Weeks</source>
        <translation>Afgelopen twee weken</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="92"/>
        <source>Last Month</source>
        <translation>Afgelopen maand</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="97"/>
        <source>Last Two Months</source>
        <translation>Afgelopen twee maanden</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="102"/>
        <source>Last Three Months</source>
        <translation>Afgelopen drie maanden</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="107"/>
        <source>Last 6 Months</source>
        <translation>Afgelopen halfjaar</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="112"/>
        <source>Last Year</source>
        <translation>Afgelopen jaar</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="117"/>
        <source>Everything</source>
        <translation>Alles</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="122"/>
        <source>Custom</source>
        <translation>Zelf kiezen</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="130"/>
        <source>Start:</source>
        <translation>Start:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="153"/>
        <source>End:</source>
        <translation>Einde:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="176"/>
        <source>Reset view to selected date range</source>
        <translatorcomment>WJG: is wat preciezer, als het past</translatorcomment>
        <translation>Herstel naar geselecteerd datumbereik</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="195"/>
        <location filename="../oscar/overview.ui" line="244"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="225"/>
        <source>Toggle Graph Visibility</source>
        <translation>Grafieken aan/uit</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="260"/>
        <source>Drop down to see list of graphs to switch on/off.</source>
        <translation>Lijst met grafieken om aan/uit te zetten.</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="264"/>
        <source>Graphs</source>
        <translation>Grafieken</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="160"/>
        <source>Apnea
Hypopnea
Index</source>
        <translation>Apneu
Hypopneu
Index</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="166"/>
        <source>Usage</source>
        <translation>Gebruik</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="166"/>
        <source>Usage
(hours)</source>
        <translation>Gebruik
(uren)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="177"/>
        <source>Total Time in Apnea</source>
        <translation>Totale tijd in apneu</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="177"/>
        <source>Total Time in Apnea
(Minutes)</source>
        <translation>Totale tijd in apneu
(minuten)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="169"/>
        <source>Session Times</source>
        <translation>Sessietijden</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="158"/>
        <source>Respiratory
Disturbance
Index</source>
        <translation>Ademhalings
Stoornis
Index (RDI)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="211"/>
        <source>Body
Mass
Index</source>
        <translation>Body
Mass
Index
(BMI)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="212"/>
        <source>How you felt
(0-10)</source>
        <translation>Hoe U zich voelde
(0-10)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="502"/>
        <source>Show all graphs</source>
        <translation>Alle grafieken zichtbaar</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="515"/>
        <source>Hide all graphs</source>
        <translation>Verberg alle grafieken</translation>
    </message>
</context>
<context>
    <name>OximeterImport</name>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialoog</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="61"/>
        <location filename="../oscar/oximeterimport.cpp" line="37"/>
        <source>Oximeter Import Wizard</source>
        <translation>Oxymeter import wizard</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="768"/>
        <source>Skip this page next time.</source>
        <translation>Sla deze pagina volgende keer over.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="866"/>
        <source>Where would you like to import from?</source>
        <translation>Waar wilt u vandaan importeren?</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="967"/>
        <source>CMS50E/F users, when importing directly, please don&apos;t select upload on your device until OSCAR prompts you to.</source>
        <translation>CMS50E/F gebruikers die direct importeren moeten de upload pas starten als OSCAR dat aangeeft.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1004"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If enabled, OSCAR will automatically reset your CMS50&apos;s internal clock using your computers current time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Indien ingeschakeld, zal OSCAR de interne klok van je CMS50 instellen op de tijd van uw computer.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1077"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will erase the imported session from your oximeter after import has completed. &lt;/p&gt;&lt;p&gt;Use with caution,  becauseif something goes wrong before OSCAR saves your session, you won&apos;t get it back.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Deze optie wist de geimporteerde sessie van uw oxymeter na het importeren. &lt;/p&gt;&lt;p&gt;Wees er voorzichtig mee, want als er iets fout gaat voordat OSCAR de sessie heeft opgeslagen, bent U het voorgoed kwijt.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1106"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import (via cable) from your oximeters internal recordings.&lt;/p&gt;&lt;p&gt;After selecting on this option, some oximeters will require you to do something in the devices menu to initiate the upload.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Deze optie geeft de mogelijkheid om de gegevens uit uw oxymeter (via een kabel) te importeren.&lt;/p&gt;&lt;p&gt;Na kiezen van deze optie, moet bij sommige oxymeters de upload gestart worden.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1148"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If you don&apos;t mind a being attached to a running computer overnight, this option provide a useful plethysomogram graph, which gives an indication of heart rhythm, on top of the normal oximetry readings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Als U er geen probleem mee hebt om de hele nacht aan de computer te hangen, geeft deze optie de mogelijkheid van een live &quot;plethysomogram&quot;-grafiek, met indicatie van het hartritme en natuurlijk de normale zuurstofsaturatiegrafiek.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1154"/>
        <source>Record attached to computer overnight (provides plethysomogram)</source>
        <translation>Opname direct naar de computer (geeft plethysomogram)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1187"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import from data files created by software that came with your Pulse Oximeter, such as SpO2Review.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Deze optie laat gegevensbestanden importeren, die door het programma van de oxymeter zijn gemaakt, zoals SpO2Review.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1193"/>
        <source>Import from a datafile saved by another program, like SpO2Review</source>
        <translation>Importeren van gegevensbestanden van een ander programma, zoals SpO2Review</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1272"/>
        <location filename="../oscar/oximeterimport.ui" line="1319"/>
        <source>Please connect your oximeter device</source>
        <translation>Sluit uw oxymeter aan</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1345"/>
        <source>Press Start to commence recording</source>
        <translation>Druk op Start om de opname te beginnen</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1391"/>
        <source>Show Live Graphs</source>
        <translation>Live grafieken laten zien</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1422"/>
        <location filename="../oscar/oximeterimport.ui" line="1676"/>
        <source>Duration</source>
        <translation>Tijdsduur</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1458"/>
        <source>SpO2 %</source>
        <translation>SpO2 %</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1540"/>
        <source>Pulse Rate</source>
        <translation>Polsslag</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1632"/>
        <source>Multiple Sessions Detected</source>
        <translation>Meerdere sessies gevonden</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1671"/>
        <source>Start Time</source>
        <translation>Starttijd</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1681"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1698"/>
        <source>Import Completed. When did the recording start?</source>
        <translation>Import gelukt. Wanneer is de opname gestart?</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1722"/>
        <source>Day recording (normally would of) started</source>
        <translation>De dag waarop de opname (normaal gesproken) gestart is</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1768"/>
        <source>Oximeter Starting time</source>
        <translation>Oxymeter starttijd</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1780"/>
        <source>I want to use the time reported by my oximeter&apos;s built in clock.</source>
        <translation>Ik wil de tijd van de klok van de oxymeter gebruiken.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1796"/>
        <source>I started this oximeter recording at (or near) the same time as a session on my CPAP machine.</source>
        <translation>Ik startte de oxymeter (ongeveer) tegelijk met de CPAP.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1857"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: Syncing to CPAP session starting time will always be more accurate.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Let op: Beide apparaten tegelijk starten is altijd nauwkeuriger.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1878"/>
        <source>Choose CPAP session to sync to:</source>
        <translation>Kies de CPAP-sessie waarmee moet worden gesynchroniseerd:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1917"/>
        <location filename="../oscar/oximeterimport.ui" line="1956"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1998"/>
        <source>You can manually adjust the time here if required:</source>
        <translation>U kunt hier eventueel de tijd zelf aanpassen:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2019"/>
        <source>HH:mm:ssap</source>
        <translation>uu:mm:ss AP</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2116"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuleren</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2097"/>
        <source>&amp;Information Page</source>
        <translation>&amp;Informatiepagina</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="823"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Please note: &lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Make sure your correct oximeter type is selected otherwise import will fail.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Let op: &lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Zorg dat U het juiste type van uw oxymeter hebt gekozen, anders gaat het importeren fout.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="910"/>
        <source>Select Oximeter Type:</source>
        <translation>Selecteer type oxymeter:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="921"/>
        <source>CMS50Fv3.7+/H/I, Pulox PO-400/500</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="926"/>
        <source>CMS50D+/E/F, Pulox PO-200/300</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="931"/>
        <source>ChoiceMMed MD300W1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1007"/>
        <source>Set device date/time</source>
        <translation>Zet datum en tijd van uw apparaat gelijk</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1014"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Check to enable updating the device identifier next import, which is useful for those who have multiple oximeters lying around.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sla bij de volgende import de identificatie van uw apparaat op. Dat is handig als U meerdere oxymeters hebt.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1017"/>
        <source>Set device identifier</source>
        <translation>Sla identificatie op</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1036"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Here you can enter a 7 character pet name for this oximeter.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Hier kunt U zelf een naam van maximaal 7 karakters voor deze oxymeter opgeven.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1080"/>
        <source>Erase session after successful upload</source>
        <translation>Wis sessie na succesvol importeren</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1112"/>
        <source>Import directly from a recording on a device</source>
        <translation>Importeer direct een opname uit een apparaat</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1237"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Reminder for CPAP users: &lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt;Did you remember to import your CPAP sessions first?&lt;br/&gt;&lt;/span&gt;If you forget, you won&apos;t have a valid time to sync this oximetry session to.&lt;br/&gt;To a ensure good sync between devices, always try to start both at the same time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Herinnering voor CPAP gebruikers: &lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt;Bent U niet vergeten om eerst uw CPAP sessies te importeren?&lt;br/&gt;&lt;/span&gt;Als U dat vergeet, heeft U geen geldig tijdstip om de oxymetrie sessie mee te synchroniseren.&lt;br/&gt;Om een goede synchronisatie te waarborgen, kunt U het beste proberen om beide apparaten tegelijk te starten.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1290"/>
        <source>If you can read this, you likely have your oximeter type set wrong in preferences.</source>
        <translation>Als U dit leest, heeft U waarschijnlijk in voorkeuren het verkeerde type oxymeter ingesteld.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1648"/>
        <source>Please choose which one you want to import into OSCAR</source>
        <translation>Kies welke U in OSCAR wilt importeren</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1825"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;OSCAR needs a starting time to know where to save this oximetry session to.&lt;/p&gt;&lt;p&gt;Choose one of the following options:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;OSCAR heeft een starttijd nodig om de sessie op te kunnen slaan.&lt;/p&gt;&lt;p&gt;Kies een van de volgende opties:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2135"/>
        <source>&amp;Retry</source>
        <translation>&amp;Opnieuw</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2154"/>
        <source>&amp;Choose Session</source>
        <translation>&amp;Kies sessie</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2173"/>
        <source>&amp;End Recording</source>
        <translation>&amp;Einde opname</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2192"/>
        <source>&amp;Sync and Save</source>
        <translation>&amp;Sync en sla op</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2211"/>
        <source>&amp;Save and Finish</source>
        <translation>&amp;Opslaan en afsluiten</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2230"/>
        <source>&amp;Start</source>
        <translation>&amp;Start</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="188"/>
        <source>Scanning for compatible oximeters</source>
        <translation>Zoeken naar een compatibele oxymeter</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="220"/>
        <source>Could not detect any connected oximeter devices.</source>
        <translation>Kon geen enkele oxymeter vinden.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="228"/>
        <source>Connecting to %1 Oximeter</source>
        <translation>Verbinden met de oxymeter %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="256"/>
        <source>Renaming this oximeter from &apos;%1&apos; to &apos;%2&apos;</source>
        <translation>Wijzig de naam van deze oxymeter van &apos;%1&apos; naar &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="259"/>
        <source>Oximeter name is different.. If you only have one and are sharing it between profiles, set the name to the same on both profiles.</source>
        <translation>De naam van de oxymeter is anders dan verwacht. Als U er maar een hebt die U gebruikt met meerdere profielen, zorg dat steeds dezelfde naam wordt gebruikt.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="299"/>
        <source>&quot;%1&quot;, session %2</source>
        <translation>&quot;%1&quot;, sessie %2</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="321"/>
        <source>Nothing to import</source>
        <translation>Niets te importeren</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="323"/>
        <source>Your oximeter did not have any valid sessions.</source>
        <translation>Er staan geen geldige sessies op deze oxymeter.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="324"/>
        <source>Close</source>
        <translation>Sluiten</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="327"/>
        <source>Waiting for %1 to start</source>
        <translation>Wacht op starten van %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="328"/>
        <source>Waiting for the device to start the upload process...</source>
        <translation>Wacht tot het apparaat gaat verzenden...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="330"/>
        <source>Select upload option on %1</source>
        <translation>Kies &apos;upload&apos; op de %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="364"/>
        <source>%1 device is uploading data...</source>
        <translation>Oxymeter %1 stuurt gegevens...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="365"/>
        <source>Please wait until oximeter upload process completes. Do not unplug your oximeter.</source>
        <translation>Even wachten tot de gegevensoverdracht klaar is. Houd de oxymeter aangesloten.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="384"/>
        <source>Oximeter import completed..</source>
        <translation>Import geslaagd..</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="408"/>
        <source>Select a valid oximetry data file</source>
        <translation>Kies een geldig gegevensbestand</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="408"/>
        <source>Oximetry Files (*.spo *.spor *.spo2 *.SpO2 *.dat)</source>
        <translation>Oxymetrie bestanden (*.spo, *.spor, *.spo2 *.dat)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="430"/>
        <source>No Oximetry module could parse the given file:</source>
        <translation>Er was geen oxymeter die het opgegeven bestand kon lezen:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="475"/>
        <source>Live Oximetry Mode</source>
        <translation>Directe oxymetrie-aansluiting</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="527"/>
        <source>Live Oximetry Stopped</source>
        <translation>Directe oxymetrie gestopt</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="528"/>
        <source>Live Oximetry import has been stopped</source>
        <translation>Directe oxymetrie-import is beeindigd</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1072"/>
        <source>Oximeter Session %1</source>
        <translation>Oxymetrie sessie %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1117"/>
        <source>OSCAR gives you the ability to track Oximetry data alongside CPAP session data, which can give valuable insight into the effectiveness of CPAP treatment. It will also work standalone with your Pulse Oximeter, allowing you to store, track and review your recorded data.</source>
        <translation>OSCAR maakt het mogelijk om de gegevens van de oxymeter te vergelijken met die van de CPAP. Daarmee kunt U waardevol inzicht krijgen in de effectiviteit van de CPAP behandeling. Het werkt ook met alleen de oxymeter, U kunt gegevens opslaan, volgen en bekijken.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1119"/>
        <source>OSCAR is currently compatible with Contec CMS50D+, CMS50E, CMS50F and CMS50I serial oximeters.&lt;br/&gt;(Note: Direct importing from bluetooth models is &lt;span style=&quot; font-weight:600;&quot;&gt;probably not&lt;/span&gt; possible yet)</source>
        <translation>OSCAR is momenteel compatibel met oxymeters van Contec, type CMS50D+, CMS50E, CMS50F en CMS50I&lt;br/&gt;(Let op: Importeren vanuit bluetooth modellen is &lt;span style=&quot; font-weight:600;&quot;&gt;waarschijnlijk nog niet&lt;/span&gt; mogelijk)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1128"/>
        <source>If you are trying to sync oximetry and CPAP data, please make sure you imported your CPAP sessions first before proceeding!</source>
        <translation>Als U oxymetrie en CPAP gegevens wilt synchroniseren, moet U EERST uw CPAP gegevens importeren voordat U verder gaat!</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1131"/>
        <source>For OSCAR to be able to locate and read directly from your Oximeter device, you need to ensure the correct device drivers (eg. USB to Serial UART) have been installed on your computer. For more information about this, %1click here%2.</source>
        <translation>Zorg dat U de juiste drivers hebt geinstalleerd (zoals USB naar serieel UART) om uw oxymeter te kunnen uitlezen. Voor meer informatie hierover, %1klik hier%2.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="331"/>
        <source>You need to tell your oximeter to begin sending data to the computer.</source>
        <translation>U moet upload op de oxymeter starten zodat gegevens naar de computer worden gezonden.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="333"/>
        <source>Please connect your oximeter, enter it&apos;s menu and select upload to commence data transfer...</source>
        <translation>Sluit uw oxymeter aan, ga naar het menu en selecteer upload om gegevensoverdracht te starten...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="457"/>
        <source>Oximeter not detected</source>
        <translation>Geen oxymeter gedetecteerd</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="464"/>
        <source>Couldn&apos;t access oximeter</source>
        <translation>Kon geen oxymeter benaderen</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="478"/>
        <source>Starting up...</source>
        <translation>Opstarten...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="479"/>
        <source>If you can still read this after a few seconds, cancel and try again</source>
        <translation>Als U dit na enkele seconden nog ziet, druk dan op &apos;cancel&apos; en probeer het opnieuw</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="526"/>
        <source>Live Import Stopped</source>
        <translation>Directe import beeindigd</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="578"/>
        <source>%1 session(s) on %2, starting at %3</source>
        <translation>%1 sessie(s) op %2, beginnend met %3</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="582"/>
        <source>No CPAP data available on %1</source>
        <translation>Geen CPAP gegevens beschikbaar op %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="588"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="710"/>
        <source>Recording...</source>
        <translation>Opnemen...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="717"/>
        <source>Finger not detected</source>
        <translation>Geen vinger gedetecteerd</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="817"/>
        <source>I want to use the time my computer recorded for this live oximetry session.</source>
        <translation>Ik wil de tijd van mijn computer gebruiken voor deze directe oxymetrie-sessie.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="820"/>
        <source>I need to set the time manually, because my oximeter doesn&apos;t have an internal clock.</source>
        <translation>Ik moet de tijd zelf instellen, want mijn oxymeter heeft geen klok.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="832"/>
        <source>Something went wrong getting session data</source>
        <translation>Er ging iets fout bij het ophalen van sessie-gegevens</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1113"/>
        <source>Welcome to the Oximeter Import Wizard</source>
        <translation>Welkom bij de Oxymeter import wizard</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1115"/>
        <source>Pulse Oximeters are medical devices used to measure blood oxygen saturation. During extended Apnea events and abnormal breathing patterns, blood oxygen saturation levels can drop significantly, and can indicate issues that need medical attention.</source>
        <translation>Pols oxymeters zijn medische apparaten die worden gebruikt om de zuurstofsaturatie in het bloed te meten. Gedurende langere apneus en bij abnormaal ademhalen kan de zuurstofsaturatie flink zakken. Dit kan een indicatie zijn voor noodzakelijke medische hulp.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1121"/>
        <source>You may wish to note, other companies, such as Pulox, simply rebadge Contec CMS50&apos;s under new names, such as the Pulox PO-200, PO-300, PO-400. These should also work.</source>
        <translation>Andere bedrijven, zoals Pulox plakken hun eigen naam op de Contec CMS50, zoals de Pulox PO-200, PO-300, PO-400. Deze zouden ook met dit programma moeten werken.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1124"/>
        <source>It also can read from ChoiceMMed MD300W1 oximeter .dat files.</source>
        <translation>Ook de .dat bestanden van de ChoiceMMed MD300W1 oxymeter kunnen worden ingelezen.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1126"/>
        <source>Please remember:</source>
        <translation>Niet vergeten:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1130"/>
        <source>Important Notes:</source>
        <translation>Belangrijke opmerkingen:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1133"/>
        <source>Contec CMS50D+ devices do not have an internal clock, and do not record a starting time. If you do not have a CPAP session to link a recording to, you will have to enter the start time manually after the import process is completed.</source>
        <translation>De apparaten van Contec CMS50D hebben geen interne klok en slaan dus ook de starttijd niet op. Als U geen CPAP gegevens hebt om de opname mee te verbinden, moet U zelf de starttijd ingeven nadat het importeren is voltooid.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1135"/>
        <source>Even for devices with an internal clock, it is still recommended to get into the habit of starting oximeter records at the same time as CPAP sessions, because CPAP internal clocks tend to drift over time, and not all can be reset easily.</source>
        <translation>Zelfs voor apparaten met een interne klok wordt ook aangeraden om er een gewoonte van te maken om beide apparaten tegelijk te starten. Ook omdat de klok van de CPAP langzaam verloopt en deze niet makkelijk kan worden gelijkgezet.</translation>
    </message>
</context>
<context>
    <name>Oximetry</name>
    <message>
        <location filename="../oscar/oximetry.ui" line="14"/>
        <source>Form</source>
        <translation>Formulier</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="89"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="102"/>
        <source>d/MM/yy h:mm:ss AP</source>
        <translation>dd/MM/jj uu:mm:ss AP</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="131"/>
        <source>R&amp;eset</source>
        <translation>R&amp;eset</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="160"/>
        <source>SpO2</source>
        <translation>SpO2</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="245"/>
        <source>Pulse</source>
        <translation>Pols</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="346"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="366"/>
        <source>&amp;Open .spo/R File</source>
        <translation>&amp;Open SpO/R gegevensbestand</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="385"/>
        <source>Serial &amp;Import</source>
        <translation>Seriële &amp;import</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="398"/>
        <source>&amp;Start Live</source>
        <translation>&amp;Start live</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="421"/>
        <source>Serial Port</source>
        <translation>Seriële poort</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="450"/>
        <source>&amp;Rescan Ports</source>
        <translation>&amp;Herscan poorten</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="23"/>
        <source>Preferences</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="64"/>
        <source>&amp;Import</source>
        <translation>&amp;Importeer</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="154"/>
        <source>Combine Close Sessions </source>
        <translation>Combineer nabije sessies </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="164"/>
        <location filename="../oscar/preferencesdialog.ui" line="249"/>
        <location filename="../oscar/preferencesdialog.ui" line="731"/>
        <source>Minutes</source>
        <translation>Minuten</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="184"/>
        <source>Multiple sessions closer together than this value will be kept on the same day.
</source>
        <translation>Tijd tussen sessies die nog tot dezelfde dag gerekend moeten worden.
</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="239"/>
        <source>Ignore Short Sessions</source>
        <translation>Negeer korte sessies</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="266"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Sessions shorter in duration than this will not be displayed&lt;span style=&quot; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-style:italic;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Kortere sessies worden niet weergegeven&lt;span style=&quot; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-style:italic;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="310"/>
        <source>Day Split Time</source>
        <translation>Volgende dag start om</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="320"/>
        <source>Sessions starting before this time will go to the previous calendar day.</source>
        <translation>Sessies die voor dit tijdstip gestart werden, horen bij de vorige dag.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="393"/>
        <source>Session Storage Options</source>
        <translation>Opties voor sessie-opslag</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="439"/>
        <source>Compress SD Card Backups (slower first import, but makes backups smaller)</source>
        <translation>Comprimeer SD-kaart back-ups (langzamere eerste import, maar minder opslagruimte nodig)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="417"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Changing SD Backup compression options doesn&apos;t automatically recompress backup data.  &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Wijzigen van SD-backup compressie comprimeert de back-upgegevens niet automatisch opnieuw.  &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="637"/>
        <source>&amp;CPAP</source>
        <translation>&amp;Masker en apparaat</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1267"/>
        <source>Regard days with under this usage as &quot;incompliant&quot;. 4 hours is usually considered compliant.</source>
        <translatorcomment>WJG: bij gebrek aan beter, maar &apos;niet-compliant&apos; en &apos;compliant&apos; zijn geen termen die je in Van Dale tegenkomt
Als ze het maar begrijpen, klachten mogen </translatorcomment>
        <translation>Beschouw dagen met minder gebruik als &quot;niet-therapietrouw&quot;. 4 uur wordt meestal als &quot;therapietrouw&quot; beschouwd .</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1270"/>
        <source> hours</source>
        <translation> uren</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="945"/>
        <source>Enable/disable experimental event flagging enhancements. 
It allows detecting borderline events, and some the machine missed.
This option must be enabled before import, otherwise a purge is required.</source>
        <translation>Zet experimentele incidentmarkeringen aan of uit.
Dit detecteert incidenten &apos;op het randje&apos; en door het apparaat gemiste incidenten.
Deze optie moet worden aangezet vóór het importeren, anders eerst alles wissen.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1023"/>
        <source>Flow Restriction</source>
        <translatorcomment>AK: Inderdaad, afsluiting is 0%, hier kan je kiezen,
Debietreductie
Debietbeperking
Stroombeperking
Debietbegrenzing
Doorstroombeperking</translatorcomment>
        <translation>Debietreductie</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1064"/>
        <source>Percentage of restriction in airflow from the median value. 
A value of 20% works well for detecting apneas. </source>
        <translation>Percentage van de vermindering van de luchtstroom ten opzichte van de mediane waarde. 
Een waarde van 20% werkt goed voor het opsporen van apneus. </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="987"/>
        <location filename="../oscar/preferencesdialog.ui" line="1068"/>
        <location filename="../oscar/preferencesdialog.ui" line="1545"/>
        <location filename="../oscar/preferencesdialog.ui" line="1705"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1409"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;True maximum is the maximum of the data set.&lt;/p&gt;&lt;p&gt;99th percentile filters out the rarest outliers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ware maximum is het hoogste punt van de gegevens.&lt;/p&gt;&lt;p&gt;De 99e percentiel filtert de uitschieters uit.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1427"/>
        <source>Combined Count divided by Total Hours</source>
        <translation>Totaal aantal gedeeld door aantal uren</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1432"/>
        <source>Time Weighted average of Indice</source>
        <translation>Tijdgewogen gemiddelde van de index</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1437"/>
        <source>Standard average of indice</source>
        <translation>Gewoon gemiddelde van de index</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1368"/>
        <source>Culminative Indices</source>
        <translation>Som van de indexen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="950"/>
        <source>Custom CPAP User Event Flagging</source>
        <translation>Aangepaste gebruikers markering</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1041"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:italic;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Custom flagging is an experimental method of detecting events missed by the machine. They are &lt;span style=&quot; text-decoration: underline;&quot;&gt;not&lt;/span&gt; included in AHI.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:italic;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Aangepast markeren is een experimentele werkwijze voor het detecteren van incidenten die zijn gemist door het apparaat. Ze worden &lt;span style=&quot; text-decoration: underline;&quot;&gt;niet &lt;/ span&gt; opgenomen in de AHI.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1085"/>
        <source>Duration of airflow restriction</source>
        <translatorcomment>20/9 WJG: Vanaf hier weer verder gegaan</translatorcomment>
        <translation>Duur van de vermindering van de luchtstroom</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="977"/>
        <location filename="../oscar/preferencesdialog.ui" line="1088"/>
        <location filename="../oscar/preferencesdialog.ui" line="1562"/>
        <location filename="../oscar/preferencesdialog.ui" line="1650"/>
        <location filename="../oscar/preferencesdialog.ui" line="1679"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1124"/>
        <source>Event Duration</source>
        <translation>Tijdsduur incident</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1111"/>
        <source>Allow duplicates near machine events.</source>
        <translatorcomment>20/9 WJG: Maar ik kan deze tekst niet terugvinden op het tabblad CPAP van Preferences
AK: inderdaad, vreemd</translatorcomment>
        <translation>Sta duplicaten toe vlak naast apparaat-incidenten.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1189"/>
        <source>Adjusts the amount of data considered for each point in the AHI/Hour graph.
Defaults to 60 minutes.. Highly recommend it&apos;s left at this value.</source>
        <translation>Regelt de hoeveelheid gegevens die worden beschouwd voor elk punt in de grafiek AHI/uur.
Staat standaard op 60 minuten. Sterk aanbevolen het op deze waarde te laten staan,
anders is het geen AHI/uur meer.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1193"/>
        <source> minutes</source>
        <translation> minuten</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1232"/>
        <source>Reset the counter to zero at beginning of each (time) window.</source>
        <translation>Zet de teller op nul aan het begin van elke periode.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1235"/>
        <source>Zero Reset</source>
        <translation>Telkens op nul zetten</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="684"/>
        <source>CPAP Clock Drift</source>
        <translation>Correctie afwijking klok CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="500"/>
        <source>Do not import sessions older than:</source>
        <translation>Importeer geen sessies ouder dan:</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="507"/>
        <source>Sessions older than this date will not be imported</source>
        <translation>Sessies ouder dan deze datum worden niet geimporteerd</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="533"/>
        <source>dd MMMM yyyy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1078"/>
        <source>Show in Event Breakdown Piechart</source>
        <translation>Laat zien in de incidenten grafiek</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1101"/>
        <source>#1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1007"/>
        <source>#2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1000"/>
        <source>Resync Machine Detected Events (Experimental)</source>
        <translation>Synchroniseer de door het apparaat gedetecteerde incidenten opnieuw (experimenteel)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1248"/>
        <source>User definable threshold considered large leak</source>
        <translation>Instelbare grens voor &apos;overmatige&apos; lekkage (meestal 24 l/min)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1251"/>
        <source> L/min</source>
        <translation> l/min</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1215"/>
        <source>Whether to show the leak redline in the leak graph</source>
        <translation>Of U de rode lijn in de lekgrafiek wilt zien</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1792"/>
        <location filename="../oscar/preferencesdialog.ui" line="1864"/>
        <source>Search</source>
        <translation>Zoeken</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1488"/>
        <source>&amp;Oximetry</source>
        <translation>&amp;Oxymetrie</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2364"/>
        <source>Line Thickness</source>
        <translation>Lijndikte</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2400"/>
        <source>The pixel thickness of line plots</source>
        <translation>Pixelgrootte van lijngrafieken</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2670"/>
        <source>Pixmap caching is an graphics acceleration technique. May cause problems with font drawing in graph display area on your platform.</source>
        <translation>&quot;Pixmap caching&quot; is een grafische versnellingstechniek. Kan problemen geven bij sommige teksten in de grafische omgeving.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2680"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;These features have recently been pruned. They will come back later. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Deze instellingen zijn tijdelijk uitgeschakeld. Ze komen later terug.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1640"/>
        <source>SPO2</source>
        <translation>SpO2</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1702"/>
        <source>Percentage drop in oxygen saturation</source>
        <translatorcomment>20/9 WJG: Zuurstof wellicht niet echt nodig?</translatorcomment>
        <translation>Percentage daling van zuurstofsaturatie</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1695"/>
        <source>Pulse</source>
        <translatorcomment>209/ WJG: Als &apos;t past</translatorcomment>
        <translation>Polsslag</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1660"/>
        <source>Sudden change in Pulse Rate of at least this amount</source>
        <translation>Plotselinge verandering in polsslag van tenminste deze hoeveelheid</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1552"/>
        <location filename="../oscar/preferencesdialog.ui" line="1582"/>
        <location filename="../oscar/preferencesdialog.ui" line="1663"/>
        <source> bpm</source>
        <translatorcomment>20/9 WJG: slagen per minuut</translatorcomment>
        <translation> per minuut</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1647"/>
        <source>Minimum duration of drop in oxygen saturation</source>
        <translation>Minimale duur van de verlaging</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1676"/>
        <source>Minimum duration of pulse change event.</source>
        <translation>Minimale duur van de verandering van de polsslag.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1559"/>
        <source>Small chunks of oximetry data under this amount will be discarded.</source>
        <translation>Kortdurende oxymetrie-incidenten worden verwaarloosd.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1920"/>
        <source>&amp;General</source>
        <translation>&amp;Algemeen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1949"/>
        <source>General Settings</source>
        <translation>Algemene instellingen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2690"/>
        <source>Daily view navigation buttons will skip over days without data records</source>
        <translation>De navigatieknoppen slaan de dagen zonder gegevens over</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2693"/>
        <source>Skip over Empty Days</source>
        <translation>Sla lege dagen over</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1970"/>
        <source>Allow use of multiple CPU cores where available to improve performance. 
Mainly affects the importer.</source>
        <translation>Gebruik meerdere CPU-cores voor betere prestaties.
Werkt vooral bij importeren.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1974"/>
        <source>Enable Multithreading</source>
        <translation>Multithreading inschakelen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="573"/>
        <source>Bypass the login screen and load the most recent User Profile</source>
        <translation>Sla het inlogscherm over en laad het meest recente gebruikersprofiel</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1315"/>
        <source>Changes to the following settings needs a restart, but not a recalc.</source>
        <translation>Wijzigingen in de volgende instellingen werken pas na een herstart, maar er is geen herberekening nodig.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="482"/>
        <source>Create SD Card Backups during Import (Turn this off at your own peril!)</source>
        <translation>Maak tijdens importeren een backup van de SD-kaart (Uitschakelen op eigen risico!)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1815"/>
        <location filename="../oscar/preferencesdialog.ui" line="1894"/>
        <source>Reset &amp;Defaults</source>
        <translation>&amp;Standaardinstellingen herstellen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1828"/>
        <location filename="../oscar/preferencesdialog.ui" line="1907"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;Just because you can, does not mean it&apos;s good practice.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Waarschuwing: &lt;/span&gt;Hoewel het  mogelijk is, betekent dit nog niet dat het een goede keuze is &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1318"/>
        <source>Preferred Calculation Methods</source>
        <translation>Voorkeur berekeningsmethoden</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1347"/>
        <source>Middle Calculations</source>
        <translation>Gemiddelden</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1361"/>
        <source>Upper Percentile</source>
        <translation>Bovenste percentiel</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="91"/>
        <source>Session Splitting Settings</source>
        <translation>Instelling splitsen van sessies</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="359"/>
        <source>Don&apos;t Split Summary Days (Warning: read the tooltip!)</source>
        <translation>Dagen met alleen overzichtgegevens niet splitsen (Waarschuwing: lees de knopinfo!)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="567"/>
        <source>Memory and Startup Options</source>
        <translation>Geheugen en startopties</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="609"/>
        <source>Pre-Load all summary data at startup</source>
        <translation>Laad bij het starten alle overzichtgegevens</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="596"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This setting keeps waveform and event data in memory after use to speed up revisiting days.&lt;/p&gt;&lt;p&gt;This is not really a necessary option, as your operating system caches previously used files too.&lt;/p&gt;&lt;p&gt;Recommendation is to leave it switched off, unless your computer has a ton of memory.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Deze instelling houdt de gegevens van golfvorm en gebeurtenissen in het geheugen, zodat het bij terugkeer naar dezelfde dag sneller gaat.&lt;/p&gt;&lt;p&gt;Het is niet echt noodzakelijk, want de computer doet dit zelf ook al.&lt;/p&gt;&lt;p&gt;Aanbevolen wordt dus om het uit te laten, tenzij U gigantisch veel geheugen hebt.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="599"/>
        <source>Keep Waveform/Event data in memory</source>
        <translation>Houd gegevens van golfvormen en gebeurtenissen in het geheugen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="623"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cuts down on any unimportant confirmation dialogs during import.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voorkomt onnodige vragen tijdens het importeren.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="626"/>
        <source>Import without asking for confirmation</source>
        <translation>Importeer zonder bevestiging te vragen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1153"/>
        <source>General CPAP and Related Settings</source>
        <translation>Algemene instellingen omtrent de CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1162"/>
        <source>Enable Unknown Events Channels</source>
        <translation>Zet de kanalen van onbekende gebeurtenissen aan</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1293"/>
        <source>AHI</source>
        <extracomment>Apnea Hypopnea Index</extracomment>
        <translation>AHI</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1298"/>
        <source>RDI</source>
        <extracomment>Respiratory Disturbance Index</extracomment>
        <translation>RDI</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1169"/>
        <source>AHI/Hour Graph Time Window</source>
        <translation>AHI/uur grafiek tijdvenster</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1225"/>
        <source>Preferred major event index</source>
        <translation>Voorkeur index</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1176"/>
        <source>Compliance defined as</source>
        <translation>Therapietrouw bij</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1218"/>
        <source>Flag leaks over threshold</source>
        <translation>Aangeven lek-limiet</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="765"/>
        <source>Seconds</source>
        <translation>Seconden</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="711"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: This is not intended for timezone corrections! Make sure your operating system clock and timezone is set correctly.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Let op: Dit is niet bedoeld om de tijdzone te corrigeren! Zorg dat de klok en tijdzone van de computer goed staan.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="758"/>
        <source>Hours</source>
        <translation>Uren</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1324"/>
        <source>For consistancy, ResMed users should use 95% here,
as this is the only value available on summary-only days.</source>
        <translatorcomment>20/9 WJG: koppelteken en extra woorje</translatorcomment>
        <translation>Voor consistentie moeten ResMed-gebruikers hier 95% instellen,
want dit is de enige waarde die beschikbaar is op de dagen met alleen een samenvatting.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1375"/>
        <source>Median is recommended for ResMed users.</source>
        <translation>Mediaan wordt aanbevolen voor ResMed-gebruikers.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1379"/>
        <location filename="../oscar/preferencesdialog.ui" line="1442"/>
        <source>Median</source>
        <translation>Mediaan</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1384"/>
        <source>Weighted Average</source>
        <translation>Gewogen gemiddelde</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1389"/>
        <source>Normal Average</source>
        <translation>Normaal gemiddelde</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2732"/>
        <source>Try changing this from the default setting (Desktop OpenGL) if you experience rendering problems with OSCAR&apos;s graphs.</source>
        <translation>Als U problemen hebt met grafieken, probeer dan een andere dan de standaard instelling (Desktop Open GL).</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2757"/>
        <source>Fonts (Application wide settings)</source>
        <translation>Lettertype (geldt voor hele programma)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1413"/>
        <source>True Maximum</source>
        <translation>Ware maximum</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1418"/>
        <source>99% Percentile</source>
        <translation>99% percentiel</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1354"/>
        <source>Maximum Calcs</source>
        <translation>Berekening maximum</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="431"/>
        <source>Compress ResMed (EDF) backups to save disk space.
Backed up EDF files are stored in the .gz format, 
which is common on Mac &amp; Linux platforms.. 

OSCAR can import from this compressed backup directory natively.. 
To use it with ResScan will require the .gz files to be uncompressed first..</source>
        <translation>Comprimeer ResMed (EDF) back-ups om schijfruimte te besparen.
Back-ups van EDF bestanden worden opgeslagen in het gz-formaat,
dat veel gebruikt wordt op Mac &amp; Linux-systemen ..

OSCAR kan hier rechtstreeks uit importeren,
maar voor ResScan moeten de .gz-bestanden eerst uitgepakt worden..</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="451"/>
        <source>The following options affect the amount of disk space OSCAR uses, and have an effect on how long import takes.</source>
        <translation>De volgende opties hebben effect op de gebruikte schijfruimte en op de snelheid van importeren.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="461"/>
        <source>This makes OSCAR&apos;s data take around half as much space.
But it makes import and day changing take longer.. 
If you&apos;ve got a new computer with a small solid state disk, this is a good option.</source>
        <translation>De grootte van de opslag wordt gehalveerd,
maar maakt het importeren en verwerken trager.
Als U een nieuwe computer met SSD hebt, is dit een goede keuze.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="466"/>
        <source>Compress Session Data (makes OSCAR data smaller, but day changing slower.)</source>
        <translation>Comprimeer sessiegegevens (minder opslagruimte, maar tragere verwerking.)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="473"/>
        <source>This maintains a backup of SD-card data for ResMed machines, 

ResMed S9 series machines delete high resolution data older than 7 days, 
and graph data older than 30 days..

OSCAR can keep a copy of this data if you ever need to reinstall. 
(Highly recomended, unless your short on disk space or don&apos;t care about the graph data)</source>
        <translation>Dit zorgt voor een back-up van de SD-kaart voor ResMed-apparaten,

ResMed apparaten verwijderen hoge resolutie-gegevens ouder dan 7 dagen,
en grafiekgegevens die ouder zijn dan 30 dagen..

OSCAR kan een kopie van deze gegevens bewaren voor na een herinstallatie.
(Sterk aanbevolen, tenzij U weinig schijfruimte hebt of niets om grafiekgegevens geeft)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="606"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Makes starting OSCAR a bit slower, by pre-loading all the summary data in advance, which speeds up overview browsing and a few other calculations later on. &lt;/p&gt;&lt;p&gt;If you have a large amount of data, it might be worth keeping this switched off, but if you typically like to view &lt;span style=&quot; font-style:italic;&quot;&gt;everything&lt;/span&gt; in overview, all the summary data still has to be loaded anyway. &lt;/p&gt;&lt;p&gt;Note this setting doesn&apos;t affect waveform and event data, which is always demand loaded as needed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Maakt het opstarten van OSCAR een beetje trager door vooraf alle overzichtgegevens te laden, maar verder loopt het programma daardoor wel sneller. &lt;/p&gt;&lt;p&gt;Als U erg veel gegevens hebt, kunt U dit beter uit laten, want als U &lt;span style=&quot; font-style:italic;&quot;&gt; alle overzichten&lt;/span&gt; wilt bekijken, moeten deze gegevens toch worden opgehaald. &lt;/p&gt;&lt;p&gt;Let op: dit beinvloedt niet de gegevens van golfvorm en gebeurtenissen, want die moeten toch altijd worden geladen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="997"/>
        <source>This experimental option attempts to use OSCAR&apos;s event flagging system to improve machine detected event positioning.</source>
        <translation>Deze experimentele optie probeert de incident-markeringen te gebruiken om een betere correlatie te kunnen zien.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1749"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Syncing Oximetry and CPAP Data&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;CMS50 data imported from SpO2Review (from .spoR files) or the serial import method does &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600; text-decoration: underline;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; have the correct timestamp needed to sync.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Live view mode (using a serial cable) is one way to acheive an accurate sync on CMS50 oximeters, but does not counter for CPAP clock drift.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;If you start your Oximeters recording mode at &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-style:italic;&quot;&gt;exactly &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;the same time you start your CPAP machine, you can now also achieve sync. &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;The serial import process takes the starting time from last nights first CPAP session. (Remember to import your CPAP data first!)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;.Lucida Grande UI&apos;; font-size:13pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Synchroniseer oxymetrie en CPAP gegevens&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;De CMS50 gegevens, geimporteerd uit SpO2Review (uit .spoR bestanden) en de seriele import methode hebben &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600; text-decoration: underline;&quot;&gt;niet&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; de correcte kloktijd die nodig is voor synchronisatie.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Live weergave (met een seriele/USB kabel) is een manier om een accurate synchronisatie met CMS50 oxymeters te krijgen, maar helpt ook niet tegen het verlopen van de klok van de CPAP.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Als U de opname van uw oxymeter op &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-style:italic;&quot;&gt;precies &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;hetzelfde moment start met uw CPAP, kunt U ook synchroon lopen. &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Het proces van import neemt de starttijd van de eerste CPAP sessie van de vorige nacht. (Vergeet niet om eerst uw CPAP gegevens te importeren!)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1981"/>
        <source>Show Remove Card reminder notification on OSCAR shutdown</source>
        <translation>Toon aanwijzing om de SD-kaart te verwijderen bij afsluiten</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2013"/>
        <source>Automatically Check For Updates</source>
        <translation>Automatisch controleren op updates</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2030"/>
        <source>Check for new version every</source>
        <translation>Controleer elke</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2037"/>
        <source>Sourceforge hosts this project for free.. Please be considerate of their resources..</source>
        <translation>Sourceforge hosts dit project gratis .. Maak er zorgvuldig gebruik van..</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2053"/>
        <source>days.</source>
        <translation>dagen.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2118"/>
        <source>&amp;Check for Updates now</source>
        <translation>Nu &amp;controleren</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2141"/>
        <source>Last Checked For Updates: </source>
        <translation>Laatste controle: </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2154"/>
        <source>TextLabel</source>
        <translation>Tekstlabel</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2176"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If your interested in helping test new features and bugfixes early, click here.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;But please be warned this will sometimes mean breaky code..&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>20/9 WJG: aanpsreekvorm</translatorcomment>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Als U geïnteresseerd bent in het helpen testen van nieuwe features en bugfixes, klik hier.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Maar wees gewaarschuwd: dit zal soms vastlopers betekenen!!&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2185"/>
        <source>I want to try experimental and test builds (Advanced users only please.)</source>
        <translation>Ik wil experimentele en testupdates proberen (s.v.p. alleen gevorderde gebruikers!)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2209"/>
        <source>&amp;Appearance</source>
        <translation>&amp;Uiterlijk</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2238"/>
        <source>Graph Settings</source>
        <translation>Grafiekinstellingen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2254"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Which tab to open on loading a profile. (Note: It will default to Profile if OSCAR is set to not open a profile on startup)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Welke tab openen bij het laden van een profiel. (Let op: Gaat standaard naar Profiel als is ingesteld dat geen er profiel bij de start opent)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2512"/>
        <source>Bar Tops</source>
        <translation>Staafgrafieken</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2517"/>
        <source>Line Chart</source>
        <translation>Lijngrafieken</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2607"/>
        <source>Overview Linecharts</source>
        <translation>Overzicht lijngrafieken</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2552"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This makes scrolling when zoomed in easier on sensitive bidirectional TouchPads&lt;/p&gt;&lt;p&gt;50ms is recommended value.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Dit maakt scrollen makkelijker bij een tablet,&lt;/p&gt;&lt;p&gt; 50 ms wordt aanbevolen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2434"/>
        <source>Scroll Dampening</source>
        <translation>Scrollen dempen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2354"/>
        <source>Overlay Flags</source>
        <translation>Markeringen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2380"/>
        <source>The visual method of displaying waveform overlay flags.
</source>
        <translation>De visuele methode voor het tonen van markeringen in golfvormgrafieken.
</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2385"/>
        <source>Standard Bars</source>
        <translation>Standaardbalken</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2338"/>
        <source>Graph Height</source>
        <translation>Grafiekhoogte</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2531"/>
        <source>Default display height of graphs in pixels</source>
        <translation>Standaardhoogte grafieken in pixels</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2446"/>
        <source>How long you want the tooltips to stay visible.</source>
        <translation>Hoe lang de tooltips zichtbaar moeten blijven.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1769"/>
        <source>Events</source>
        <translation>Incidenten</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1628"/>
        <source>Flag rapid changes in oximetry stats</source>
        <translation>Markeer snelle veranderingen in de oxymeter statistieken</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1539"/>
        <source>Other oximetry options</source>
        <translation>Andere oxymeter opties</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1589"/>
        <source>Flag SPO2 Desaturations Below</source>
        <translation>Markeer SpO2 desaturaties onder</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1572"/>
        <source>Discard segments under</source>
        <translation>Verwerp segmenten onder</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1609"/>
        <source>Flag Pulse Rate Above</source>
        <translation>Markeer polsslag boven</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1599"/>
        <source>Flag Pulse Rate Below</source>
        <translation>Markeer polsslag onder</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="356"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;This setting should be used with caution...&lt;/span&gt; Switching it off comes with consequences involving accuracy of summary only days, as certain calculations only work properly provided summary only sessions that came from individual day records are kept together. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;ResMed users:&lt;/span&gt; Just because it seems natural to you and I that the 12 noon session restart should be in the previous day, does not mean ResMed&apos;s data agrees with us. The STF.edf summary index format has serious weaknesses that make doing this not a good idea.&lt;/p&gt;&lt;p&gt;This option exists to pacify those who don&apos;t care and want to see this &amp;quot;fixed&amp;quot; no matter the costs, but know it comes with a cost. If you keep your SD card in every night, and import at least once a week, you won&apos;t see problems with this very often.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="781"/>
        <source>This calculation requires Total Leaks data to be provided by the CPAP machine. (Eg, PRS1, but not ResMed, which has these already)

The Unintentional Leak calculations used here are linear, they don&apos;t model the mask vent curve.

If you use a few different masks, pick average values instead. It should still be close enough.</source>
        <translation>Voor deze berekening zijn de gegevens van de totale lek nodig.(Van de Respironics, maar  ResMed  berekent dit zelf al)

De berekening van de onbedoelde lekkage gebeurt lineair, niet volgens de masker-curve.

Als U meerdere maskers gebruikt, neem dan gemiddelde waarden, dat is voldoende nauwkeurig.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="788"/>
        <source>Calculate Unintentional Leaks When Not Present</source>
        <translation>Bereken de onbedoelde lekkage als deze niet door de machine gegeven wordt</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="797"/>
        <source>Your masks vent rate at 20cmH2O pressure</source>
        <translation>De bedoelde lek van uw masker bij 20 cmH2O druk</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="853"/>
        <source>Your masks vent rate at 4cmH2O pressure</source>
        <translation>De bedoelde lek van uw masker bij 4  cmH2O druk</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="884"/>
        <source>4 cmH2O</source>
        <translation>4 cm H2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="894"/>
        <source>20 cmH2O</source>
        <translation>20 cm H2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="926"/>
        <source>Note: A linear calculation method is used. Changing these values requires a recalculation.</source>
        <translation>Let op: hier wordt een lineaire benadering gebruikt. Voor verandering van deze waarden moet worden herberekend.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1159"/>
        <source>Show flags for machine detected events that haven&apos;t been identified yet.</source>
        <translation>Zet de markeringen voor de zelf gekozen incident-vlaggen aan.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2424"/>
        <source>Tooltip Timeout</source>
        <translation>Tooltip timeout</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2504"/>
        <source>Graph Tooltips</source>
        <translation>Grafiek tekstballonnen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2390"/>
        <source>Top Markers</source>
        <translation>Top markeringen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="576"/>
        <source>Auto-Launch CPAP Importer after opening profile</source>
        <translation>Start automatisch met importeren na het openen van een profiel</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="616"/>
        <source>Automatically load last used profile on start-up</source>
        <translation>Laadt automatisch het laatste profiel bij opstarten</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1456"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note: &lt;/span&gt;Due to summary design limitations, ResMed machines do not support changing these settings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note: &lt;/span&gt;Wegens beperkingen in het ontwerp van overzichten kan dit bij ResMed apparaten niet worden gewijzigd.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1509"/>
        <source>Oximetry Settings</source>
        <translation>Oxymetrie instellingen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2244"/>
        <source>On Opening</source>
        <translation>Bij start</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2257"/>
        <location filename="../oscar/preferencesdialog.ui" line="2261"/>
        <source>Profile</source>
        <translation>Profiel</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2266"/>
        <location filename="../oscar/preferencesdialog.ui" line="2305"/>
        <source>Welcome</source>
        <translation>Welkom</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2271"/>
        <location filename="../oscar/preferencesdialog.ui" line="2310"/>
        <source>Daily</source>
        <translation>Dagrapport</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2281"/>
        <location filename="../oscar/preferencesdialog.ui" line="2320"/>
        <source>Statistics</source>
        <translation>Statistiek</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2289"/>
        <source>Switch Tabs</source>
        <translation>Wissel tabbladen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2300"/>
        <source>No change</source>
        <translation>Zelfde</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2328"/>
        <source>After Import</source>
        <translation>Na import</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2626"/>
        <source>Other Visual Settings</source>
        <translation>Overige visuele instellingen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2632"/>
        <source>Anti-Aliasing applies smoothing to graph plots.. 
Certain plots look more attractive with this on. 
This also affects printed reports.

Try it and see if you like it.</source>
        <translation>Anti-aliasing strijkt de grafieken glad.
Sommige grafieken zien er dan mooier uit.
Dit is ook van invloed op afgedrukte rapporten.

Probeer het en kijk of U het leuk vindt.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2639"/>
        <source>Use Anti-Aliasing</source>
        <translation>Gebruik Anti-aliasing</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2646"/>
        <source>Makes certain plots look more &quot;square waved&quot;.</source>
        <translation>Zorgt ervoor dat sommige grafieken  er hoekiger uitzien.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2649"/>
        <source>Square Wave Plots</source>
        <translation>Hoekige golfgrafieken</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2656"/>
        <source>Allows graphs to be &quot;screenshotted&quot; for display purposes.
The Event Breakdown PIE chart uses this method, as does
the printing code.
Unfortunately some older computers/versions of Qt can cause
this application to be unstable with this feature enabled.</source>
        <translatorcomment>20/9 WJG: ietsje duidelijker en past beter in het hulptekstje</translatorcomment>
        <translation>Toont grafieken als &quot;screenshots&quot;.
Het cirkeldiagram van de incidentgrafiek maakt hiervan gebruik,
net als de afdrukcode.
Helaas veroorzaken sommige oudere computers en versies van Qt
dat hierdoor deze toepassing instabiel wordt.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2663"/>
        <source>Show event breakdown pie chart</source>
        <translation>Toon cirkeldiagram</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2673"/>
        <source>Use Pixmap Caching</source>
        <translation>Gebruik Pixmap Caching</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2683"/>
        <source>Animations &amp;&amp; Fancy Stuff</source>
        <translation>Animaties en grappige dingen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2700"/>
        <source>Whether to allow changing yAxis scales by double clicking on yAxis labels</source>
        <translation>Toestaan om de automatische y-as instelling te wijzigen door dubbelklikken op een label</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2703"/>
        <source>Allow YAxis Scaling</source>
        <translation>Sta automatische y-as instelling toe</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2726"/>
        <source>Graphics Engine (Requires Restart)</source>
        <translation>Grafische kaart (Herstart nodig)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2792"/>
        <source>Font</source>
        <translation>Lettertype</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2811"/>
        <source>Size</source>
        <translation>Grootte</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2830"/>
        <source>Bold  </source>
        <translation>Vet  </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2852"/>
        <source>Italic</source>
        <translation>Cursief</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2865"/>
        <source>Application</source>
        <translation>Toepassing</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2929"/>
        <source>Graph Text</source>
        <translation>Grafiektekst</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2990"/>
        <source>Graph Titles</source>
        <translation>Gafiektitels</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3051"/>
        <source>Big  Text</source>
        <translation>Grote tekst</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3118"/>
        <location filename="../oscar/preferencesdialog.cpp" line="428"/>
        <location filename="../oscar/preferencesdialog.cpp" line="560"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3162"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuleren</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3169"/>
        <source>&amp;Ok</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1841"/>
        <source>Waveforms</source>
        <translation>Golfvormgrafiek</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="423"/>
        <location filename="../oscar/preferencesdialog.cpp" line="554"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="424"/>
        <location filename="../oscar/preferencesdialog.cpp" line="555"/>
        <source>Color</source>
        <translation>Kleur</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="427"/>
        <location filename="../oscar/preferencesdialog.cpp" line="559"/>
        <source>Label</source>
        <translation>Label</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="765"/>
        <source>Data Reindex Required</source>
        <translation>Gegevens opnieuw indexeren</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="766"/>
        <source>A data reindexing proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation>Herindexering van de gegevens is nodig. Dit kan een paar minuten duren.

Weet U zeker dat U deze wijzigingen wilt doorvoeren?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="772"/>
        <source>Restart Required</source>
        <translation>Herstart vereist</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="48"/>
        <source>Flag</source>
        <translation>Markering</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="49"/>
        <source>Minor Flag</source>
        <translation>Kleine vlag</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="50"/>
        <source>Span</source>
        <translation>Bereik</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="51"/>
        <source>Always Minor</source>
        <translation>Altijd klein</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="256"/>
        <source>Never</source>
        <translation>Nooit</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="426"/>
        <source>Flag Type</source>
        <translation>Soort markering</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="444"/>
        <source>CPAP Events</source>
        <translation>CPAP incidenten</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="445"/>
        <source>Oximeter Events</source>
        <translation>Oxymeter incidenten</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="446"/>
        <source>Positional Events</source>
        <translation>Positie incidenten</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="447"/>
        <source>Sleep Stage Events</source>
        <translation>Slaapfase incidenten</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="448"/>
        <source>Unknown Events</source>
        <translation>Onbekende incidenten</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="620"/>
        <source>Double click to change the descriptive name this channel.</source>
        <translation>Dubbelklik om de naam van dit kanaal te wijzigen.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="498"/>
        <location filename="../oscar/preferencesdialog.cpp" line="627"/>
        <source>Double click to change the default color for this channel plot/flag/data.</source>
        <translation>Dubbelklik om de kleur te wijzigen van dit kanaal (grafiek/markering/gegevens).</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="64"/>
        <source>&lt;p&gt;&lt;b&gt;Please Note:&lt;/b&gt; OSCAR&apos;s advanced session splitting capabilities are not possible with &lt;b&gt;ResMed&lt;/b&gt; machines due to a limitation in the way their settings and summary data is stored, and therefore they have been disabled for this profile.&lt;/p&gt;&lt;p&gt;On ResMed machines, days will &lt;b&gt;split at noon&lt;/b&gt; like in ResMed&apos;s commercial software.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Let op: &lt;/b&gt;de geavanceerde sessiesplitsingsmogelijkheden van OSCAR zijn niet mogelijk met &lt;b&gt;ResMed&lt;/b&gt;-apparaten vanwege een beperking in de manier waarop de instellingen en samenvattingsgegevens worden opgeslagen, en daarom zijn ze uitgeschakeld voor dit profiel. &lt;/p&gt;&lt;p&gt;Op ResMed-machines worden dagen &lt;b&gt;gesplitst tussen de middag&lt;/b&gt;, zoals in de commerciële software van ResMed.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="283"/>
        <location filename="../oscar/preferencesdialog.cpp" line="284"/>
        <location filename="../oscar/preferencesdialog.cpp" line="1250"/>
        <location filename="../oscar/preferencesdialog.cpp" line="1255"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2276"/>
        <location filename="../oscar/preferencesdialog.ui" line="2315"/>
        <location filename="../oscar/preferencesdialog.cpp" line="425"/>
        <location filename="../oscar/preferencesdialog.cpp" line="556"/>
        <source>Overview</source>
        <translation>Overzicht</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="490"/>
        <source>Double click to change the descriptive name the &apos;%1&apos; channel.</source>
        <translation>Dubbelklik om de beschrijving van kanaal &apos;%1&apos; te wijzigen.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="503"/>
        <source>Whether this flag has a dedicated overview chart.</source>
        <translation>Of deze vlag een eigen overzichtgrafiek heeft.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="513"/>
        <source>Here you can change the type of flag shown for this event</source>
        <translation>Hier kunt U het soort markering van dit incident wijzigen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="518"/>
        <location filename="../oscar/preferencesdialog.cpp" line="651"/>
        <source>This is the short-form label to indicate this channel on screen.</source>
        <translation>Dit is het beknopte label om dit kanaal op het scherm te tonen.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="524"/>
        <location filename="../oscar/preferencesdialog.cpp" line="657"/>
        <source>This is a description of what this channel does.</source>
        <translation>Dit is de beschrijving van wat dit kanaal doet.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="557"/>
        <source>Lower</source>
        <translation>Onderste</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="558"/>
        <source>Upper</source>
        <translation>Bovenste</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="577"/>
        <source>CPAP Waveforms</source>
        <translation>CPAP golfgrafiek</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="578"/>
        <source>Oximeter Waveforms</source>
        <translation>Oxymeter grafiek</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="579"/>
        <source>Positional Waveforms</source>
        <translation>Positie grafiek</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="580"/>
        <source>Sleep Stage Waveforms</source>
        <translation>Slaapfase grafiek</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="636"/>
        <source>Whether a breakdown of this waveform displays in overview.</source>
        <translation>Of er een verdeling van deze golfvorm wordt ge toond in de overzichtpagina.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="641"/>
        <source>Here you can set the &lt;b&gt;lower&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation>Hier kunt U de &lt;b&gt;onderste&lt;/b&gt; drempel instellen van enkele berekeningen aan de %1 grafiek</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="646"/>
        <source>Here you can set the &lt;b&gt;upper&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation>Hier kunt U de &lt;b&gt;bovenste&lt;/b&gt; drempel instellen van enkele berekeningen aan de %1 grafiek</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="756"/>
        <source>Data Processing Required</source>
        <translation>Gegevens opnieuw verwerken</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="757"/>
        <source>A data re/decompression proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation>Voor het toepassen van deze wijzigingen is de- en hercompressie vereist. Deze bewerking kan enkele minuten duren om te voltooien.

Weet U zeker dat U deze wijzigingen wilt aanbrengen?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="773"/>
        <source>One or more of the changes you have made will require this application to be restarted, in order for these changes to come into effect.

Would you like do this now?</source>
        <translation>De wijzigingen maken een herstart noodzakelijk.

Wil tU dit nu doen?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1130"/>
        <source>This may not be a good idea</source>
        <translation>Dit lijkt me niet zo&apos;n goed idee</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1131"/>
        <source>ResMed S9 machines routinely delete certain data from your SD card older than 7 and 30 days (depending on resolution).</source>
        <translation>ResMed S9 apparaten wissen bepaalde gegevens van uw SD kaart als ze ouder zijn dan 7 en 30 dagen (afhankelijk van de resolutie).</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1132"/>
        <source> If you ever need to reimport this data again (whether in OSCAR or ResScan) this data won&apos;t come back.</source>
        <translation> Als U ooit gegevens opnieuw moet inlezen (in OSCAR of in ResScan), krijgt U deze gegevens niet terug.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1133"/>
        <source> If you need to conserve disk space, please remember to carry out manual backups.</source>
        <translation> Als U zuinig moet zijn met schijfruimte, vergeet dan niet om zelf backups te maken.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1134"/>
        <source> Are you sure you want to disable these backups?</source>
        <translation> Weet U zeker dat U deze automatische backups wilt uitschakelen?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1178"/>
        <source>Switching off backups is not a good idea, because OSCAR needs these to rebuild the database if errors are found.

</source>
        <translation>Het is geen goed idee om da automatische backup-functie uit te schakelen, OSCAR heeft deze nodig voor het eventuele herstellen van de database.

</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1179"/>
        <source>Are you really sure you want to do this?</source>
        <translation>Weet U zeker dat U dit wilt?</translation>
    </message>
</context>
<context>
    <name>ProfileSelect</name>
    <message>
        <location filename="../oscar/profileselect.ui" line="14"/>
        <source>Select Profile</source>
        <translation>Kies profiel</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="115"/>
        <source>Search:</source>
        <translation>Zoeken:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="191"/>
        <source>Start with the selected user profile.</source>
        <translation>Start met het geselecteerde gebruikersprofiel.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="215"/>
        <source>Create a new user profile.</source>
        <translation>Maak een nieuw gebruikersprofiel.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="235"/>
        <source>Choose a different OSCAR data folder.</source>
        <translation>Kies een andere map met OSCAR_Data.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="274"/>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="310"/>
        <source>Click here if you didn&apos;t want to start OSCAR.</source>
        <translation>Klik hier als U OSCAR niet wilde starten.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="383"/>
        <source>The current location of OSCAR data store.</source>
        <translation>De huidige locatie van de map met OSCAR_Data.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="241"/>
        <source>&amp;Different Folder</source>
        <translation>&amp;Andere map</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="287"/>
        <source>[version]</source>
        <translation>[versie]</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="313"/>
        <source>&amp;Quit</source>
        <translation>&amp;Afsluiten</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="370"/>
        <source>Folder:</source>
        <translation>Map:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="389"/>
        <source>[data directory]</source>
        <translation>[gegevens directory]</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="218"/>
        <source>New Profile</source>
        <translation>Nieuw profiel</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="194"/>
        <source>&amp;Select User</source>
        <translation>&amp;Selecteer profiel</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="92"/>
        <source>Open Profile</source>
        <translation>Open profiel</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="93"/>
        <source>Edit Profile</source>
        <translation>Wijzig profiel</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="95"/>
        <source>Delete Profile</source>
        <translation>Wis profiel</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="139"/>
        <location filename="../oscar/profileselect.cpp" line="220"/>
        <source>Enter Password for %1</source>
        <translation>Geef wachtwoord voor %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="158"/>
        <location filename="../oscar/profileselect.cpp" line="342"/>
        <source>Incorrect Password</source>
        <translation>Verkeerd wachtwoord</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="160"/>
        <source>You entered the password wrong too many times.</source>
        <translation>U hebt te vaak een verkeerd wachtwoord getypt.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="182"/>
        <source>Enter the word DELETE below to confirm.</source>
        <translation>Typ het woord DELETE hieronder om te bevestigen.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="182"/>
        <source>You are about to destroy profile &apos;%1&apos;.</source>
        <translation>U wist nu het profiel &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="201"/>
        <source>Sorry</source>
        <translation>Sorry</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="201"/>
        <source>You need to enter DELETE in capital letters.</source>
        <translation>U moet het woord DELETE in HOOFDLETTERS intypen.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="239"/>
        <source>You entered an incorrect password</source>
        <translation>U hebt een verkeerd wachtwoord gegeven</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="242"/>
        <source>If you&apos;re trying to delete because you forgot the password, you need to delete it manually.</source>
        <translation>Als U probeert om het te wissen omdat U het wachtwoord bent vergeten, moet U de directory zelf wissen.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="255"/>
        <source>There was an error deleting the profile directory, you need to manually remove it.</source>
        <translation>Er ging iets mis bij het wissen van de profieldirectory, U moet deze zelf wissen.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="259"/>
        <source>Profile &apos;%1&apos; was succesfully deleted</source>
        <translation>Het profiel &apos;%1&apos; is succesvol gewist</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="289"/>
        <source>Create new profile</source>
        <translation>Maak nieuw profiel</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="326"/>
        <source>Enter Password</source>
        <translation>Geef wachtwoord</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="345"/>
        <source>You entered an Incorrect Password too many times. Exiting!</source>
        <translation>U typte te vaak een verkeerd wachtwoord.
Het programma wordt nu afgesloten!</translation>
    </message>
</context>
<context>
    <name>ProfileSelector</name>
    <message>
        <location filename="../oscar/profileselector.ui" line="14"/>
        <source>Form</source>
        <translation>Formulier</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="26"/>
        <source>Filter:</source>
        <translation>Filter:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="36"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="171"/>
        <source>OSCAR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="186"/>
        <source>Version</source>
        <translation>Versie</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="203"/>
        <source>&amp;Open Profile</source>
        <translation>&amp;Open profiel</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="214"/>
        <source>&amp;Edit Profile</source>
        <translation>Profiel &amp;aanpassen</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="228"/>
        <source>&amp;New Profile</source>
        <translation>&amp;Nieuw profiel</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="247"/>
        <source>Profile: None</source>
        <translation>Profiel: geen</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="268"/>
        <source>Please select or create a profile...</source>
        <translation>Selecteer of maak een profiel ...</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="319"/>
        <source>Destroy Profile</source>
        <translation>Verwijder profiel</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="88"/>
        <source>Profile</source>
        <translation>Profiel</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="89"/>
        <source>Ventilator Brand</source>
        <translation>Merk apparaat</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="90"/>
        <source>Ventilator Model</source>
        <translation>Type apparaat</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="91"/>
        <source>Other Data</source>
        <translation>Andere gegevens</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="92"/>
        <source>Last Imported</source>
        <translation>Laatste import</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="93"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="133"/>
        <location filename="../oscar/profileselector.cpp" line="304"/>
        <source>%1, %2</source>
        <translation>%2 %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="168"/>
        <source>You must create a profile</source>
        <translation>U moet een profiel aanmaken</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="231"/>
        <location filename="../oscar/profileselector.cpp" line="355"/>
        <source>Enter Password for %1</source>
        <translation>Geef wachtwoord voor %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="247"/>
        <location filename="../oscar/profileselector.cpp" line="374"/>
        <source>You entered an incorrect password</source>
        <translation>U hebt een verkeerd wachtwoord gegeven</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="250"/>
        <source>Forgot your password?</source>
        <translation>Wachtwoord vergeten?</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="250"/>
        <source>Ask on the forums how to reset it, it&apos;s actually pretty easy.</source>
        <translation>Vraag op de forums hoe U het kunt resetten, het is eigenlijk vrij eenvoudig.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="314"/>
        <source>Select a profile first</source>
        <translation>Kies eerst een profiel</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="377"/>
        <source>If you&apos;re trying to delete because you forgot the password, you need to either reset it or delete the profile folder manually.</source>
        <translation>Als u probeert te verwijderen omdat u het wachtwoord bent vergeten, moet u het resetten of de profielmap handmatig verwijderen.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="387"/>
        <source>You are about to destroy profile &apos;&lt;b&gt;%1&lt;/b&gt;&apos;.</source>
        <translation>U staat op het punt het profiel &quot;&lt;b&gt;%1&lt;/b&gt;&quot; te wissen.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="387"/>
        <source>Think carefully, as this will irretrievably delete the profile along with all &lt;b&gt;backup data&lt;/b&gt; stored under&lt;br/&gt;%2.</source>
        <translation>Denk goed na, want hierdoor wordt het profiel onherroepelijk verwijderd, samen met alle &lt;b&gt;back-upgegevens&lt;/b&gt; opgeslagen onder &lt;br/&gt;%2.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="387"/>
        <source>Enter the word &lt;b&gt;DELETE&lt;/b&gt; below (exactly as shown) to confirm.</source>
        <translation>Voer het woord &lt;b&gt;VERWIJDEREN&lt;/b&gt; hieronder in (precies zoals getoond) om te bevestigen.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="405"/>
        <source>DELETE</source>
        <translation>VERWIJDEREN</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="406"/>
        <source>Sorry</source>
        <translation>Sorry</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="406"/>
        <source>You need to enter DELETE in capital letters.</source>
        <translation>U moet het woord VERWIJDEREN in HOOFDLETTERS intypen.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="419"/>
        <source>There was an error deleting the profile directory, you need to manually remove it.</source>
        <translation>Er ging iets mis bij het wissen van de profielmap, U moet deze zelf wissen.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="423"/>
        <source>Profile &apos;%1&apos; was succesfully deleted</source>
        <translation>Het profiel &apos;%1&apos; is succesvol gewist</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="433"/>
        <source>Bytes</source>
        <translation>Bytes</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="433"/>
        <source>KB</source>
        <translation>kB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="433"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="433"/>
        <source>GB</source>
        <translation>GB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="433"/>
        <source>TB</source>
        <translation>TB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="433"/>
        <source>PB</source>
        <translation>PB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="453"/>
        <source>Summaries:</source>
        <translation>Overzichten:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="454"/>
        <source>Events:</source>
        <translation>Gebeurtenissen:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="455"/>
        <source>Backups:</source>
        <translation>Backups:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="467"/>
        <location filename="../oscar/profileselector.cpp" line="507"/>
        <source>Hide disk usage information</source>
        <translation>Verberg informatie over schijfgebruik</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="470"/>
        <source>Show disk usage information</source>
        <translation>Toon informatie over schijfgebruik</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="488"/>
        <source>Name: %1, %2</source>
        <translation>Naam: %2 %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="491"/>
        <source>Phone: %1</source>
        <translation>Telefoon: %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="494"/>
        <source>Email: &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</source>
        <translation>E-mail: &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="497"/>
        <source>Address:</source>
        <translation>Adres:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="500"/>
        <source>No profile information given</source>
        <translation>Geen profielinformatie opgegeven</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="503"/>
        <source>Profile: %1</source>
        <translation>Profiel: %1</translation>
    </message>
</context>
<context>
    <name>ProgressDialog</name>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="56"/>
        <source>Abort</source>
        <translation>Afbreken</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1246"/>
        <source>No Data</source>
        <translation>Geen gegevens</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="496"/>
        <source>&quot;</source>
        <translation>inch</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="497"/>
        <source>ft</source>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="498"/>
        <source>lb</source>
        <translation>lb</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="499"/>
        <source>oz</source>
        <translation>oz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="500"/>
        <source>Kg</source>
        <translation>kg</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="501"/>
        <source>cmH2O</source>
        <translation>cmWK</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="198"/>
        <source>Med.</source>
        <translation>Med.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="216"/>
        <source>Min: %1</source>
        <translation>Min.: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="247"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="257"/>
        <source>Min: </source>
        <translation>Min.: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="252"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="262"/>
        <source>Max: </source>
        <translation>Max.: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="266"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="270"/>
        <source>%1: </source>
        <translation>%1: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="274"/>
        <source>???: </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="281"/>
        <source>Max: %1</source>
        <translation>Max.: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="287"/>
        <source>%1 (%2 days): </source>
        <translation>%1.(%2 dagen): </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="289"/>
        <source>%1 (%2 day): </source>
        <translation>%1 (%2 dagen): </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="347"/>
        <source>% in %1</source>
        <translation>% in %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="353"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="664"/>
        <location filename="../oscar/SleepLib/common.cpp" line="502"/>
        <source>Hours</source>
        <translation>Uren</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="359"/>
        <source>Min %1</source>
        <translation>Min. %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="649"/>
        <source>
Hours: %1</source>
        <translation>
Uren:.%1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="715"/>
        <source>%1 low usage, %2 no usage, out of %3 days (%4% compliant.) Length: %5 / %6 / %7</source>
        <translation>%1 kort gebruikt, %2 niet gebruikt, op %3 dagen (%4% therapietrouw.) Tijdsduur: %5 / %6 / %7</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="796"/>
        <source>Sessions: %1 / %2 / %3 Length: %4 / %5 / %6 Longest: %7 / %8 / %9</source>
        <translation>Sessies: %1 / %2 / %3 Tijdsduur: %4 / %5 / %6 Langste: %7 / %8 / %9</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="900"/>
        <source>%1
Length: %3
Start: %2
</source>
        <translation>%1
Tijdsduur: %3
Start: %2
</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="902"/>
        <source>Mask On</source>
        <translation>Masker op</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="902"/>
        <source>Mask Off</source>
        <translation>Masker af</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="913"/>
        <source>%1
Length: %3
Start: %2</source>
        <translation>%1
Tijdsduur: %3
Start: %2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1083"/>
        <source>TTIA:</source>
        <translation>TTiA:</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1096"/>
        <source>
TTIA: %1</source>
        <translation>
TTiA: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1196"/>
        <source>%1 %2 / %3 / %4</source>
        <translation>%1 %2 / %3 / %4</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="512"/>
        <source>bpm</source>
        <translation>slagen per minuut</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="523"/>
        <source>Error</source>
        <translation>Fout</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="524"/>
        <source>Warning</source>
        <translation>Waarschuwing</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="527"/>
        <source>Please Note</source>
        <translation>LET OP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="537"/>
        <source>&amp;Yes</source>
        <translation>&amp;Ja</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="538"/>
        <source>&amp;No</source>
        <translation>&amp;Nee</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="539"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuleren</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="540"/>
        <source>&amp;Destroy</source>
        <translation>&amp;Wissen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="541"/>
        <source>&amp;Save</source>
        <translation>&amp;Opslaan</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="567"/>
        <source>Min EPAP</source>
        <translation>Min. EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="568"/>
        <source>Max EPAP</source>
        <translation>Max. EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="570"/>
        <source>Min IPAP</source>
        <translation>Min. IPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="571"/>
        <source>Max IPAP</source>
        <translation>Max. IPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="613"/>
        <source>ÇSR</source>
        <translation>CSR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="680"/>
        <source>On</source>
        <translation>Aan</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="681"/>
        <source>Off</source>
        <translation>Uit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="543"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="303"/>
        <source>BMI</source>
        <translation>BMI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="503"/>
        <source>Minutes</source>
        <translation>Minuten</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="504"/>
        <source>Seconds</source>
        <translation>Seconden</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="509"/>
        <source>Events/hr</source>
        <translation>Incidenten per uur</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="511"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="514"/>
        <source>Litres</source>
        <translation>Liters</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="515"/>
        <source>ml</source>
        <translation>ml</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="516"/>
        <source>Breaths/min</source>
        <translation>Ademhalingen per minuut</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="520"/>
        <source>Degrees</source>
        <translation>Graden</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="525"/>
        <source>Information</source>
        <translation>Informatie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="526"/>
        <source>Busy</source>
        <translation>Bezig</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="682"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3529"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="544"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="299"/>
        <source>Weight</source>
        <translation>Gewicht</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="490"/>
        <source>Software Engine</source>
        <translation>Software Engine</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="491"/>
        <source>ANGLE / OpenGLES</source>
        <translation>ANGLE / OpenGLES</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="492"/>
        <source>Desktop OpenGL</source>
        <translation>Desktop OpenGL</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="494"/>
        <source> m</source>
        <translation> m</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="495"/>
        <source> cm</source>
        <translation> cm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="505"/>
        <source>h</source>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="506"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="507"/>
        <source>s</source>
        <translation>s s</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="508"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="545"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="304"/>
        <source>Zombie</source>
        <translation>Zombie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="546"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="185"/>
        <source>Pulse Rate</source>
        <translatorcomment>20/9 WJG: overal gebruiken we polsslag - moeten we daar eigenlijk niet hartslag van maken? Dat lijkt me eigenlijk beter...
Toch maar niet (nog)</translatorcomment>
        <translation>Polsslag</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="547"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="190"/>
        <source>SpO2</source>
        <translation>SpO2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="548"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="194"/>
        <source>Plethy</source>
        <translatorcomment>20/9 WJG: Wat is dat?
AK: Het kwam me bekend voor:
plethy definition, meaning, English dictionary, synonym, see also &apos;plethora&apos;,plethoric&apos;,plenty&apos;,pleat&apos;, Reverso dictionary, English definition, English vocabulary.
DAT HAD JIJ TOCH MOETEN WETEN?
Plethysmos = toename
http://www.apneaboard.com/forums/Thread-CMS50D--3956
</translatorcomment>
        <translation>Plethy</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="552"/>
        <source>Profile</source>
        <translation>Profiel</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="556"/>
        <source>Oximeter</source>
        <translation>oxymeter</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="560"/>
        <source>Default</source>
        <translation>Standaard</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="563"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3462"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3004"/>
        <source>CPAP</source>
        <translation>CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="564"/>
        <source>BiPAP</source>
        <translation>BiPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="565"/>
        <source>Bi-Level</source>
        <translation>Bi-level</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="566"/>
        <source>EPAP</source>
        <translation>EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="569"/>
        <source>IPAP</source>
        <translation>IPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="572"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3005"/>
        <source>APAP</source>
        <translation>APAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="573"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3011"/>
        <source>ASV</source>
        <translation>ASV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="574"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="292"/>
        <source>AVAPS</source>
        <translation>AVAPS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="575"/>
        <source>ST/ASV</source>
        <translation>ST/ASV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="577"/>
        <source>Humidifier</source>
        <translation>Bevochtiger</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="579"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="146"/>
        <source>H</source>
        <translation>H</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="580"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="144"/>
        <source>OA</source>
        <translation>OA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="581"/>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="582"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="142"/>
        <source>CA</source>
        <translation>CA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="583"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="150"/>
        <source>FL</source>
        <translation>FL</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="584"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="172"/>
        <source>SA</source>
        <translation>SA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="585"/>
        <source>LE</source>
        <translation>LE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="586"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="169"/>
        <source>EP</source>
        <translation>EP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="587"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="154"/>
        <source>VS</source>
        <translation>VS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="589"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="156"/>
        <source>VS2</source>
        <translation>VS2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="590"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="152"/>
        <source>RERA</source>
        <translation>RERA (RE)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="591"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3483"/>
        <source>PP</source>
        <translation>PP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="592"/>
        <source>P</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="593"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="152"/>
        <source>RE</source>
        <translation>RE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="594"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="166"/>
        <source>NR</source>
        <translation>NR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="595"/>
        <source>NRI</source>
        <translation>NRI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="596"/>
        <source>O2</source>
        <translation>O2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="597"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="200"/>
        <source>PC</source>
        <translation>PC</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="598"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="175"/>
        <source>UF1</source>
        <translation>UF1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="599"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="178"/>
        <source>UF2</source>
        <translation>UF2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="600"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="181"/>
        <source>UF3</source>
        <translation>UF3</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="602"/>
        <source>PS</source>
        <translation>PS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="603"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="256"/>
        <source>AHI</source>
        <translation>AHI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="604"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="265"/>
        <source>RDI</source>
        <translation>RDI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="605"/>
        <source>AI</source>
        <translation>AI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="606"/>
        <source>HI</source>
        <translation>HI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="607"/>
        <source>UAI</source>
        <translation>UAI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="608"/>
        <source>CAI</source>
        <translation>CAI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="609"/>
        <source>FLI</source>
        <translation>FLI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="611"/>
        <source>REI</source>
        <translation>REI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="612"/>
        <source>EPI</source>
        <translation>EPI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="614"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="140"/>
        <source>PB</source>
        <translation>PB</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="618"/>
        <source>IE</source>
        <translation>I/E</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="619"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="241"/>
        <source>Insp. Time</source>
        <translation>Inademtijd</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="620"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="238"/>
        <source>Exp. Time</source>
        <translation>Uitademtijd</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="621"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="244"/>
        <source>Resp. Event</source>
        <translation>Ademhalings-incident</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="622"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="150"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="247"/>
        <source>Flow Limitation</source>
        <translation>Stroombeperking (FL)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="623"/>
        <source>Flow Limit</source>
        <translation>Stroomlimiet</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="624"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="172"/>
        <source>SensAwake</source>
        <translation>SensAwake</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="625"/>
        <source>Pat. Trig. Breath</source>
        <translation>Pat. Veroorz. Ademh</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="626"/>
        <source>Tgt. Min. Vent</source>
        <translation>Doel min. vent</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="627"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="250"/>
        <source>Target Vent.</source>
        <translation>Doelventilatie.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="628"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="222"/>
        <source>Minute Vent.</source>
        <translation>Minuutventilatie.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="629"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="216"/>
        <source>Tidal Volume</source>
        <translation>Teugvolume</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="630"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="225"/>
        <source>Resp. Rate</source>
        <translation>Ademtempo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="631"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="219"/>
        <source>Snore</source>
        <translation>Snurken</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="632"/>
        <source>Leak</source>
        <translation>Lekkage</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="633"/>
        <source>Leaks</source>
        <translation>Maskerlek</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="636"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="259"/>
        <source>Total Leaks</source>
        <translation>Totale lek</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="637"/>
        <source>Unintentional Leaks</source>
        <translation>Onbedoelde lek</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="638"/>
        <source>MaskPressure</source>
        <translation>Maskerdruk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="639"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="207"/>
        <source>Flow Rate</source>
        <translation>Stroomsnelheid</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="640"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="315"/>
        <source>Sleep Stage</source>
        <translation>Slaapfase</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="641"/>
        <source>Usage</source>
        <translation>Gebruik</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="642"/>
        <source>Sessions</source>
        <translation>Sessies</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="643"/>
        <source>Pr. Relief</source>
        <translation>Drukvermindering</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="529"/>
        <source>No Data Available</source>
        <translation>Geen gegevens beschikbaar</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="530"/>
        <source>Compliance Only :(</source>
        <translation>Alleen naleving ;(</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="531"/>
        <source>Graphs Switched Off</source>
        <translation>Grafieken uitgeschakeld</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="532"/>
        <source>Summary Only :(</source>
        <translation>Alleen overzichtgegevens :(</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="533"/>
        <source>Sessions Switched Off</source>
        <translation>Sessies uitgeschakeld</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="645"/>
        <source>Bookmarks</source>
        <translation>Bladwijzers</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="646"/>
        <location filename="../oscar/SleepLib/journal.cpp" line="25"/>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="647"/>
        <source>v%1</source>
        <translation>v%1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="649"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="2999"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3001"/>
        <source>Mode</source>
        <translation>Modus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="650"/>
        <source>Model</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="651"/>
        <source>Brand</source>
        <translation>Merk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="652"/>
        <source>Serial</source>
        <translation>Serienummer</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="653"/>
        <source>Series</source>
        <translation>Serie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="654"/>
        <source>Machine</source>
        <translation>Apparaat</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="655"/>
        <source>Channel</source>
        <translation>Kanaal</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="656"/>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="658"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="272"/>
        <source>Inclination</source>
        <translation>Inclinatie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="659"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="269"/>
        <source>Orientation</source>
        <translation>Orientatie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="661"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="662"/>
        <source>DOB</source>
        <translation>Geboortedatum</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="663"/>
        <source>Phone</source>
        <translation>Telefoon</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="664"/>
        <source>Address</source>
        <translation>Adres</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="665"/>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="666"/>
        <source>Patient ID</source>
        <translation>Patient-ID</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="667"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="669"/>
        <source>Bedtime</source>
        <translation>Gaan slapen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="670"/>
        <source>Wake-up</source>
        <translation>Opgestaan</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="671"/>
        <source>Mask Time</source>
        <translation>Maskertijd</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="672"/>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="124"/>
        <source>Unknown</source>
        <translation>Onbekend</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="673"/>
        <source>None</source>
        <translation>Geen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="674"/>
        <source>Ready</source>
        <translation>Klaar</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="676"/>
        <source>First</source>
        <translation>Eerste dag</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="677"/>
        <source>Last</source>
        <translation>Laatste dag</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="678"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="305"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="679"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="306"/>
        <source>End</source>
        <translation>Einde</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="683"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3530"/>
        <source>No</source>
        <translation>Nee</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="685"/>
        <source>Min</source>
        <translation>Min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="686"/>
        <source>Max</source>
        <translation>Max</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="687"/>
        <source>Med</source>
        <translation>Med</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="689"/>
        <source>Average</source>
        <translation>Gemiddeld</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="690"/>
        <source>Median</source>
        <translation>Mediaan</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="202"/>
        <location filename="../oscar/SleepLib/common.cpp" line="691"/>
        <source>Avg</source>
        <translation>Gem</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="200"/>
        <location filename="../oscar/SleepLib/common.cpp" line="692"/>
        <source>W-Avg</source>
        <translation>Gew. gem</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="549"/>
        <source>Pressure</source>
        <translation>Druk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="517"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="519"/>
        <source>Severity (0-1)</source>
        <translation>Ernst (0-1)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="551"/>
        <source>Daily</source>
        <translation>Dagrapport</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="553"/>
        <source>Overview</source>
        <translation>Overzicht</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="554"/>
        <source>Oximetry</source>
        <translation>Oxymetrie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="557"/>
        <source>Event Flags</source>
        <translation>Incident markeringen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="36"/>
        <source>Windows User</source>
        <translation>Windows-gebruiker</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="200"/>
        <source>Using </source>
        <translation>Het bestand </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="200"/>
        <source>, found SleepyHead -
</source>
        <translation>, is nog van SleepyHead -
</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="201"/>
        <source>You must run the OSCAR Migration Tool</source>
        <translation>U moet de OSCAR Migratie Tool gebruiken</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="510"/>
        <source>&lt;i&gt;Your old machine data should be regenerated provided this backup feature has not been disabled in preferences during a previous data import.&lt;/i&gt;</source>
        <translation>&lt;i&gt;Uw oude gegevens moeten worden ingelezen, als de backup-functie tenminste niet is uitgeschakeld&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="443"/>
        <source>Launching Windows Explorer failed</source>
        <translation>Het is niet gelukt om de Windows Verkenner te starten</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="444"/>
        <source>Could not find explorer.exe in path to launch Windows Explorer.</source>
        <translation>Kan explorer.exe niet in het pad vinden.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="496"/>
        <source>OSCAR (%1) needs to upgrade its database for %2 %3 %4</source>
        <translation>OSCAR (%1) moet de database voor %2 %3 %4.vernieuwen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="509"/>
        <source>&lt;b&gt;OSCAR maintains a backup of your devices data card that it uses for this purpose.&lt;/b&gt;</source>
        <translation>&lt;b&gt;OSCAR maakt een backup van uw SD-kaart voor dit doel.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="513"/>
        <source>OSCAR does not yet have any automatic card backups stored for this device.</source>
        <translation>OSCAR heeft nog geen automatische backup-functie voor dit apparaat.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="514"/>
        <source>This means you will need to import this machine data again afterwards from your own backups or data card.</source>
        <translation>Dat betekent dat U de gegevens van dit apparaat straks opnieuw van de kaart of uit een eigen backup moet inlezen.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="517"/>
        <source>Important:</source>
        <translation>Belangrijk:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="517"/>
        <source>Once you upgrade, you &lt;font size=+1&gt;can not&lt;/font&gt; use this profile with the previous version anymore.</source>
        <translation>Na deze upgrade kunt U de gegevens van dit profiel.&lt;font size=+1&gt;niet meer&lt;/font&gt; gebruiken met een eerdere versie.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="518"/>
        <source>If you are concerned, click No to exit, and backup your profile manually, before starting OSCAR again.</source>
        <translation>Als U zich zorgen maakt, klik dan op Nee om af te sluiten en maak eerst een backup van uw profiel voordat U OSCAR opnieuw start.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="519"/>
        <source>Are you ready to upgrade, so you can run the new version of OSCAR?</source>
        <translation>Bent U er klaar voor om met de nieuwe versie te gaan werken?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="533"/>
        <source>Sorry, the purge operation failed, which means this version of OSCAR can&apos;t start.</source>
        <translation>Sorry, het wissen is mislukt. Dat betekent dat deze versie van OSCAR niet kan starten.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="546"/>
        <source>Would you like to switch on automatic backups, so next time a new version of OSCAR needs to do so, it can rebuild from these?</source>
        <translation>Wilt U de automatische backup-functie inschakelen, opdat OSCAR eventueel de database kan herstellen?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="553"/>
        <source>OSCAR will now start the import wizard so you can reinstall your %1 data.</source>
        <translation>Er wordt een importhulp gestart zodat U de gegevens van uw %1 kunt inlezen.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="563"/>
        <source>OSCAR will now exit, then (attempt to) launch your computers file manager so you can manually back your profile up:</source>
        <translation>OSCAR sluit nu af en probeert het bestandsbeheer te starten, zodat U een backup van het profiel kunt maken:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="565"/>
        <source>Use your file manager to make a copy of your profile directory, then afterwards, restart OSCAR and complete the upgrade process.</source>
        <translation>Gebruik het bestandsbeheer om een copie van het profiel te maken. Start daarna OSCAR opnieuw en maak het proces verder af.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="522"/>
        <source>Machine Database Changes</source>
        <translation>Wijzigingen in de gegevens van het apparaat</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="534"/>
        <source>The machine data folder needs to be removed manually.</source>
        <translation>U moet zelf de map OSCAR_Data wissen.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="535"/>
        <source>This folder currently resides at the following location:</source>
        <translation>Deze map staat momenteel hier:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="541"/>
        <source>Rebuilding from %1 Backup</source>
        <translation>Herstellen vanuit backup %1</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="179"/>
        <source>Choose the SleepyHead data folder to migrate</source>
        <translation>Kies de map SleepyHeadData die moet worden gemigreerd</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="180"/>
        <source>or CANCEL to skip migration.</source>
        <translation>Of klik AFBREKEN om de migratie over te slaan.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="194"/>
        <source>The folder you chose does not contain valid SleepyHead data.</source>
        <translation>De map die je koos bevat geen gegevens van SleepyHead.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="195"/>
        <source>You cannot use this folder:</source>
        <translation>U kunt deze map niet gebruiken:</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="210"/>
        <source>Migrating </source>
        <translation>Ik migreer nu </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="210"/>
        <source> files</source>
        <translation> bestanden</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="211"/>
        <source>from </source>
        <translation>van </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="211"/>
        <source>to </source>
        <translation>naar </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="396"/>
        <source>OSCAR will set up a folder for your data.</source>
        <translation>OSCAR maakt een nieuwe map aan voor uw gegevens.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="397"/>
        <source>If you have been using SleepyHead, OSCAR can copy your old data to this folder later.</source>
        <translation>Als U voorheen SleepyHead heeft gebruikt, kan OSCAR uw oude gegevens eventueel later naar deze map kopiëren.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="398"/>
        <source>We suggest you use this folder: </source>
        <translation>We bevelen deze map aan: 
</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="399"/>
        <source>Click Ok to accept this, or No if you want to use a different folder.</source>
        <translation>Klik op OK om dit te accepteren of Nee als u een andere map wilt gebruiken.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="410"/>
        <source>Next time you run OSCAR, you will be asked again.</source>
        <translation>De volgende keer als U OSCAR gebruikt, wordt dit u weer gevraagd.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="405"/>
        <source>Choose or create a new folder for OSCAR data</source>
        <translation>Kies of maak een nieuwe map voor OSCAR_Data</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="409"/>
        <source>As you did not select a data folder, OSCAR will exit.</source>
        <translation>Omdat U geen map voor gegevensopslag hebt gekozen, wordt OSCAR nu afgesloten.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="421"/>
        <source>The folder you chose is not empty, nor does it already contain valid OSCAR data.</source>
        <translation>De map die U koos is niet leeg, maar bevat ook geen gegevens van OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="443"/>
        <source>Migrate SleepyHead Data?</source>
        <translation>SleepyHeadData migreren?</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="444"/>
        <source>On the next screen OSCAR will ask you to select a folder with SleepyHead data</source>
        <translation>Op het volgende scherm zal OSCAR U vragen om een map met gegevens van SleepyHead (meestal heet die SleepyHeadData) te kiezen</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="445"/>
        <source>Click [OK] to go to the next screen or [No] if you do not wish to use any SleepyHead data.</source>
        <translation>Klik op [OK] om naar het volgende scherm te gaan of [Nee] als u de oude gegevens van SleepyHead niet wilt gebruiken.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="502"/>
        <source>The version of OSCAR you just ran is OLDER than the one used to create this data (%1).</source>
        <translation>De versie van OSCAR die U hiervoor gebruikte is OUDER dan degene waarmee U deze gegevens maakte (%1).</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="504"/>
        <source>It is likely that doing this will cause data corruption, are you sure you want to do this?</source>
        <translation>Dit geeft waarschijnlijk aanleiding tot verminkte gegevens, weet U zeker dat U dit wilt?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="522"/>
        <source>Question</source>
        <translation>Vraag</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="408"/>
        <source>Exiting</source>
        <translation>Afsluiten</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="422"/>
        <source>Are you sure you want to use this folder?</source>
        <translation>Weet U zeker dat U deze map wilt gebruiken?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1167"/>
        <source>Are you sure you want to reset all your channel colors and settings to defaults?</source>
        <translation>Weet U zeker dat U de kleuren en instellingen van alle grafieken wilt herstellen?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1220"/>
        <source>Are you sure you want to reset all your waveform channel colors and settings to defaults?</source>
        <translation>Weet U zeker dat U alle kleuren en instellingen wilt resetten?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="246"/>
        <source>RemStar Plus Compliance Only</source>
        <translation>RemStar Plus met alleen naleving</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="249"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3454"/>
        <source>RemStar Pro with C-Flex+</source>
        <translation>RemStar Pro met C-Flex+</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="252"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3455"/>
        <source>RemStar Auto with A-Flex</source>
        <translation>RemStar Auto met A-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="255"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3456"/>
        <source>RemStar BiPAP Pro with Bi-Flex</source>
        <translation>RemStar BiPAP Pro met Bi-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="258"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3457"/>
        <source>RemStar BiPAP Auto with Bi-Flex</source>
        <translation>RemStar BiPAP Auto met Bi-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="261"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3459"/>
        <source>BiPAP autoSV Advanced</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="264"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3461"/>
        <source>BiPAP AVAPS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="267"/>
        <source>Unknown Model</source>
        <translation>Onbekend model</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="275"/>
        <source>System One (60 Series)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="278"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="345"/>
        <source>DreamStation</source>
        <translation>DreamStation</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="281"/>
        <source>unknown</source>
        <translation>onbekend</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="515"/>
        <source>Getting Ready...</source>
        <translation>Voorbereiden...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="576"/>
        <source>Non Data Capable Machine</source>
        <translation>Dit apparaat verstrekt geen gegevens</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="577"/>
        <source>Your Philips Respironics CPAP machine (Model %1) is unfortunately not a data capable model.</source>
        <translation>Uw apparaat van Respironics (Model %1) kan helaas geen gegevens verstrekken.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="588"/>
        <source>Machine Unsupported</source>
        <translation>Niet ondersteund apparaat</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="589"/>
        <source>Sorry, your Philips Respironics CPAP machine (Model %1) is not supported yet.</source>
        <translation>Sorry, uw Philips Respironics CPAP (Model %1) wordt nog niet ondersteund.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="578"/>
        <source>I&apos;m sorry to report that OSCAR can only track hours of use and very basic settings for this machine.</source>
        <translation>Het spijt me dat OSCAR van dit apparaat alleen gebruiksuren en erg simpele instellingen kan verwerken.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="590"/>
        <source>The developers needs a .zip copy of this machines&apos; SD card and matching Encore .pdf reports to make it work with OSCAR.</source>
        <translation>Wij willen graag een .zip bestand van de SD-kaart en bijbehorende Encore .pdf rapporten om het te kunnen analyseren voor gebruik in OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="631"/>
        <source>Scanning Files...</source>
        <translation>Bestanden bekijken...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="763"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1980"/>
        <source>Importing Sessions...</source>
        <translation>Sessies importeren...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="768"/>
        <source>Finishing up...</source>
        <translation>Afronden...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2240"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3574"/>
        <source>15mm</source>
        <translation>15mm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2240"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3573"/>
        <source>22mm</source>
        <translation>22mm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3458"/>
        <source>RemStar Plus</source>
        <translation>RemStar Plus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3460"/>
        <source>BiPAP autoSV Advanced 60 Series</source>
        <translation>BiPAP autoSV Advanced 60 Series</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3463"/>
        <source>CPAP Pro</source>
        <translation>CPAP Pro</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3464"/>
        <source>Auto CPAP</source>
        <translation>Auto CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3465"/>
        <source>BiPAP Pro</source>
        <translation>BiPAP Pro</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3466"/>
        <source>Auto BiPAP</source>
        <translation>Auto BiPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3487"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3489"/>
        <source>Flex Mode</source>
        <translation>Flex modus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3488"/>
        <source>PRS1 pressure relief mode.</source>
        <translation>PRS1 drukhulp modus.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3494"/>
        <source>C-Flex</source>
        <translation>C-flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3495"/>
        <source>C-Flex+</source>
        <translation>C-flex+</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3496"/>
        <source>A-Flex</source>
        <translation>A-flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3497"/>
        <source>Rise Time</source>
        <translation>Stijgtijd</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3498"/>
        <source>Bi-Flex</source>
        <translation>Bi-flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3502"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3504"/>
        <source>Flex Level</source>
        <translation>Flex instelling</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3503"/>
        <source>PRS1 pressure relief setting.</source>
        <translation>PRS1 drukhulp instelling.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3508"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3539"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3561"/>
        <source>x1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3509"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3540"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3562"/>
        <source>x2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3510"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3541"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3563"/>
        <source>x3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3511"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3542"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3564"/>
        <source>x4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3512"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3543"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3565"/>
        <source>x5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3516"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3518"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3056"/>
        <source>Humidifier Status</source>
        <translation>Status bevochtiger</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3517"/>
        <source>PRS1 humidifier connected?</source>
        <translation>Is de bevochtiger aan de PRS1 aangesloten?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3520"/>
        <source>Disconnected</source>
        <translation>Losgekoppeld</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3521"/>
        <source>Connected</source>
        <translation>Aangekoppeld</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3525"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3527"/>
        <source>Heated Tubing</source>
        <translation>Verwarmde slang</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3526"/>
        <source>Heated Tubing Connected</source>
        <translation>Verwarmde slang aangesloten</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3534"/>
        <source>Humidification Level</source>
        <translation>Instelling bevochtiger</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3535"/>
        <source>PRS1 Humidification level</source>
        <translation>Instelling bevochtiger PRS1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3536"/>
        <source>Humid. Lvl.</source>
        <translation>Bevocht.inst.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3547"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3548"/>
        <source>System One Resistance Status</source>
        <translation>Status System One weerstand</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3549"/>
        <source>Sys1 Resist. Status</source>
        <translation>Sys1 weerst. status</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3556"/>
        <source>System One Resistance Setting</source>
        <translation>Instelling System One weerstand</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3557"/>
        <source>System One Mask Resistance Setting</source>
        <translation>Instelling maskerweerstand</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3558"/>
        <source>Sys1 Resist. Set</source>
        <translation>Inst.weerstand</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3569"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3571"/>
        <source>Hose Diameter</source>
        <translation>Slangdiameter</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3570"/>
        <source>Diameter of primary CPAP hose</source>
        <translation>Diameter van de belangrijkste slang</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3578"/>
        <source>System One Resistance Lock</source>
        <translation>Weerstand vergrendeling</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3579"/>
        <source>Whether System One resistance settings are available to you.</source>
        <translation>Of de instellingen beschikbaar zijn.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3580"/>
        <source>Sys1 Resist. Lock</source>
        <translation>Weerst. vergrendeld</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3587"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3589"/>
        <source>Auto On</source>
        <translation>Automatische start</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3588"/>
        <source>A few breaths automatically starts machine</source>
        <translation>Het apparaat start na enkele ademhalingen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3596"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3598"/>
        <source>Auto Off</source>
        <translation>Automatisch uit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3597"/>
        <source>Machine automatically switches off</source>
        <translation>Het apparaat schakelt automatisch uit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3605"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3607"/>
        <source>Mask Alert</source>
        <translation>Masker waarschuwing</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3606"/>
        <source>Whether or not machine allows Mask checking.</source>
        <translation>Of controle van het masker is ingeschakeld.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3614"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3616"/>
        <source>Show AHI</source>
        <translation>Toon AHI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3615"/>
        <source>Whether or not machine shows AHI via LCD panel.</source>
        <translation>Of het apparaat de AHI op het scherm toont.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3628"/>
        <source>Unknown PRS1 Code %1</source>
        <translation>Onbekende PRS1 code %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3629"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3630"/>
        <source>PRS1_%1</source>
        <translation>PRS1_%1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3686"/>
        <source>Breathing Not Detected</source>
        <translation>Geen ademhaling gedetecteerd (BND)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3687"/>
        <source>A period during a session where the machine could not detect flow.</source>
        <translation>Een periode tijdens een sessie waarbij het apparaat geen flow kon detecteren.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3688"/>
        <source>BND</source>
        <translation>BND</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3703"/>
        <source>Timed Breath</source>
        <translation>Geprogrammeerde ademhaling</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3704"/>
        <source>Machine Initiated Breath</source>
        <translation>Door apparaat getriggerde ademhaling</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3705"/>
        <source>TB</source>
        <translation>TB</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="253"/>
        <source>Don&apos;t forget to place your datacard back in your CPAP machine</source>
        <translation>Vergeet niet om de SD-kaart weer in uw apparaat te steken</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="253"/>
        <source>OSCAR Reminder</source>
        <translation>OSCAR herinnering</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="424"/>
        <source>You can only work with one instance of an individual OSCAR profile at a time.</source>
        <translation>U mag in OSCAR maar met één profiel tegelijk open hebben.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="425"/>
        <source>If you are using cloud storage, make sure OSCAR is closed and syncing has completed first on the other computer before proceeding.</source>
        <translation>Als U cloud-opslag gebruikt, zorg dan dat OSCAR is afgesloten en de synchronisatie is afgerond voordat U verder gaat.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="438"/>
        <source>Loading profile &quot;%1&quot;...</source>
        <translation>Profiel &quot;%1&quot; aan het laden...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2142"/>
        <source>Sorry, your %1 %2 machine is not currently supported.</source>
        <translation>Sorry, uw %1 %2 apparaat wordt nog niet ondersteund.</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="38"/>
        <source>There are no graphs visible to print</source>
        <translation>Geen zichtbare grafieken om af te drukken</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="55"/>
        <source>Would you like to show bookmarked areas in this report?</source>
        <translation>Wilt U gebieden met bladwijzer in dit rapport tonen?</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="98"/>
        <source>Printing %1 Report</source>
        <translation>Rapport %1 afdrukken</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="132"/>
        <source>%1 Report</source>
        <translation>%1 Rapport</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="190"/>
        <source>: %1 hours, %2 minutes, %3 seconds
</source>
        <translation>: %1 uren, %2 minuten, %3 seconden
</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="236"/>
        <source>RDI	%1
</source>
        <translation>RDI	%1
</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="238"/>
        <source>AHI	%1
</source>
        <translation>AHI	%1
</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="271"/>
        <source>AI=%1 HI=%2 CAI=%3 </source>
        <translation>AI: %1 HI: %2 CAI: %3 </translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="277"/>
        <source>REI=%1 VSI=%2 FLI=%3 PB/CSR=%4%%</source>
        <translation>REI: %1 VSI: %2 FLI: %3 PB/CSR: %4%</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="281"/>
        <source>UAI=%1 </source>
        <translation>UAI: %1 </translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="283"/>
        <source>NRI=%1 LKI=%2 EPI=%3</source>
        <translation>NRI: %1 LKI: %2 EPI: %3</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="352"/>
        <source>Reporting from %1 to %2</source>
        <translation>Rapport van %1 tot %2</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="424"/>
        <source>Entire Day&apos;s Flow Waveform</source>
        <translation>Flow golfvorm hele dag</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="430"/>
        <source>Current Selection</source>
        <translation>Huidige selectie</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="440"/>
        <source>Entire Day</source>
        <translation>Gehele dag</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="555"/>
        <source>OSCAR v%1</source>
        <translation>OSCAR v%1</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="561"/>
        <source>Page %1 of %2</source>
        <translation>Pagina %1 van %2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="64"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="395"/>
        <source>Jan</source>
        <translation>Jan</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="64"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="395"/>
        <source>Feb</source>
        <translation>Feb</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="64"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="395"/>
        <source>Mar</source>
        <translation>Mrt</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="64"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="395"/>
        <source>Apr</source>
        <translation>Apr</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="64"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="395"/>
        <source>May</source>
        <translation>Mei</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="64"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="395"/>
        <source>Jun</source>
        <translation>Jun</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="396"/>
        <source>Jul</source>
        <translation>Jul</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="396"/>
        <source>Aug</source>
        <translation>Aug</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="396"/>
        <source>Sep</source>
        <translation>Sep</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="396"/>
        <source>Oct</source>
        <translation>Okt</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="396"/>
        <source>Nov</source>
        <translation>Nov</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="396"/>
        <source>Dec</source>
        <translation>Dec</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="366"/>
        <source>Events</source>
        <translation>Incidenten</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="364"/>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="367"/>
        <source>Duration</source>
        <translation>Tijdsduur</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="381"/>
        <source>(% %1 in events)</source>
        <translation>(%1% in incidenten)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="110"/>
        <source>Couldn&apos;t parse Channels.xml, this build is seriously borked, no choice but to abort!!</source>
        <translation>Kon Channels.xml niet lezen, deze versie is echt brak, geen andere keuze dan om af te breken!</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="121"/>
        <source>Therapy Pressure</source>
        <translation>Therapiedruk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="122"/>
        <source>Inspiratory Pressure</source>
        <translation>Inademdruk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="123"/>
        <source>Lower Inspiratory Pressure</source>
        <translation>Laagste inademdruk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="124"/>
        <source>Higher Inspiratory Pressure</source>
        <translation>Hoogste inademdruk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="125"/>
        <source>Expiratory Pressure</source>
        <translation>Uitademdruk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="126"/>
        <source>Lower Expiratory Pressure</source>
        <translation>Onderste uitademdruk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="127"/>
        <source>Higher Expiratory Pressure</source>
        <translation>Bovenste uitademdruk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="128"/>
        <source>Pressure Support</source>
        <translation>Drukhulp</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="129"/>
        <source>PS Min</source>
        <translation>PS min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="129"/>
        <source>Pressure Support Minimum</source>
        <translation>Minimale drukhulp</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="130"/>
        <source>PS Max</source>
        <translation>PS max</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="130"/>
        <source>Pressure Support Maximum</source>
        <translation>Maximale drukhulp</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="131"/>
        <source>Min Pressure</source>
        <translation>Minimale druk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="131"/>
        <source>Minimum Therapy Pressure</source>
        <translation>Minimum therapiedruk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="131"/>
        <source>Pressure Min</source>
        <translation>Minimum druk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="132"/>
        <source>Pressure Max</source>
        <translation>Maximum druk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="138"/>
        <source>Cheyne Stokes Respiration</source>
        <translation>Cheyne Stokes Ademhaling (CSR)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="138"/>
        <source>CSR</source>
        <translation>CSR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="140"/>
        <source>Periodic Breathing</source>
        <translation>Periodieke ademhaling (PB)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="140"/>
        <source>An abnormal period of Periodic Breathing</source>
        <translation>Een abnormale tijdsduur van periodieke ademhaling</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="142"/>
        <source>Clear Airway</source>
        <translation>Open luchtweg of Centrale apneu (CA)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="144"/>
        <source>Obstructive</source>
        <translation>Obstructieve apneu (OA)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="152"/>
        <source>Respiratory Effort Related Arousal: An restriction in breathing that causes an either an awakening or sleep disturbance.</source>
        <translation>Ontwaken door ademhalingsprobleem: Een beperking van de ademhaling die (gedeeltelijk) ontwaken of een verstoring van de slaap veroorzaakt.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="175"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="178"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="181"/>
        <source>A user definable event detected by OSCAR&apos;s flow waveform processor.</source>
        <translation>Door de gebruiker instelbaar incident dat door OSCAR wordt herkend.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="197"/>
        <source>Perfusion Index</source>
        <translation>Perfusie index</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="197"/>
        <source>A relative assessment of the pulse strength at the monitoring site</source>
        <translation>Een relatieve benadering van de sterkte van de polsslag op de gemeten plek</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="197"/>
        <source>Perf. Index %</source>
        <translation>Perf index %</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="278"/>
        <source>CPAP Session contains summary data only</source>
        <translation>Deze sessie bevat uitsluitend overzichtgegevens</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="282"/>
        <source>PAP Mode</source>
        <translation>Soort apparaat</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="282"/>
        <source>PAP Device Mode</source>
        <translation>Soort PAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="286"/>
        <source>APAP (Variable)</source>
        <translation>APAP (variabel)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="290"/>
        <source>ASV (Fixed EPAP)</source>
        <translation>ASV (Vaste EPAP)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="291"/>
        <source>ASV (Variable EPAP)</source>
        <translation>ASV (Variabele EPAP)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="300"/>
        <source>Height</source>
        <translation>Lengte</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="300"/>
        <source>Physical Height</source>
        <translation>Lichaamslengte</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="301"/>
        <source>Notes</source>
        <translation>Notities</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="301"/>
        <source>Bookmark Notes</source>
        <translation>Bladwijzer notities</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="303"/>
        <source>Body Mass Index</source>
        <translation>Body Mass Index</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="304"/>
        <source>How you feel (0 = like crap, 10 = unstoppable)</source>
        <translation>Hoe voelt U zich (0=waardeloos, 10=fantastisch)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="305"/>
        <source>Bookmark Start</source>
        <translation>Bladwijzer begin</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="306"/>
        <source>Bookmark End</source>
        <translation>Bladwijzer eind</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="307"/>
        <source>Last Updated</source>
        <translation>Laatst bijgewerkt</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="308"/>
        <source>Journal Notes</source>
        <translation>Dagboek notities</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="308"/>
        <source>Journal</source>
        <translation>Dagboek</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="315"/>
        <source>1=Awake 2=REM 3=Light Sleep 4=Deep Sleep</source>
        <translation>1=Wakker 2=REM 3=Lichte slaap 4=Diepe slaap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="317"/>
        <source>Brain Wave</source>
        <translation>Hersengolf</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="317"/>
        <source>BrainWave</source>
        <translation>Hersengolf</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="318"/>
        <source>Awakenings</source>
        <translation>Ontwakingen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="318"/>
        <source>Number of Awakenings</source>
        <translation>Aantal keren wakker geworden</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="319"/>
        <source>Morning Feel</source>
        <translation>Morgenstemming</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="319"/>
        <source>How you felt in the morning</source>
        <translation>Hoe U zich &apos;s morgens voelt</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="320"/>
        <source>Time Awake</source>
        <translation>Wektijd</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="320"/>
        <source>Time spent awake</source>
        <translation>Tijdsduur wakker gebleven</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="321"/>
        <source>Time In REM Sleep</source>
        <translation>Tijd in REM-slaap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="321"/>
        <source>Time spent in REM Sleep</source>
        <translation>Tijdsduur in REM-slaap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="321"/>
        <source>Time in REM Sleep</source>
        <translation>Tijd in REM-slaap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="322"/>
        <source>Time In Light Sleep</source>
        <translation>Tijd in ondiepe slaap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="322"/>
        <source>Time spent in light sleep</source>
        <translation>Tijdsduur in ondiepe slaap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="322"/>
        <source>Time in Light Sleep</source>
        <translation>Tijd in ondiepe slaap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="323"/>
        <source>Time In Deep Sleep</source>
        <translation>Tijd in diepe slaap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="323"/>
        <source>Time spent in deep sleep</source>
        <translation>Tijdsduur in diepe slaap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="323"/>
        <source>Time in Deep Sleep</source>
        <translation>Tijd in diepe slaap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="324"/>
        <source>Time to Sleep</source>
        <translation>Tijd tot slapen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="324"/>
        <source>Time taken to get to sleep</source>
        <translation>Tijdsduur tot in slaap vallen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>Zeo ZQ</source>
        <translation>Zeo ZQ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>Zeo sleep quality measurement</source>
        <translation>Zeo slaapkwaliteit meting</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>ZEO ZQ</source>
        <translation>ZEO ZQ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="334"/>
        <source>Debugging channel #1</source>
        <translation>Kanaal#1 repareren</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="334"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="335"/>
        <source>Top secret internal stuff you&apos;re not supposed to see ;)</source>
        <translation>Top secret internal stuff you&apos;re not supposed to see ;)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="334"/>
        <source>Test #1</source>
        <translation>Test #1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="335"/>
        <source>Debugging channel #2</source>
        <translation>Kanaal #2 repareren</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="335"/>
        <source>Test #2</source>
        <translation>Test #2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="730"/>
        <source>Zero</source>
        <translation>Nul</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="733"/>
        <source>Upper Threshold</source>
        <translation>Bovengrens</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="736"/>
        <source>Lower Threshold</source>
        <translation>Ondergrens</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="132"/>
        <source>Max Pressure</source>
        <translation>Max. druk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="132"/>
        <source>Maximum Therapy Pressure</source>
        <translation>Maximum therapiedruk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="133"/>
        <source>Ramp Time</source>
        <translation>Aanlooptijd</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="133"/>
        <source>Ramp Delay Period</source>
        <translation>Aanloop vertraging</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="135"/>
        <source>Ramp Event</source>
        <translation>Aanloop incident</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3134"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3136"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="135"/>
        <source>Ramp</source>
        <translation>Aanloop</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="138"/>
        <source>An abnormal period of Cheyne Stokes Respiration</source>
        <translation>Een abnormale tijdsduur van Cheyne-Stokes ademhaling</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="148"/>
        <source>An apnea that couldn&apos;t be determined as Central or Obstructive.</source>
        <translation>Een apneu die niet als centraal of obstructief kon worden geclassificeerd.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="150"/>
        <source>A restriction in breathing from normal, causing a flattening of the flow waveform.</source>
        <translation>Een abnormale beperking van de ademhaling, waardoor de stroomsnelheid afvlakte.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="156"/>
        <source>Vibratory Snore (VS2) </source>
        <translation>Vibrerend snurken (VS2) </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <source>Leak Flag</source>
        <translation>Lekmarkering (LF)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <source>LF</source>
        <translation>LF</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="275"/>
        <source>Mask On Time</source>
        <translation>Tijdstip masker opgezet</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="275"/>
        <source>Time started according to str.edf</source>
        <translation>Starttijd volgens het bestand str.edf</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="278"/>
        <source>Summary Only</source>
        <translation>Alleen overzichtsgegevens</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="134"/>
        <source>Ramp Pressure</source>
        <translation>Aanloopdruk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="134"/>
        <source>Starting Ramp Pressure</source>
        <translation>Aanloop startdruk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="510"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="142"/>
        <source>An apnea where the airway is open</source>
        <translation>Een apneu waarbij de luchtweg niet is afgesloten</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="144"/>
        <source>An apnea caused by airway obstruction</source>
        <translation>Een apneu waarbij de luchtweg is afgesloten</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="146"/>
        <source>Hypopnea</source>
        <translation>Hypopneu (H)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="146"/>
        <source>A partially obstructed airway</source>
        <translation>Een gedeeltelijk afgesloten luchtweg</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="148"/>
        <source>Unclassified Apnea</source>
        <translation>Onbekende apneu (UA)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="148"/>
        <source>UA</source>
        <translation>UA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="154"/>
        <source>Vibratory Snore</source>
        <translation>Vibrerend snurken (VS)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="154"/>
        <source>A vibratory snore</source>
        <translation>Een snurk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="156"/>
        <source>A vibratory snore as detcted by a System One machine</source>
        <translation>System One detecteert vibrerend snurken</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3481"/>
        <source>Pressure Pulse</source>
        <translation>Drukpuls</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3482"/>
        <source>A pulse of pressure &apos;pinged&apos; to detect a closed airway.</source>
        <translation>Een kleine drukgolf waarmee een afgesloten luchtweg wordt gedetecteerd.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="634"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="163"/>
        <source>Large Leak</source>
        <translation>Groot lek (LL)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="163"/>
        <source>A large mask leak affecting machine performance.</source>
        <translation>Dusdanige lekkage dat het apparaat niet meer goed detecteert.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="635"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="163"/>
        <source>LL</source>
        <translation>LL</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="166"/>
        <source>Non Responding Event</source>
        <translation>Incident zonder reactie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="166"/>
        <source>A type of respiratory event that won&apos;t respond to a pressure increase.</source>
        <translation>Een ademhalings-incident dat niet door drukverhoging ophoudt.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="169"/>
        <source>Expiratory Puff</source>
        <translation>Uitademstoot</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="169"/>
        <source>Intellipap event where you breathe out your mouth.</source>
        <translation>Een Intellipap incident waarbij U door de mond uitademt.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="172"/>
        <source>SensAwake feature will reduce pressure when waking is detected.</source>
        <translation>De SensAwake functie verlaagt de druk als een arousal wordt gedetecteerd.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="175"/>
        <source>User Flag #1</source>
        <translation>Gebruikersmarkering UF1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="178"/>
        <source>User Flag #2</source>
        <translation>Gebruikersmarkering UF2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="181"/>
        <source>User Flag #3</source>
        <translation>Gebruikersmarkering UF3</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="185"/>
        <source>Heart rate in beats per minute</source>
        <translation>Pols in slagen per minuut</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="190"/>
        <source>Blood-oxygen saturation percentage</source>
        <translation>Bloedzuurstof saturatie in procent</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="190"/>
        <source>SpO2 %</source>
        <translation>SpO2 %</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="194"/>
        <source>Plethysomogram</source>
        <translation>Plethysomogram</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="194"/>
        <source>An optical Photo-plethysomogram showing heart rhythm</source>
        <translation>Een optisch foto-plethysomogram die het hartritme laat zien</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="200"/>
        <source>Pulse Change</source>
        <translation>Wijziging in polsslag (PC)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="200"/>
        <source>A sudden (user definable) change in heart rate</source>
        <translation>Een plotselinge verandering in polsslag (instelbaar)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="203"/>
        <source>SpO2 Drop</source>
        <translation>SpO2 verlaging (SD)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="203"/>
        <source>A sudden (user definable) drop in blood oxygen saturation</source>
        <translation>Een plotselinge verlaging in zuurstofsaturatie (instelbaar)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="203"/>
        <source>SD</source>
        <translation>SD</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="207"/>
        <source>Breathing flow rate waveform</source>
        <translation>Ademhalings golfvorm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="513"/>
        <source>L/min</source>
        <translation>l/min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="210"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="213"/>
        <source>Mask Pressure</source>
        <translation>Maskerdruk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="213"/>
        <source>Mask Pressure (High resolution)</source>
        <translation>Maskerdruk (hoge resolutie)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="216"/>
        <source>Amount of air displaced per breath</source>
        <translation>Hoeveelheid lucht verplaatst door ademhaling</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="219"/>
        <source>Graph displaying snore volume</source>
        <translation>Grafiek die de mate van snurken weergeeft</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3009"/>
        <source>??</source>
        <translation>??</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="222"/>
        <source>Minute Ventilation</source>
        <translation>Minuutventilatie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="222"/>
        <source>Amount of air displaced per minute</source>
        <translation>Hoeveelheid verplaatste lucht per minuut</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="225"/>
        <source>Respiratory Rate</source>
        <translation>Ademhalingstempo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="225"/>
        <source>Rate of breaths per minute</source>
        <translation>Tempo van de ademhaling per minuut</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="228"/>
        <source>Patient Triggered Breaths</source>
        <translation>Pat. Veroorz. Ademh</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="228"/>
        <source>Percentage of breaths triggered by patient</source>
        <translation>Percentage ademhalingen door de patient</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="228"/>
        <source>Pat. Trig. Breaths</source>
        <translation>Pat. geact. teugen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="231"/>
        <source>Leak Rate</source>
        <translation>Leksnelheid</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="231"/>
        <source>Rate of detected mask leakage</source>
        <translation>Snelheid van de maskerlekkage</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="235"/>
        <source>I:E Ratio</source>
        <translation>I:E verhouding</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="235"/>
        <source>Ratio between Inspiratory and Expiratory time</source>
        <translation>Verhouding tussen inadem- en uitademtijd</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="518"/>
        <source>ratio</source>
        <translation>verhouding</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="238"/>
        <source>Expiratory Time</source>
        <translation>Uitademtijd</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="238"/>
        <source>Time taken to breathe out</source>
        <translation>Tijdsduur van het uitademen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="241"/>
        <source>Inspiratory Time</source>
        <translation>Inademtijd</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="241"/>
        <source>Time taken to breathe in</source>
        <translation>Tijdsduur van het inademen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="244"/>
        <source>Respiratory Event</source>
        <translation>Ademhalingsincident</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="244"/>
        <source>A ResMed data source showing Respiratory Events</source>
        <translation>Een ResMed gegevensblok met ademhalingsincidenten</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="247"/>
        <source>Graph showing severity of flow limitations</source>
        <translation>Grafiek die de ernst van de stroombeperking aangeeft</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="247"/>
        <source>Flow Limit.</source>
        <translation>Stroombeperk.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="250"/>
        <source>Target Minute Ventilation</source>
        <translation>Doelminuutventilatie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="253"/>
        <source>Maximum Leak</source>
        <translation>Maximum lekkage</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="253"/>
        <source>The maximum rate of mask leakage</source>
        <translation>De maximum leksnelheid</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="253"/>
        <source>Max Leaks</source>
        <translation>Max. lek</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="256"/>
        <source>Apnea Hypopnea Index</source>
        <translation>Apneu-hypopneu Index</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="256"/>
        <source>Graph showing running AHI for the past hour</source>
        <translation>Grafiek met de voortschrijdende AHI van het afgelopen uur</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="259"/>
        <source>Total Leak Rate</source>
        <translation>Totale lekkage</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="259"/>
        <source>Detected mask leakage including natural Mask leakages</source>
        <translation>Gedetecteerde maskerlekkage inclusief de bedoelde lek</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="262"/>
        <source>Median Leak Rate</source>
        <translation>Mediaan van de lekkage</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="262"/>
        <source>Median rate of detected mask leakage</source>
        <translation>De mediaan van de leksnelheid</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="262"/>
        <source>Median Leaks</source>
        <translation>Mediaan lek</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="265"/>
        <source>Respiratory Disturbance Index</source>
        <translation>Ademhalings Stoornis Index (RDI)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="265"/>
        <source>Graph showing running RDI for the past hour</source>
        <translation>Grafiek met de voorstschrijdende RDI van het afgelopen uur</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="269"/>
        <source>Sleep position in degrees</source>
        <translation>Slaaphouding in graden</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="272"/>
        <source>Upright angle in degrees</source>
        <translation>Zit/lig stand in graden</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1036"/>
        <source>Plots Disabled</source>
        <translation>Grafieken uitgeschakeld</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1110"/>
        <source>Duration %1:%2:%3</source>
        <translation>Tijdsduur %1 %2 %3</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1111"/>
        <source>AHI %1</source>
        <translation>AHI %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="997"/>
        <source>Days: %1</source>
        <translation>Dagen: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1001"/>
        <source>Low Usage Days: %1</source>
        <translation>Korte dagen: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1002"/>
        <source>(%1% compliant, defined as &gt; %2 hours)</source>
        <translation>(%1% therapietrouw, met meer dan %2 uren)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1118"/>
        <source>(Sess: %1)</source>
        <translation>(Sessies: %1)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1126"/>
        <source>Bedtime: %1</source>
        <translation>Naar bed: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1128"/>
        <source>Waketime: %1</source>
        <translation>Opstaan: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1168"/>
        <source>90%</source>
        <translation>90%</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1240"/>
        <source>(Summary Only)</source>
        <translation>(Alleen overzichtgegevens)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="423"/>
        <source>There is a lockfile already present for this profile &apos;%1&apos;, claimed on &apos;%2&apos;.</source>
        <translation>Er is een blokkeervlag voor het profiel &apos;%1&apos;, dat in gebruik is door &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="154"/>
        <source>Peak</source>
        <translation>Piek</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="158"/>
        <source>%1% %2</source>
        <translation>%1% %2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="287"/>
        <source>Fixed Bi-Level</source>
        <translation>Vaste Bi-level</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="288"/>
        <source>Auto Bi-Level (Fixed PS)</source>
        <translation>Auto Bi-level (met vaste PS)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="289"/>
        <source>Auto Bi-Level (Variable PS)</source>
        <translation>Auto Bi-level (Variabele PS)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1373"/>
        <source>%1%2</source>
        <translation>%1%2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1396"/>
        <source>Fixed %1 (%2)</source>
        <translation>Vaste %1 (%2)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1398"/>
        <source>Min %1 Max %2 (%3)</source>
        <translation>Min: %1 Max: %2 (%3)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1400"/>
        <location filename="../oscar/SleepLib/day.cpp" line="1415"/>
        <source>EPAP %1 IPAP %2 (%3)</source>
        <translation>EPAP: %1 IPAP: %2 (%3)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1402"/>
        <source>PS %1 over %2-%3 (%4)</source>
        <translation>PS: %1 over %2-%3 (%4)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1404"/>
        <location filename="../oscar/SleepLib/day.cpp" line="1408"/>
        <source>Min EPAP %1 Max IPAP %2 PS %3-%4 (%5)</source>
        <translation>Min EPAP: %1 Max IPAP: %2 PS: %3-%4 (%5)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1406"/>
        <source>EPAP %1 PS %2-%3 (%6)</source>
        <translation>EPAP: %1 PS: %2-%3 (%6)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="145"/>
        <location filename="../oscar/SleepLib/day.cpp" line="147"/>
        <location filename="../oscar/SleepLib/day.cpp" line="149"/>
        <location filename="../oscar/SleepLib/day.cpp" line="154"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="297"/>
        <source>Most recent Oximetry data: &lt;a onclick=&apos;alert(&quot;daily=%2&quot;);&apos;&gt;%1&lt;/a&gt; </source>
        <translation>Meest recente oxymetriegegevens: &lt;a onclick=&apos;alert(&quot;daily=%2&quot;);&apos;&gt;%1&lt;/a&gt; </translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="298"/>
        <source>(last night)</source>
        <translation>(afgelopen nacht)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="299"/>
        <source>(yesterday)</source>
        <translation>(gisteren)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="300"/>
        <source>(%2 day ago)</source>
        <translation>(%2 dag(en) geleden)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="305"/>
        <source>No oximetry data has been imported yet.</source>
        <translation>Er zijn nog geen oxymetriegegevens geïmporteerd.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="39"/>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="41"/>
        <source>Contec</source>
        <translation>Contec</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="39"/>
        <source>CMS50</source>
        <translation>CMS50</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="77"/>
        <source>Fisher &amp; Paykel</source>
        <translation>Fisher &amp; Paykell</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="77"/>
        <source>ICON</source>
        <translation>ICON</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="77"/>
        <source>DeVilbiss</source>
        <translation>DeVilbiss</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="77"/>
        <source>Intellipap</source>
        <translation>IntelliPap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="85"/>
        <source>SmartFlex Settings</source>
        <translation>Instellingen SmartFlex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="39"/>
        <source>ChoiceMMed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="39"/>
        <source>MD300</source>
        <translation>MD300</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="66"/>
        <source>Respironics</source>
        <translation>Respironics</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="66"/>
        <source>M-Series</source>
        <translation>M-series</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="221"/>
        <source>Philips Respironics</source>
        <translation>Philips Respironics</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="272"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="221"/>
        <source>System One</source>
        <translation>System One</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="446"/>
        <source>ResMed</source>
        <translation>ResMed</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="446"/>
        <source>S9</source>
        <translation>S9</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="455"/>
        <source>EPR: </source>
        <translation>EPR: </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="37"/>
        <source>Somnopose</source>
        <translation>SomnoPose</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="37"/>
        <source>Somnopose Software</source>
        <translation>SomnoPose Programma</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="38"/>
        <source>Zeo</source>
        <translation>Zeo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="38"/>
        <source>Personal Sleep Coach</source>
        <translation>Persoonlijke Slaap Trainer</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="196"/>
        <source>Database Outdated
Please Rebuild CPAP Data</source>
        <translation>Verouderde database
Gaarne gegevens opnieuw inlezen</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="373"/>
        <source> (%2 min, %3 sec)</source>
        <translation> (%2 min, %3 sec)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="375"/>
        <source> (%3 sec)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1543"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1545"/>
        <source>SmartFlex Mode</source>
        <translation>SmartFlex instelling</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1544"/>
        <source>Intellipap pressure relief mode.</source>
        <translation>IntelliPap drukhulp modus.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1550"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3025"/>
        <source>Ramp Only</source>
        <translation>Alleen tijdens aanloop</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1551"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3026"/>
        <source>Full Time</source>
        <translation>Continu</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1554"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1556"/>
        <source>SmartFlex Level</source>
        <translation>Instelling SmartFlex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1555"/>
        <source>Intellipap pressure relief level.</source>
        <translation>IntelliPap drukhulp instelling.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="657"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1676"/>
        <source>VPAP Adapt</source>
        <translation>Aangepaste VPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1644"/>
        <source>Parsing Identification File</source>
        <translation>Identificatiebestand verwerken</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1743"/>
        <source>Locating STR.edf File(s)...</source>
        <translation>Lokaliseren STR.edf bestand (en) ...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1918"/>
        <source>Cataloguing EDF Files...</source>
        <translation>EDF-bestanden catalogiseren ...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1929"/>
        <source>Queueing Import Tasks...</source>
        <translation>Importtaken in de wachtrij zetten ...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1989"/>
        <source>Finishing Up...</source>
        <translation>Afronden...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3000"/>
        <source>CPAP Mode</source>
        <translation variants="yes">
            <lengthvariant>Soort apparaat</lengthvariant>
            <lengthvariant></lengthvariant>
            <lengthvariant></lengthvariant>
        </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3006"/>
        <source>VPAP-T</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3007"/>
        <source>VPAP-S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3008"/>
        <source>VPAP-S/T</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3010"/>
        <source>VPAPauto</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3012"/>
        <source>ASVAuto</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3013"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3014"/>
        <source>???</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3015"/>
        <source>Auto for Her</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3018"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3020"/>
        <source>EPR</source>
        <translation>EPR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3019"/>
        <source>ResMed Exhale Pressure Relief</source>
        <translation>ResMed uitademingsdrukhulp</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3027"/>
        <source>Patient???</source>
        <translation>Patient???</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3030"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3032"/>
        <source>EPR Level</source>
        <translation>EPR niveau</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3031"/>
        <source>Exhale Pressure Relief Level</source>
        <translation>Niveau van uitademingsdrukhulp</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3035"/>
        <source>0cmH2O</source>
        <translation>0 cmWK</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3036"/>
        <source>1cmH2O</source>
        <translation>1 cmWK</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3037"/>
        <source>2cmH2O</source>
        <translation>2 cmWK</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3038"/>
        <source>3cmH2O</source>
        <translation>3 cmWK</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3045"/>
        <source>SmartStart</source>
        <translation>Autostart</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3046"/>
        <source>Machine auto starts by breathing</source>
        <translation>Apparaat start met ademhaling</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3047"/>
        <source>Smart Start</source>
        <translation>Automatisch starten</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3054"/>
        <source>Humid. Status</source>
        <translation>Bevocht. status</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3055"/>
        <source>Humidifier Enabled Status</source>
        <translation>Status bevochtiger aan</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3063"/>
        <source>Humid. Level</source>
        <translation>Bevocht. stand</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3064"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3065"/>
        <source>Humidity Level</source>
        <translation>Stand bevochtiger</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3079"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3081"/>
        <source>Temperature</source>
        <translation>Temperatuur</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3080"/>
        <source>ClimateLine Temperature</source>
        <translation>Temperatuur ClimateLine</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3086"/>
        <source>Temp. Enable</source>
        <translation>Temp. aan</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3087"/>
        <source>ClimateLine Temperature Enable</source>
        <translation>Temperatuur ClimateLine aan</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3088"/>
        <source>Temperature Enable</source>
        <translation>Temperatuur aan</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3097"/>
        <source>AB Filter</source>
        <translation>AB filter</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3098"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3099"/>
        <source>Antibacterial Filter</source>
        <translation>AntiBacterieel filter</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3106"/>
        <source>Pt. Access</source>
        <translation>Pat. toegang</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3107"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3108"/>
        <source>Patient Access</source>
        <translation>Toegang patient</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3115"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3116"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3117"/>
        <source>Climate Control</source>
        <translation>Climate Control</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3120"/>
        <source>Manual</source>
        <translation>Handmatig</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3121"/>
        <source>Auto</source>
        <translation>Automatisch</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3124"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3126"/>
        <source>Mask</source>
        <translation>Masker</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3125"/>
        <source>ResMed Mask Setting</source>
        <translation>ResMed masker instelling</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3129"/>
        <source>Pillows</source>
        <translation>Neuskussens</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3130"/>
        <source>Full Face</source>
        <translation>Volgelaat</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3131"/>
        <source>Nasal</source>
        <translation>Neus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3135"/>
        <source>Ramp Enable</source>
        <translation>Aanloop aan</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="116"/>
        <source>Weinmann</source>
        <translation>Weinmann</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="116"/>
        <source>SOMNOsoft2</source>
        <translation>SOMNOsoft2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="386"/>
        <source>Pop out Graph</source>
        <translation>Zwevende grafiek</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1406"/>
        <source>I&apos;m very sorry your machine doesn&apos;t record useful data to graph in Daily View :(</source>
        <translation>Het spijt me echt dat uw apparaat geen bruikbare gegevens opslaat om hier te laten zien :(</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1409"/>
        <source>There is no data to graph</source>
        <translation>Geen gegevens om te laten zien</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1590"/>
        <source>d MMM [ %1 - %2 ]</source>
        <translation>d MMM [ %1 - %2 ]</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2111"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2154"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2225"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2242"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2206"/>
        <source>Hide All Events</source>
        <translation>Verberg alle incidenten</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2207"/>
        <source>Show All Events</source>
        <translation>Toon alle incidenten</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2548"/>
        <source>Unpin %1 Graph</source>
        <translation>%1 grafiek losmaken</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2550"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2625"/>
        <source>Popout %1 Graph</source>
        <translation>Grafiek %1 zwevend maken</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2627"/>
        <source>Pin %1 Graph</source>
        <translation>%1 grafiek vastzetten</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="107"/>
        <source>%1: %2</source>
        <translation>%1: %2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="150"/>
        <source>Relief: %1</source>
        <translation>Vermindering:%1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="156"/>
        <source>Hours: %1h, %2m, %3s</source>
        <translation>Tijd: %1 h, %2 mm %3 s</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="259"/>
        <source>Machine Information</source>
        <translation>Apparaat-informatie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="25"/>
        <source>Journal Data</source>
        <translation>Dagboek gegevens</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="43"/>
        <source>OSCAR found an old Journal folder, but it looks like it&apos;s been renamed:</source>
        <translation>OSCAR vond een oude dagboek, maar het schijnt dat de naam is gewijzigd:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="45"/>
        <source>OSCAR will not touch this folder, and will create a new one instead.</source>
        <translation>OSCAR doet niets met deze map, maar zal een nieuwe maken.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="46"/>
        <source>Please be careful when playing in OSCAR&apos;s profile folders :-P</source>
        <translation>Weez voorzichtig met wijzigen van de profielmappen van OSCAR :-P</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="53"/>
        <source>For some reason, OSCAR couldn&apos;t find a journal object record in your profile, but did find multiple Journal data folders.

</source>
        <translation>Om de een of andere reden heeft OSCAR geen dagboekgegevens in uw profiel gevonden, maar wel meerdere mappen met dagboekgegevens.

</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="54"/>
        <source>OSCAR picked only the first one of these, and will use it in future:

</source>
        <translation>OSCAR heeft de eerste gebruikt en zal deze blijven gebruiken:

</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="56"/>
        <source>If your old data is missing, copy the contents of all the other Journal_XXXXXXX folders to this one manually.</source>
        <translation>Als de oude gegevens ontbreken, copieer dan alle Journal_XXXXXXX mappen naar deze map.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="41"/>
        <source>CMS50F3.7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="41"/>
        <source>CMS50F</source>
        <translation>CMS50F</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="351"/>
        <source>Snapshot %1</source>
        <translation>Momentopname %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="266"/>
        <source>CMS50D+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="266"/>
        <source>CMS50E/F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/updateparser.cpp" line="228"/>
        <source>%1
Line %2, column %3</source>
        <translation>%1
Regel %2, kolom %3</translation>
    </message>
    <message>
        <location filename="../oscar/updateparser.cpp" line="241"/>
        <source>Could not parse Updates.xml file.</source>
        <translation>Kon het bestand Updates.xml niet lezen.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="612"/>
        <source>Loading %1 data for %2...</source>
        <translation>%1-gegevens voor%2 laden ...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="622"/>
        <source>Scanning Files</source>
        <translation>Bestanden scannen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="658"/>
        <source>Migrating Summary File Location</source>
        <translation>Samenvattingsbestand verplaatsen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="961"/>
        <source>Loading Summaries.xml.gz</source>
        <translation>Summaries.xml.gz laden</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="1088"/>
        <source>Loading Summary Data</source>
        <translation>Samenvatting-gegevens laden</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="14"/>
        <source>Please Wait...</source>
        <translation>Even wachten ...</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/MinutesAtPressure.cpp" line="312"/>
        <source>Peak %1</source>
        <translation>Piek %1</translation>
    </message>
</context>
<context>
    <name>Report</name>
    <message>
        <location filename="../oscar/reports.ui" line="14"/>
        <source>Form</source>
        <translation>Formulier</translation>
    </message>
    <message>
        <location filename="../oscar/reports.ui" line="27"/>
        <source>about:blank</source>
        <translation>about:blank</translation>
    </message>
</context>
<context>
    <name>SessionBar</name>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="246"/>
        <source>%1h %2m</source>
        <translation>%1u: %2m</translation>
    </message>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="289"/>
        <source>No Sessions Present</source>
        <translation>Geen sessies gevonden</translation>
    </message>
</context>
<context>
    <name>Statistics</name>
    <message>
        <location filename="../oscar/statistics.cpp" line="1111"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1051"/>
        <source>Most Recent</source>
        <translation>Laatste ingelezen dag</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1053"/>
        <source>Last 30 Days</source>
        <translation>Afgelopen 30 dagen</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1055"/>
        <source>Last Year</source>
        <translation>Afgelopen jaar</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="558"/>
        <location filename="../oscar/statistics.cpp" line="559"/>
        <source>Average %1</source>
        <translation>Gemiddelde %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="502"/>
        <source>CPAP Statistics</source>
        <translation>CPAP statistiek</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="505"/>
        <location filename="../oscar/statistics.cpp" line="1216"/>
        <source>CPAP Usage</source>
        <translation>CPAP gebruik</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="506"/>
        <source>Average Hours per Night</source>
        <translation>Gemiddeld aantal uren per nacht</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="507"/>
        <source>Compliance</source>
        <translation>Therapietrouw</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="519"/>
        <source>Leak Statistics</source>
        <translation>Lekstatistiek</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="524"/>
        <source>Pressure Statistics</source>
        <translation>Drukstatistiek</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="537"/>
        <source>Oximeter Statistics</source>
        <translation>Oxymeterstatistiek</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="541"/>
        <source>Blood Oxygen Saturation</source>
        <translation>Bloedzuurstof saturatie</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="546"/>
        <source>Pulse Rate</source>
        <translation>Polsslag</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="557"/>
        <source>%1 Median</source>
        <translation>%1 mediaan</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="561"/>
        <source>Min %1</source>
        <translation>Min. %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="562"/>
        <source>Max %1</source>
        <translation>Max. %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="563"/>
        <source>%1 Index</source>
        <translation>%1 index</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="564"/>
        <source>% of time in %1</source>
        <translation>Tijd in %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="565"/>
        <source>% of time above %1 threshold</source>
        <translation>Tijd boven de %1 grens</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="566"/>
        <source>% of time below %1 threshold</source>
        <translation>Tijd onder de %1 grens</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="587"/>
        <source>Name: %1, %2</source>
        <translation>Naam: %2 %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="589"/>
        <source>DOB: %1</source>
        <translation>Geboortedatum: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="592"/>
        <source>Phone: %1</source>
        <translation>Telefoon: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="595"/>
        <source>Email: %1</source>
        <translation>E-mail: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="598"/>
        <source>Address:</source>
        <translation>Adres:</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="641"/>
        <source>Usage Statistics</source>
        <translation>Gebruiks-statistieken</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="657"/>
        <source>This report was generated by OSCAR v%1</source>
        <translation>Dit rapport is gegenereerd door OSCAR v%1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="993"/>
        <source>I can haz data?!?</source>
        <translatorcomment>haz = slang voor have</translatorcomment>
        <translation>Ik wil DATA?!?</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="995"/>
        <source>Oscar has no data to report :(</source>
        <translation>OSCAR heeft geen gegevens om te laten zien :(</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1217"/>
        <source>Days Used: %1</source>
        <translation>Dagen gebruikt: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1218"/>
        <source>Low Use Days: %1</source>
        <translation>Dagen (te) kort gebruikt: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1219"/>
        <source>Compliance: %1%</source>
        <translation>Therapietrouw: %1%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1243"/>
        <source>Days AHI of 5 or greater: %1</source>
        <translation>Dagen met AHI=5 of meer: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1250"/>
        <source>Best AHI</source>
        <translation>Laagste AHI</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1254"/>
        <location filename="../oscar/statistics.cpp" line="1266"/>
        <source>Date: %1 AHI: %2</source>
        <translation>Datum: %1 AHI: %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1260"/>
        <source>Worst AHI</source>
        <translation>Slechtste AHI</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1297"/>
        <source>Best Flow Limitation</source>
        <translation>Laagste stroombeperking</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1301"/>
        <location filename="../oscar/statistics.cpp" line="1314"/>
        <source>Date: %1 FL: %2</source>
        <translation>Datum: %1 FL: %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1307"/>
        <source>Worst Flow Limtation</source>
        <translation>Slechtste stroombeperking</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1319"/>
        <source>No Flow Limitation on record</source>
        <translation>Geen stroombeperking gevonden</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1340"/>
        <source>Worst Large Leaks</source>
        <translation>Grootste lekkage</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1348"/>
        <source>Date: %1 Leak: %2%</source>
        <translation>Datum: %1 Lek: %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1354"/>
        <source>No Large Leaks on record</source>
        <translation>Geen grote lekkage gevonden</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1377"/>
        <source>Worst CSR</source>
        <translation>Slechtste CSR</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1385"/>
        <source>Date: %1 CSR: %2%</source>
        <translation>Datum: %1 CSR: %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1390"/>
        <source>No CSR on record</source>
        <translation>Geen CSR gevonden</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1407"/>
        <source>Worst PB</source>
        <translation>Slechtste PB</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1415"/>
        <source>Date: %1 PB: %2%</source>
        <translation>Datum: %1 PB: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1420"/>
        <source>No PB on record</source>
        <translation>Geen PB gemeten</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1428"/>
        <source>Want more information?</source>
        <translation>Wilt U meer informatie?</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1429"/>
        <source>OSCAR needs all summary data loaded to calculate best/worst data for individual days.</source>
        <translation>OSCAR wil alle overzichtgegevens laden om de beste/slechtste van bepaalde dagen te berekenen.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1430"/>
        <source>Please enable Pre-Load Summaries checkbox in preferences to make sure this data is available.</source>
        <translation>Zet in de instellingen de keuze aan om alle gegevens vooraf te laden.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1450"/>
        <source>Best RX Setting</source>
        <translation>Beste Rx instelling</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1453"/>
        <location filename="../oscar/statistics.cpp" line="1465"/>
        <source>Date: %1 - %2</source>
        <translation>Datum: %1 - %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1456"/>
        <location filename="../oscar/statistics.cpp" line="1468"/>
        <source>Culminative AHI: %1</source>
        <translation>Totale AHI: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1457"/>
        <location filename="../oscar/statistics.cpp" line="1469"/>
        <source>Culminative Hours: %1</source>
        <translation>Totaal aantal uren: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1462"/>
        <source>Worst RX Setting</source>
        <translation>Slechtste Rx instelling</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1052"/>
        <source>Last Week</source>
        <translation>Afgelopen week</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1054"/>
        <source>Last 6 Months</source>
        <translation>Afgelopen halfjaar</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1059"/>
        <source>Last Session</source>
        <translation>Laatste sessie</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1125"/>
        <source>No %1 data available.</source>
        <translation>Geen %1 gegevens beschikbaar.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1128"/>
        <source>%1 day of %2 Data on %3</source>
        <translation>%1 dagen met %2 gegevens van %3</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1134"/>
        <source>%1 days of %2 Data, between %3 and %4</source>
        <translation>%1 dagen met %2 gegevens tussen %3 en %4</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="874"/>
        <source>Changes to Prescription Settings</source>
        <translation>Wijzigingen in de voorgeschreven instellingen</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="509"/>
        <source>Therapy Efficacy</source>
        <translation>Werkzaamheid therapie</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="658"/>
        <source>OSCAR is free open-source CPAP report software</source>
        <translation>OSCAR is gratis open-source CPAP-beoordelingssoftware</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="882"/>
        <source>Days</source>
        <translation>Dagen</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="886"/>
        <source>Pressure Relief</source>
        <translation>Drukvermindering</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="888"/>
        <source>Pressure Settings</source>
        <translation>Drukinstellingen</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="818"/>
        <source>Machine Information</source>
        <translation>Apparaat informatie</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="825"/>
        <source>First Use</source>
        <translation>Eerste gebruik</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="826"/>
        <source>Last Use</source>
        <translation>Laatste gebruik</translation>
    </message>
</context>
<context>
    <name>UpdaterWindow</name>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="60"/>
        <source>A new version of $APP is available</source>
        <translation>Er is een nieuwe versie van $APP</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="106"/>
        <source>Version Information</source>
        <translation>Versie-informatie</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="117"/>
        <source>Release Notes</source>
        <translation>Versie-opmerkingen</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="14"/>
        <source>OSCAR Updater</source>
        <translation>OSCAR updater</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="142"/>
        <source>Build Notes</source>
        <translation>Build-opmerkingen</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="195"/>
        <source>Maybe &amp;Later</source>
        <translation>Misschien &amp;later</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="215"/>
        <source>&amp;Upgrade Now</source>
        <translation>Nu &amp;upgraden</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="250"/>
        <source>Please wait while updates are downloaded and installed...</source>
        <translation>Wacht even tot de updates zijn gedownload en geïnstalleerd...</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="264"/>
        <source>Updates</source>
        <translation>Updates</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="304"/>
        <source>Component</source>
        <translation>Onderdeel</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="309"/>
        <source>Version</source>
        <translation>Versie</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="314"/>
        <source>Size</source>
        <translation>Grootte</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="319"/>
        <source>Progress</source>
        <translation>Voortgang</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="328"/>
        <source>Log</source>
        <translation>Log</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="374"/>
        <source>Downloading &amp; Installing Updates</source>
        <translation>Bestanden worden gedownload &amp; geïnstalleerd</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="394"/>
        <source>&amp;Finished</source>
        <translation>&amp;Klaar</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="146"/>
        <source>Requesting </source>
        <translation>Aanvragen </translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="504"/>
        <source>No updates were found for your platform.</source>
        <translation>Er zijn geen updates gevonden.</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="171"/>
        <source>%1 bytes received</source>
        <translation>%1 bytes ontvangen</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="93"/>
        <source>Updates are not yet implemented</source>
        <translation>Automatische update is nog niet geinstalleerd</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="127"/>
        <source>Checking for OSCAR Updates</source>
        <translation>Zoeken naar updates</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="158"/>
        <source>OSCAR Updates are currently unvailable for this platform</source>
        <translation>Voor dit platform zijn de updates van OSCAR momenteel niet beschikbaar</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="158"/>
        <location filename="../oscar/UpdaterWindow.cpp" line="504"/>
        <location filename="../oscar/UpdaterWindow.cpp" line="510"/>
        <source>OSCAR Updates</source>
        <translation>OSCAR updates</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="459"/>
        <source>Version %1 of OSCAR is available, opening link to download site.</source>
        <translation>Versie &lt;b&gt;%1&lt;/b&gt; van OSCAR is beschikbaar, de link naar de download wordt geopend.</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="462"/>
        <source>You are already running the latest version.</source>
        <translation>U gebruikt al de nieuwste versie.</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="511"/>
        <source>New OSCAR Updates are avilable:</source>
        <translation>Er zijn nieuwe updates voor OSCAR beschikbaar:</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="512"/>
        <source>Would you like to download and install them now?</source>
        <translation>Wilt U ze nu downloaden en installeren?</translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <location filename="../oscar/welcome.ui" line="14"/>
        <source>Form</source>
        <translation>Formulier</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="128"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation>Welkom bij de Open Source CPAP Analysis Reporter</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="143"/>
        <source>What would you like to do?</source>
        <translation>Wat wilt U gaan doen?</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="186"/>
        <source>CPAP Importer</source>
        <translation>CPAP importeren</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="224"/>
        <source>Oximetry Wizard</source>
        <translation>Oxymetrie wizard</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="262"/>
        <source>Daily View</source>
        <translation>Dagrapport</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="300"/>
        <source>Overview</source>
        <translation>Overzicht</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="338"/>
        <source>Statistics</source>
        <translation>Statistieken</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="581"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#ff0000;&quot;&gt;ResMed S9 SDCards &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; color:#ff0000;&quot;&gt;need &lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#ff0000;&quot;&gt;to be locked &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; color:#ff0000;&quot;&gt;before &lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#ff0000;&quot;&gt;inserting into your computer&lt;br/&gt;&lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#000000;&quot;&gt;Some operating systems write cache files which break their special filesystem Journal&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Waarschuwing: &lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#ff0000;&quot;&gt;Een ResMed S9 SD-kaartje &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; color:#ff0000;&quot;&gt;moet &lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#ff0000;&quot;&gt;op slot &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; color:#ff0000;&quot;&gt;voordat &lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#ff0000;&quot;&gt;U hem in uw computer steekt.&lt;br/&gt;&lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#000000;&quot;&gt;Enkele operating systemen schrijven er iets op, waardoor het bijzondere Overzicht van het bestandssystem niet meer klopt&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="139"/>
        <source>It would be a good idea to check File-&gt;Preferences first,</source>
        <translation>Het is een goed idee eerst Bestand-&gt; Voorkeuren te selecteren,</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="140"/>
        <source>as there are some options that affect import.</source>
        <translation>omdat er enkele opties zijn die van invloed zijn op importeren.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="141"/>
        <source>Note that some preferences are forced when a ResMed machine is detected</source>
        <translation>Merk op dat sommige voorkeuren worden geforceerd wanneer een ResMed-machine wordt gedetecteerd</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="142"/>
        <source>First import can take a few minutes.</source>
        <translation>De eerste import kan enkele minuten duren.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="155"/>
        <source>The last time you used your %1...</source>
        <translation>De laatste keer dat U de %1 gebruikte ...</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="159"/>
        <source>last night</source>
        <translation>afgelopen nacht</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="160"/>
        <source>yesterday</source>
        <translation>gisteren</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="161"/>
        <source>%2 days ago</source>
        <translation>%2 dagen geleden</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="163"/>
        <source>was %1 (on %2)</source>
        <translation>was %1 (op %2)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="171"/>
        <source>%1 hours, %2 minutes and %3 seconds</source>
        <translation>%1 uren, %2 minuten en %3 seconden</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="174"/>
        <source>Your machine was on for %1.</source>
        <translation>Uw apparaat stond aan gedurende %1.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="175"/>
        <source>&lt;font color = red&gt;You only had the mask on for %1.&lt;/font&gt;</source>
        <translation>&lt;font color=red&gt;U droeg uw masker maar gedurende %1.&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="186"/>
        <source>under</source>
        <translation>minder dan</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="187"/>
        <source>over</source>
        <translation>hoger dan</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="188"/>
        <source>reasonably close to</source>
        <translation>behoorlijk dicht bij</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="189"/>
        <source>equal to</source>
        <translation>gelijk aan</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="203"/>
        <source>You had an AHI of %1, which is %2 your %3 day average of %4.</source>
        <translation>U had een AHI van %1, dat is %2 uw %3-daagse gemiddelde van %4.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="212"/>
        <source>Your CPAP machine used a constant %1 %2 of air</source>
        <translation>Uw CPAP blies met een constante %1 %2 luchtdruk</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="215"/>
        <source>Your pressure was under %1 %2 for %3% of the time.</source>
        <translation>Uw druk was %3% van de tijd beneden %1 %2 (mediaan).</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="219"/>
        <source>Your machine used a constant %1-%2 %3 of air.</source>
        <translation>Uw CPAP gebruikte een constante luchtdruk van %1-%2 %3.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="228"/>
        <source>Your EPAP pressure fixed at %1 %2.</source>
        <translation>Uw EPAP druk stond vast op %1 %2.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="229"/>
        <location filename="../oscar/welcome.cpp" line="235"/>
        <source>Your IPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>Uw IPAP druk was beneden %1 %2 gedurende %3% van de tijd.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="234"/>
        <source>Your EPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>Uw EPAP druk was beneden %1 %2 gedurende %3% van de tijd.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="223"/>
        <source>Your machine was under %1-%2 %3 for %4% of the time.</source>
        <translation>Uw CPAP was beneden %1-%2 %3 gedurende %4% van de tijd.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="255"/>
        <source>Your average leaks were %1 %2, which is %3 your %4 day average of %5.</source>
        <translation>De gemiddelde lek was %1 %2, dat is %3 uw %4-daags gemiddelde van %5.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="261"/>
        <source>No CPAP data has been imported yet.</source>
        <translation>Er zijn nog geen CPAP gegevens geimporteerd.</translation>
    </message>
</context>
<context>
    <name>gGraph</name>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="793"/>
        <source>%1 days</source>
        <translation>%1 dagen</translation>
    </message>
</context>
<context>
    <name>gGraphView</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="392"/>
        <source>100% zoom level</source>
        <translation>100% zoomniveau</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="393"/>
        <source>Restore X-axis zoom too 100% to view entire days data.</source>
        <translation>Herstel het zoomniveau naar 100% om de hele grafiek te zien.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="395"/>
        <source>Reset Graph Layout</source>
        <translation>Herstel alle grafieken</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="396"/>
        <source>Resets all graphs to a uniform height and default order.</source>
        <translation>Herstelt alle grafieken naar standaard hoogte en volgorde.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="399"/>
        <source>Y-Axis</source>
        <translation>Y-as</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="400"/>
        <source>Plots</source>
        <translation>Grafieken</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="405"/>
        <source>CPAP Overlays</source>
        <translation>apneu-markeringen</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="408"/>
        <source>Oximeter Overlays</source>
        <translation>SpO2-markeringen</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="411"/>
        <source>Dotted Lines</source>
        <translation>Stippellijnen</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1746"/>
        <source>Double click title to pin / unpin
Click and drag to reorder graphs</source>
        <translation>Dubbelklik om dit kanaal vast te zetten
Klik en sleep om grafieken te verplaatsen</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2044"/>
        <source>Remove Clone</source>
        <translation>Wis kloon</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2048"/>
        <source>Clone %1 Graph</source>
        <translation>Kloon grafiek %1</translation>
    </message>
</context>
</TS>
